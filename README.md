[![service.png](https://i.postimg.cc/Y2TH1144/service.png)](https://postimg.cc/XXKmWBk3)

<h1> Smart Garage </h1>

Smart Garage is a web application that enables the owners of an auto repair shop to manage their day-to-day job. There is a list with cars, manufacturers, models and a list of available services (oil change, filters change etc.) and their price. The Garage keeps a history of all services done on customer’s car/cars. The application can visualize a detailed report for a given visit to the shop. It includes all the performed services from the visit, as well as the total price (in the default currency or another, chosen by the employee/customer). The software also is able to generate a PDF report with all the performed services for a given car and the total price (in the default currency or another, chosen by the customer).

All customers's contact information (name, phone, email, etc.) is saved and secured. Customers are able to check on their profile and look at all the information about their visits.

<br>

The project is created using Java, Spring Framework and Hibernate

<h2> 3rd Party APIs and services that have been used are: </h2>

     - Gmail API - for sending emails
     - Google Maps API - the map on the "How to register" page
     - Currency converter - for the prices in visits
     - Jasper Report - for generating reports
     - Swagger - for REST
     - Mockito Framework - Tests (services)

<br>

<h2>Database:</h2>

[![table.png](https://i.postimg.cc/BbR4bFFK/table.png)](https://postimg.cc/Fkb2n7Dr)

To create the Database go to DB folder

<h3>Creating the database</h3>

From the script in: tables_smartgarage.sql you can create the database

<h3>Fill it with data</h3>

From the script in: insert-data.sql you can fill it with example data

<br>

<h1> Front End </h1>

For the front end we have used Thymeleaf and Bootstrap

<h2> Names and Passwords for the system: </h2>

- Employee:
  - email: alexandrakoluncheva@gmail.com
  - pass: 12345678

- Customer:
  - email: rosica@gmail.com
  - pass: 12345678

NB! These email and passwords have been created directly in the database, when creating new customers or employees the passwords should contain at least 1 symbol, 1 letter and 1 number and the email to be real, you will recieve the password through that email!

<h2> Front Page </h2>

<h3> Navbar: </h3>

One of the first things we see when we open the page is the navbar. Depending of the authority of the user, or if the user is logged or not we will see different things

- Not Logged in:
  [![navbar1.png](https://imgur.com/pok3Jal.png)](https://imgur.com/pok3Jal)
  Here we can find links only to the Login and How to Register Pages

- Customer Logged in:
     [![navbar2.png](https://imgur.com/Piocp0p.png)](https://imgur.com/Piocp0p)
     Now the view is different - we see a link to the profile and Log Out button (Only customers have access to profile, when we log in as a customer we get redirected to the profile page)

- As an Employee:
     [![navbar3.png](https://imgur.com/BZ31Ou8.png)](https://imgur.com/BZ31Ou8)
     The functionalities change again - now we see the Admin Panel and Log out (Only employees have access to Admin Panel, once we log in as employee we get redirected to the Admin Panel)

<h3>Counters:</h3>

When we scrow down on the main page we can see a little more information about our system. In this piece we can see the real amaount of services, users vehicles and repairs we have in out database. The information is real and it will update every time we create something new

**Counters:**
     [![navbar3.png](https://imgur.com/x5EGe2z.png)](https://imgur.com/x5EGe2z)


<h2> How to Register </h2>
Logging in and seeing the register page are the only 2 functionalities a non logged in person has. First we will go trought the How to register page

**How to register:**
     [![navbar3.png](https://imgur.com/yePazmV.png)](https://imgur.com/yePazmV)

When we were creating the app we were told that only customers who have visited the service are allowed to have an account thats why we had to create the How to register page, so potential new customers can still find us. For this we used the Google Maps API.


**Google Maps:**
     [![navbar3.png](https://imgur.com/dQHQAhC.png)](https://imgur.com/dQHQAhC)

We created an account at Google console and used the key to get a map, but this isn't the only time we used google Api's - we also used their Gmail API for sending emails.

<h2> Login Page </h2>

The next and final thing an anonymus user can do is log in if he already has an account


**Login:**
     [![navbar3.png](https://imgur.com/40db9LC.png)](https://imgur.com/40db9LC)

<h3> Login Errors: </h3>
There are 2 types of errors you might encounter when testing the app


**Email with no @:**
     [![navbar3.png](https://imgur.com/YBOfIQe.png)](https://imgur.com/YBOfIQe)
     On the picture above we can see that the page reminds us that an email should always contain @


**Wrong Email or/and Password:**
     [![navbar3.png](https://imgur.com/xWb7FFl.png)](https://imgur.com/xWb7FFl)
     If you try to enter email or password that isn't a match in our system you will be shown a message. The message is "Wrong email or password" for security reasons.
     

<br>

<h2> Important links: </h2>
<h3> Trello: </h3>

https://trello.com/b/FviwtgJZ/smart-garage

<h3> Swagger: </h3>

To access the swagger documentation you need to run the application in IntelliJ and go to http://localhost:8080/swagger-ui.html

Here you can find all our endpoints both for MVC and REST


<h3> Invite for Postman Team: </h3>

You can see all the functionalities we have created and tested from our postman. Down below there is an invite.

https://app.getpostman.com/join-team?invite_code=6df3507e09e72a0c46c25552dd6d242b&ws=d14ae271-28d9-4b19-8bf0-85c4fcb72292
