package com.smartGarage.helpers;

import java.time.LocalDate;
import java.util.Optional;

public class CONSTANTS {
    // CUSTOMER:
    public static final String MOCK_CUSTOMER_ROLE_ID = "85d322cb-7a0f-11eb-9eaf-05e779a6d53b";

    public static final String MOCK_CUSTOMER_ID = "4bb2fa86-9399-11eb-ad6a-9829a64392c7";
    public static final String MOCK_CUSTOMER_EMAIL = "rosica@gmail.com";
    public static final String MOCK_CUSTOMER_USERNAME = "rosica";
    public static final String MOCK_CUSTOMER_PASSWORD = "12345678";

    public static final String MOCK_CUSTOMER_ID_NEW = "uuid";
    public static final String MOCK_CUSTOMER_EMAIL_NEW = "miglena@gmail.com";
    public static final String MOCK_CUSTOMER_USERNAME_NEW = "Miglena";
    public static final String MOCK_CUSTOMER_PASSWORD_NEW = "12345678";

    public static final String MOCK_CUSTOMER_DETAILS_ID = "f1f36eba-9399-11eb-ad6a-9829a64392c7";
    public static final String MOCK_CUSTOMER_DETAILS_FIRST_NAME = "Rosica";
    public static final String MOCK_CUSTOMER_DETAILS_LAST_NAME = "Staikova";
    public static final String MOCK_CUSTOMER_DETAILS_PHONE = "0894512456";

    // EMPLOYEE:
    public static final String MOCK_EMPLOYEE_ROLE_ID = "85d88d99-7a0f-11eb-9eaf-05e779a6d53b";

    public static final String MOCK_EMPLOYEE_ID = "2d15daf4-9399-11eb-ad6a-9829a64392c7";
    public static final String MOCK_EMPLOYEE_EMAIL = "alexandrakoluncheva@gmail.com";
    public static final String MOCK_EMPLOYEE_USERNAME = "alexandra";
    public static final String MOCK_EMPLOYEE_PASSWORD = "12345678";

    public static final String MOCK_EMPLOYEE_DETAILS_ID = "5e85284b-9399-11eb-ad6a-9829a64392c7";
    public static final String MOCK_EMPLOYEE_DETAILS_FIRST_NAME = "Alexandra";
    public static final String MOCK_EMPLOYEE_DETAILS_LAST_NAME = "Koluncheva";
    public static final String MOCK_EMPLOYEE_DETAILS_PHONE = "0894610711";

    // MANUFACTURER AND MODEL:
    public static final String MOCK_MANUFACTURER_ID = "19142ae7-8f9e-11eb-8a56-cae72813e33c";
    public static final String MOCK_MANUFACTURER_NAME = "BMW";
    public static final String MOCK_MODEL_ID = "91e97e86-8f8a-11eb-8a56-cae72813e33c";
    public static final String MOCK_MODEL_NAME = "X3";

    // SERVICES:
    public static final String MOCK_SERVICE_ID = "e54f9616-8f98-11eb-8a56-cae72813e33c";
    public static final String MOCK_SERVICE_NAME = "Change oil";
    public static final Double MOCK_PRICE = 30.0;
    public static final Double MOCK_MAX_PRICE = 50.0;


    // VEHICLES:
    public static final String MOCK_VEHICLE_ID = "81c55aaf-8f8c-11eb-8a56-cae72813e33c";
    public static final String MOCK_VIN = "abcdefgh123456789";
    public static final String MOCK_LICENSE_PLATE = "CT1234AV";
    public static final Optional<String> MOCK_LICENSE_PLATE2 = Optional.of("CT1234AV");
    public static final Integer MOCK_YEAR = 1999;

    // VISIT:
    public static final String MOCK_VISIT_ID = "e0187641-8f9c-11eb-8a56-cae72813e33c";
    public static final LocalDate MOCK_START_DATE = LocalDate.ofEpochDay(03 - 04 - 2018);
    public static final LocalDate MOCK_END_DATE = LocalDate.ofEpochDay(03 - 05 - 2018);
    public static final Optional<LocalDate> MOCK_START_DATE2 = Optional.ofNullable(LocalDate.parse("2010-05-03"));
    public static final Optional<LocalDate> MOCK_END_DATE2 = Optional.ofNullable(LocalDate.parse("2010-05-03"));

    public static final Optional<String> MOCK_START_DATE_STR = Optional.ofNullable("2010-05-03");
    public static final Optional<String> MOCK_END_DATE_STR = Optional.ofNullable("2010-05-03");

    public static final String MOCK_VEHICLE_SEARCH_PARAMETER_ID = "4bb2fa86-9399-11eb-ad6a-9829a64392c7";
}
