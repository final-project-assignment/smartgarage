package com.smartGarage.helpers;

import com.smartGarage.models.*;
import com.smartGarage.models.DTObjects.search.VehicleSearchParameters;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Helpers {

    public static Role createMockEmployeeRole() {
        var mockRoleEmployee = new Role();

        mockRoleEmployee.setRoleId(CONSTANTS.MOCK_EMPLOYEE_ROLE_ID);
        mockRoleEmployee.setType("employee");

        return mockRoleEmployee;
    }

    public static Role createMockCustomerRole() {
        var mockRoleEmployee = new Role();

        mockRoleEmployee.setRoleId(CONSTANTS.MOCK_CUSTOMER_ROLE_ID);
        mockRoleEmployee.setType("customer");

        return mockRoleEmployee;
    }

    public static Manufacturer createMockManufacturer() {
        var mockManufacturer = new Manufacturer();

        mockManufacturer.setManufacturerId(CONSTANTS.MOCK_MANUFACTURER_ID);
        mockManufacturer.setManufactureName(CONSTANTS.MOCK_MANUFACTURER_NAME);

        return mockManufacturer;
    }

    public static Model createMockModel() {
        var mockModel = new Model();

        mockModel.setModelId(CONSTANTS.MOCK_MODEL_ID);
        mockModel.setModelName(CONSTANTS.MOCK_MODEL_NAME);
        mockModel.setManufacturer(createMockManufacturer());

        return mockModel;
    }

    public static List<User> createMockUserList() {
        var mockUserList = new ArrayList<User>();
        var mockUser = new User();

        mockUser.setUserId(CONSTANTS.MOCK_CUSTOMER_ID);
        mockUser.setUsername(CONSTANTS.MOCK_CUSTOMER_USERNAME);
        mockUser.setPassword(CONSTANTS.MOCK_CUSTOMER_PASSWORD);
        Set<Role> roles = new HashSet<>();
        Role role = createMockCustomerRole();
        roles.add(role);
        mockUser.setRoles(roles);

        mockUserList.add(mockUser);
        mockUser.setUserId("uuid2");
        mockUserList.add(mockUser);
        mockUser.setUserId("uuid3");
        mockUserList.add(mockUser);
        return mockUserList;
    }

    public static List<Manufacturer> createMockManufacturerList() {
        var mockManufacturerList = new ArrayList<Manufacturer>();
        var mockManufacturer = new Manufacturer();

        mockManufacturer.setManufacturerId(CONSTANTS.MOCK_MANUFACTURER_ID);
        mockManufacturer.setManufactureName(CONSTANTS.MOCK_MANUFACTURER_NAME);

        mockManufacturerList.add(mockManufacturer);
        mockManufacturer.setManufacturerId("uuid2");
        mockManufacturerList.add(mockManufacturer);
        mockManufacturer.setManufacturerId("uuid3");
        mockManufacturerList.add(mockManufacturer);
        return mockManufacturerList;
    }

    public static List<Model> createMockModelList() {
        var mockModelList = new ArrayList<Model>();
        var mockModel = new Model();

        mockModel.setModelId(CONSTANTS.MOCK_MODEL_ID);
        mockModel.setModelName(CONSTANTS.MOCK_MODEL_NAME);
        mockModel.setManufacturer(createMockManufacturer());

        mockModelList.add(mockModel);
        mockModel.setModelId("uuid2");
        mockModelList.add(mockModel);
        mockModel.setModelId("uuid3");
        mockModelList.add(mockModel);
        return mockModelList;
    }

    public static List<Visit> createMockVisitList() {
        var mockVisitList = new ArrayList<Visit>();
        var mockVisit = new Visit();
        var mockVisitDiff = new Visit();

        mockVisit.setVisitId(CONSTANTS.MOCK_VISIT_ID);
        mockVisit.setStartDate(CONSTANTS.MOCK_START_DATE);
        mockVisit.setEndDate(CONSTANTS.MOCK_END_DATE);
        mockVisit.setVehicle(createMockVehicle());
        mockVisit.setAutoServices(createMockAutoServiceList());

        mockVisitDiff.setVisitId("uuidDiff");
        mockVisitDiff.setStartDate(CONSTANTS.MOCK_START_DATE);
        mockVisitDiff.setEndDate(CONSTANTS.MOCK_START_DATE);
        mockVisitDiff.setVehicle(createMockVehicle());
        mockVisitDiff.setAutoServices(createMockAutoServiceList());

        mockVisitList.add(mockVisit);
        mockVisit.setVisitId("uuid2");
        mockVisitList.add(mockVisit);
        mockVisit.setVisitId("uuid3");
        mockVisitList.add(mockVisit);
        mockVisitList.add(mockVisitDiff);
        return mockVisitList;
    }

    public static List<Vehicle> createMockVehicleList() {

        var mockVehicleList = new ArrayList<Vehicle>();
        var mockVehicle = new Vehicle();
        var mockVehicleDiff = new Vehicle();

        mockVehicle.setVehicleId(CONSTANTS.MOCK_VEHICLE_ID);
        mockVehicle.setYear(CONSTANTS.MOCK_YEAR);
        mockVehicle.setOwner(createMockUserCustomer());
        mockVehicle.setModel(createMockModel());
        mockVehicle.setVIN(CONSTANTS.MOCK_VIN);
        mockVehicle.setLicensePlate(CONSTANTS.MOCK_LICENSE_PLATE);

        mockVehicleDiff.setVehicleId("uuidDiff");
        mockVehicleDiff.setYear(CONSTANTS.MOCK_YEAR);
        mockVehicleDiff.setOwner(createMockUserCustomer());
        mockVehicleDiff.setModel(createMockModel());
        mockVehicleDiff.setVIN(CONSTANTS.MOCK_VIN);
        mockVehicleDiff.setLicensePlate(CONSTANTS.MOCK_LICENSE_PLATE);

        mockVehicleList.add(mockVehicle);
        mockVehicle.setVehicleId("uuid2");
        mockVehicleList.add(mockVehicle);
        mockVehicle.setVehicleId("uuid3");
        mockVehicleList.add(mockVehicle);
        mockVehicleList.add(mockVehicleDiff);

        return mockVehicleList;
    }

    public static List<AutoService> createMockAutoServiceList() {

        var mockAutoServiceList = new ArrayList<AutoService>();
        var mockAutoService = new AutoService();
        var mockAutoServiceDiff = new AutoService();

        mockAutoService.setServiceId(CONSTANTS.MOCK_SERVICE_ID);
        mockAutoService.setName(CONSTANTS.MOCK_SERVICE_NAME);
        mockAutoService.setPrice(CONSTANTS.MOCK_PRICE);


        mockAutoServiceDiff.setServiceId("uuidDiff");
        mockAutoServiceDiff.setName(CONSTANTS.MOCK_SERVICE_NAME);
        mockAutoServiceDiff.setPrice(CONSTANTS.MOCK_PRICE);

        mockAutoServiceList.add(mockAutoService);
        mockAutoService.setServiceId("uuid2");
        mockAutoServiceList.add(mockAutoService);
        mockAutoService.setServiceId("uuid3");
        mockAutoServiceList.add(mockAutoService);
        mockAutoServiceList.add(mockAutoServiceDiff);

        return mockAutoServiceList;
    }

    public static List<UserDetails> createMockUserDetailsList() {

        var mockUserDetailsList = new ArrayList<UserDetails>();
        var mockUserDetails = new UserDetails();
        var mockUserDetailsDiff = new UserDetails();

        mockUserDetails.setUserDetailId(CONSTANTS.MOCK_CUSTOMER_DETAILS_ID);
        mockUserDetails.setFirstName(CONSTANTS.MOCK_CUSTOMER_DETAILS_FIRST_NAME);
        mockUserDetails.setLastName(CONSTANTS.MOCK_CUSTOMER_DETAILS_LAST_NAME);
        mockUserDetails.setPhone(CONSTANTS.MOCK_CUSTOMER_DETAILS_PHONE);
        mockUserDetails.setUser(Helpers.createMockUserCustomer());


        mockUserDetailsDiff.setUserDetailId("uuidDiff");
        mockUserDetailsDiff.setFirstName(CONSTANTS.MOCK_CUSTOMER_DETAILS_FIRST_NAME);
        mockUserDetailsDiff.setLastName(CONSTANTS.MOCK_CUSTOMER_DETAILS_LAST_NAME);
        mockUserDetailsDiff.setPhone(CONSTANTS.MOCK_CUSTOMER_DETAILS_PHONE);
        mockUserDetailsDiff.setUser(Helpers.createMockUserCustomer());

        mockUserDetailsList.add(mockUserDetails);
        mockUserDetails.setUserDetailId("uuid2");
        mockUserDetailsList.add(mockUserDetails);
        mockUserDetails.setUserDetailId("uuid3");
        mockUserDetailsList.add(mockUserDetails);
        mockUserDetailsList.add(mockUserDetailsDiff);

        return mockUserDetailsList;
    }


    public static AutoService createMockAutoService() {
        var mockAutoService = new AutoService();

        mockAutoService.setServiceId(CONSTANTS.MOCK_SERVICE_ID);
        mockAutoService.setName(CONSTANTS.MOCK_SERVICE_NAME);
        mockAutoService.setPrice(30.0);

        return mockAutoService;
    }

    public static User createMockUserEmployee() {
        var mockUserEmployee = new User();

        mockUserEmployee.setUserId(CONSTANTS.MOCK_EMPLOYEE_ID);
        mockUserEmployee.setEmail(CONSTANTS.MOCK_EMPLOYEE_EMAIL);
        mockUserEmployee.setUsername(CONSTANTS.MOCK_EMPLOYEE_USERNAME);
        mockUserEmployee.setPassword(CONSTANTS.MOCK_EMPLOYEE_PASSWORD);
        mockUserEmployee.setRoles(Set.of(createMockEmployeeRole()));

        return mockUserEmployee;
    }

    public static UserDetails createMockUserDetailsEmployee() {
        var mockUserDetailsEmployee = new UserDetails();

        mockUserDetailsEmployee.setUser(createMockUserEmployee());
        mockUserDetailsEmployee.setUserDetailId(CONSTANTS.MOCK_EMPLOYEE_DETAILS_ID);
        mockUserDetailsEmployee.setPhone(CONSTANTS.MOCK_EMPLOYEE_DETAILS_PHONE);
        mockUserDetailsEmployee.setFirstName(CONSTANTS.MOCK_EMPLOYEE_DETAILS_FIRST_NAME);
        mockUserDetailsEmployee.setLastName(CONSTANTS.MOCK_EMPLOYEE_DETAILS_LAST_NAME);

        return mockUserDetailsEmployee;
    }

    public static User createMockUserCustomerNew() {
        var mockUserCustomer = new User();

        mockUserCustomer.setUserId(CONSTANTS.MOCK_CUSTOMER_ID_NEW);
        mockUserCustomer.setEmail(CONSTANTS.MOCK_CUSTOMER_EMAIL_NEW);
        mockUserCustomer.setUsername(CONSTANTS.MOCK_CUSTOMER_USERNAME_NEW);
        mockUserCustomer.setPassword(CONSTANTS.MOCK_CUSTOMER_PASSWORD_NEW);

        mockUserCustomer.setRoles(Set.of(createMockCustomerRole()));

        return mockUserCustomer;
    }

    public static User createMockUserCustomer() {
        var mockUserCustomer = new User();

        mockUserCustomer.setUserId(CONSTANTS.MOCK_CUSTOMER_ID);
        mockUserCustomer.setEmail(CONSTANTS.MOCK_CUSTOMER_EMAIL);
        mockUserCustomer.setUsername(CONSTANTS.MOCK_CUSTOMER_USERNAME);
        mockUserCustomer.setPassword(CONSTANTS.MOCK_CUSTOMER_PASSWORD);

        mockUserCustomer.setRoles(Set.of(createMockCustomerRole()));

        return mockUserCustomer;
    }

    public static UserDetails createMockUserDetailsCustomer() {
        var MockUserDetailsCustomer = new UserDetails();

        MockUserDetailsCustomer.setUser(createMockUserCustomer());
        MockUserDetailsCustomer.setUserDetailId(CONSTANTS.MOCK_CUSTOMER_DETAILS_ID);
        MockUserDetailsCustomer.setPhone(CONSTANTS.MOCK_CUSTOMER_DETAILS_PHONE);
        MockUserDetailsCustomer.setFirstName(CONSTANTS.MOCK_CUSTOMER_DETAILS_FIRST_NAME);
        MockUserDetailsCustomer.setLastName(CONSTANTS.MOCK_CUSTOMER_DETAILS_LAST_NAME);

        return MockUserDetailsCustomer;
    }

    public static Vehicle createMockVehicle() {
        var mockVehicle = new Vehicle();

        mockVehicle.setVehicleId(CONSTANTS.MOCK_VEHICLE_ID);
        mockVehicle.setModel(createMockModel());
        mockVehicle.setLicensePlate(CONSTANTS.MOCK_LICENSE_PLATE);
        mockVehicle.setOwner(createMockUserCustomer());
        mockVehicle.setVIN(CONSTANTS.MOCK_VIN);
        mockVehicle.setYear(CONSTANTS.MOCK_YEAR);

        return mockVehicle;
    }

    public static Visit createMockVisit() {
        var mockVisit = new Visit();

        mockVisit.setVisitId(CONSTANTS.MOCK_VISIT_ID);
        mockVisit.setVehicle(createMockVehicle());
        mockVisit.setStartDate(CONSTANTS.MOCK_START_DATE);
        mockVisit.setEndDate(CONSTANTS.MOCK_END_DATE);
        mockVisit.setAutoServices(List.of(createMockAutoService()));

        return mockVisit;
    }

    public static VehicleSearchParameters createMockSearchParam() {
        var mockParam = new VehicleSearchParameters();

        mockParam.setCustomerId(CONSTANTS.MOCK_VEHICLE_SEARCH_PARAMETER_ID);

        return mockParam;
    }

    public static AutoServiceFilterParams createMockFilterParam() {
        var mockParam = new AutoServiceFilterParams();

        mockParam.setName(CONSTANTS.MOCK_SERVICE_NAME);
        mockParam.setMinPrice(CONSTANTS.MOCK_PRICE);
        mockParam.setMaxPrice(CONSTANTS.MOCK_MAX_PRICE);

        return mockParam;
    }
}
