package com.smartGarage.services;

import com.smartGarage.exceptions.UnauthorizedOperationException;
import com.smartGarage.helpers.CONSTANTS;
import com.smartGarage.helpers.Helpers;
import com.smartGarage.models.User;
import com.smartGarage.models.Vehicle;
import com.smartGarage.repositories.contracts.VehicleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.smartGarage.helpers.Helpers.createMockUserCustomer;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class VehicleServiceImplTests {

    @Mock
    VehicleRepository repository;

    @InjectMocks
    VehicleServiceImpl service;

    static User employeeUser;
    static User customerUser;

    @BeforeAll
    static void init() {
        employeeUser = Helpers.createMockUserEmployee();
        customerUser = createMockUserCustomer();
    }

    @Test
    public void getAll_Should_Return_Visits_When_MatchExist() {
        // Arrange:
        List<Vehicle> mockList = Helpers.createMockVehicleList();

        Mockito.when(repository.getAll()).thenReturn(mockList);

        // Act
        List<Vehicle> result = service.getAll(employeeUser);

        // Assert
        Assertions.assertEquals(mockList, result);
    }

    @Test
    public void getByID_should_Return_Vehicle() {

        Vehicle mockVehicle = Helpers.createMockVehicle();
        when(repository.getByID(CONSTANTS.MOCK_VEHICLE_ID)).thenReturn(mockVehicle);

        // Act
        Vehicle result = service.getByID(employeeUser, CONSTANTS.MOCK_VEHICLE_ID);

        // Assert
        Assertions.assertEquals(mockVehicle, result);
    }

    @Test
    public void update_should_Throw_When_Customer() {
        Vehicle mockVehicle = Helpers.createMockVehicle();

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, ()
                -> service.update(customerUser, mockVehicle));
    }

    @Test
    public void update_Should_Call_Repository_When_Updating_Vehicle() {
        // Arrange
        Vehicle mockVehicle = Helpers.createMockVehicle();

        // Act
        service.update(employeeUser, mockVehicle);

        // Assert
        verify(repository, times(1)).update(mockVehicle);
    }

    @Test
    public void create_Should_Call_Repository() {
        Vehicle mockVehicle = Helpers.createMockVehicle();

        service.create(employeeUser, mockVehicle);

        // Act, Assert
        verify(repository, times(1)).create(mockVehicle);
    }

    @Test
    public void Create_Should_Create_New_Vehicle() {
        // Arrange:
        Mockito.when(repository.getByID(CONSTANTS.MOCK_VEHICLE_ID))
                .thenReturn(new Vehicle(CONSTANTS.MOCK_VEHICLE_ID,
                        CONSTANTS.MOCK_VIN,
                        CONSTANTS.MOCK_LICENSE_PLATE,
                        CONSTANTS.MOCK_YEAR,
                        createMockUserCustomer(),
                        Helpers.createMockModel()));

        // Act
        Vehicle result = service.getByID(Helpers.createMockUserEmployee(), CONSTANTS.MOCK_VEHICLE_ID);

        // Assert
        Assertions.assertEquals(CONSTANTS.MOCK_VEHICLE_ID, result.getVehicleId());
        Assertions.assertEquals(CONSTANTS.MOCK_MODEL_ID, result.getModel().getModelId());
        Assertions.assertEquals(CONSTANTS.MOCK_LICENSE_PLATE, result.getLicensePlate());
        Assertions.assertEquals(CONSTANTS.MOCK_CUSTOMER_ID, result.getOwner().getUserId());
        Assertions.assertEquals(CONSTANTS.MOCK_VIN, result.getVIN());
        Assertions.assertEquals(CONSTANTS.MOCK_YEAR, result.getYear());
    }

    @Test
    public void delete_Should_Call_Repository_When_Delete_Vehicle() {
        // Act
        service.delete(employeeUser, CONSTANTS.MOCK_VEHICLE_ID);

        // Assert
        verify(repository, times(1))
                .delete(CONSTANTS.MOCK_VEHICLE_ID);
    }

    @Test
    public void filterByOwner_Should_Return_Filtered_List() {
        // Arrange:
        List<Vehicle> vehicles = new ArrayList<>();
        vehicles.add(Helpers.createMockVehicle());

        Mockito.when(repository.filterByOwner(CONSTANTS.MOCK_CUSTOMER_ID))
                .thenReturn(vehicles);

        // Act:
        List<Vehicle> filteredList = service.filterByOwner(Helpers.createMockUserEmployee(),
                createMockUserCustomer().getUserId());

        // Assert:
        Assertions.assertEquals(filteredList, vehicles);
    }

    @Test
    public void filter_By_Vehicle_Parameters_Should_Call_Repository() {
        var mockSearchParam = Helpers.createMockSearchParam();

        service.filter(mockSearchParam);

        // Act, Assert
        verify(repository, times(1)).filter(mockSearchParam);
    }

    @Test
    public void filterByCustomer_Should_Return_Filtered_List() {
        // Arrange:
        List<Vehicle> vehicles = new ArrayList<>();
        vehicles.add(Helpers.createMockVehicle());

        Mockito.when(repository.filterByCustomer(CONSTANTS.MOCK_CUSTOMER_ID))
                .thenReturn(vehicles);

        // Act:
        List<Vehicle> filteredList = service.filterByCustomer(createMockUserCustomer(),
                createMockUserCustomer().getUserId());

        // Assert:
        Assertions.assertEquals(filteredList, vehicles);
    }

    @Test
    public void GetCount_should_Call_Repository() {
        service.getCount();

        verify(repository, times(1)).getCount();
    }
}
