package com.smartGarage.services;

import com.smartGarage.exceptions.UnauthorizedOperationException;
import com.smartGarage.helpers.CONSTANTS;
import com.smartGarage.helpers.Helpers;
import com.smartGarage.models.AutoService;
import com.smartGarage.models.User;
import com.smartGarage.repositories.contracts.AutoServiceRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AutoServiceServiceImplTests {

    @Mock
    AutoServiceRepository repository;

    @InjectMocks
    AutoServiceServiceImpl service;

    static User employeeUser;
    static User customerUser;

    @BeforeAll
    static void init() {
        employeeUser = Helpers.createMockUserEmployee();
        customerUser = Helpers.createMockUserCustomer();
    }

    @Test
    public void getAll_Should_Return_Visits_When_MatchExist() {
        // Arrange:
        List<AutoService> mockList = Helpers.createMockAutoServiceList();

        Mockito.when(repository.getAll()).thenReturn(mockList);

        // Act
        List<AutoService> result = service.getAll(employeeUser);

        // Assert
        Assertions.assertEquals(mockList, result);
    }

    @Test
    public void getAll_Should_Return_Visits_When_MatchExist_And_No_User() {
        // Arrange:
        List<AutoService> mockList = Helpers.createMockAutoServiceList();

        Mockito.when(repository.getAll()).thenReturn(mockList);

        // Act
        List<AutoService> result = service.getAll();

        // Assert
        Assertions.assertEquals(mockList, result);
    }

    @Test
    public void getByID_should_Return_Vehicle() {

        AutoService mockAutoService = Helpers.createMockAutoService();
        when(repository.getByID(CONSTANTS.MOCK_SERVICE_ID)).thenReturn(mockAutoService);

        // Act
        AutoService result = service.getByID(employeeUser, CONSTANTS.MOCK_SERVICE_ID);

        // Assert
        Assertions.assertEquals(mockAutoService, result);
    }

    @Test
    public void delete_Should_Call_Repository_When_Called() {
        // Act
        service.delete(employeeUser, CONSTANTS.MOCK_SERVICE_ID);

        // Assert
        verify(repository, times(1))
                .delete(CONSTANTS.MOCK_SERVICE_ID);
    }

    @Test
    public void delete_should_Throw_When_Customer() {
        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, ()
                -> service.delete(customerUser, CONSTANTS.MOCK_SERVICE_ID));
    }

    @Test
    public void Update_should_Throw_When_Customer() {
        AutoService mockAutoService = Helpers.createMockAutoService();

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, ()
                -> service.update(customerUser, mockAutoService));
    }

    @Test
    public void update_Should_Call_Repository_When_Updating_Vehicle() {
        // Arrange
        AutoService mockAutoService = Helpers.createMockAutoService();

        // Act
        service.update(employeeUser, mockAutoService);

        // Assert
        verify(repository, times(1)).update(mockAutoService);
    }

    @Test
    public void Create_Should_Call_Repository() {
        AutoService mockAutoService = Helpers.createMockAutoService();

        service.create(employeeUser, mockAutoService);

        // Act, Assert
        verify(repository, times(1)).create(mockAutoService);
    }

    @Test
    public void Create_Should_Create_New_AutoService() {
        // Arrange:
        Mockito.when(repository.getByID(CONSTANTS.MOCK_SERVICE_ID))
                .thenReturn(new AutoService(CONSTANTS.MOCK_SERVICE_ID,
                        CONSTANTS.MOCK_SERVICE_NAME,
                        CONSTANTS.MOCK_PRICE));

        // Act
        AutoService result = service.getByID(Helpers.createMockUserEmployee(), CONSTANTS.MOCK_SERVICE_ID);

        // Assert
        Assertions.assertEquals(CONSTANTS.MOCK_SERVICE_ID, result.getServiceId());
        Assertions.assertEquals(CONSTANTS.MOCK_SERVICE_NAME, result.getName());
        Assertions.assertEquals(CONSTANTS.MOCK_PRICE, result.getPrice());
    }

    @Test
    public void Create_should_Throw_When_Customer() {
        AutoService mockAutoService = Helpers.createMockAutoService();

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, ()
                -> service.create(customerUser, mockAutoService));
    }

    @Test
    public void filter_Should_Return_Filtered_List() {
        // Arrange:
        List<AutoService> autoServices = new ArrayList<>();
        autoServices.add(Helpers.createMockAutoService());

        Mockito.when(repository.filter(
                java.util.Optional.of(CONSTANTS.MOCK_CUSTOMER_ID),
                java.util.Optional.of(CONSTANTS.MOCK_PRICE),
                java.util.Optional.of(CONSTANTS.MOCK_MAX_PRICE)))
                .thenReturn(autoServices);

        // Act:
        List<AutoService> filteredList = service.filter(employeeUser,
                java.util.Optional.of(CONSTANTS.MOCK_CUSTOMER_ID),
                java.util.Optional.of(CONSTANTS.MOCK_PRICE),
                java.util.Optional.of(CONSTANTS.MOCK_MAX_PRICE));

        // Assert:
        Assertions.assertEquals(filteredList, autoServices);
    }

    @Test
    public void filter_should_Throw_When_Customer() {
        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, ()
                -> service.filter(customerUser,
                java.util.Optional.of(CONSTANTS.MOCK_CUSTOMER_ID),
                java.util.Optional.of(CONSTANTS.MOCK_PRICE),
                java.util.Optional.of(CONSTANTS.MOCK_MAX_PRICE)));
    }

    @Test
    public void GetCount_should_Call_Repository() {
        service.getCount();

        verify(repository, times(1)).getCount();
    }

    @Test
    public void Filter_By_All_Params_should_Call_Repository() {
        service.filter(Helpers.createMockFilterParam());

        verify(repository, times(1)).filter(Helpers.createMockFilterParam());
    }
}
