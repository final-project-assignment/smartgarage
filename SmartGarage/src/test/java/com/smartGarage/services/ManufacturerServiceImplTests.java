package com.smartGarage.services;

import com.smartGarage.exceptions.UnauthorizedOperationException;
import com.smartGarage.helpers.CONSTANTS;
import com.smartGarage.helpers.Helpers;
import com.smartGarage.models.Manufacturer;
import com.smartGarage.models.Model;
import com.smartGarage.models.User;
import com.smartGarage.repositories.contracts.ManufacturerRepository;
import com.smartGarage.repositories.contracts.ModelRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ManufacturerServiceImplTests {

    @Mock
    ManufacturerRepository mockRepository;

    @InjectMocks
    ManufacturerServiceImpl service;

    static User employeeUser;
    static User customerUser;

    @BeforeAll
    static void init() {
        employeeUser = Helpers.createMockUserEmployee();
        customerUser = Helpers.createMockUserCustomer();
    }

    @Test
    public void getAll_Should_Return_Manufacturers_When_MatchExist() {
        // Arrange:
        List<Manufacturer> mockList = Helpers.createMockManufacturerList();

        Mockito.when(mockRepository.getAll()).thenReturn(mockList);

        // Act
        List<Manufacturer> result = service.getAll(employeeUser);

        // Assert
        Assertions.assertEquals(mockList, result);
    }

    @Test
    public void getAll_Should_Return_Manufacturers_When_No_Employee() {
        // Arrange:
        List<Manufacturer> mockList = Helpers.createMockManufacturerList();

        Mockito.when(mockRepository.getAll()).thenReturn(mockList);

        // Act
        List<Manufacturer> result = service.getAll();

        // Assert
        Assertions.assertEquals(mockList, result);
    }

    @Test
    public void getByID_should_Return_Manufacturer() {
        String mockId = "111";
        Manufacturer mockManufacturer = Helpers.createMockManufacturer();
        when(mockRepository.getByID(mockId)).thenReturn(mockManufacturer);

        // Act
        Manufacturer result = service.getByID(employeeUser, mockId);

        // Assert
        Assertions.assertEquals(mockManufacturer, result);
    }

    @Test
    public void getAll_should_Throw_When_Customer() {
        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.getAll(customerUser));
    }

    @Test
    public void getByID_should_Throw_When_Customer() {
        String mockId = "11";

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.getByID(customerUser, mockId));
    }

    @Test
    public void Create_should_Call_Repository_When_Called() {
        Manufacturer mockManufacturer = Helpers.createMockManufacturer();

        service.create(employeeUser, mockManufacturer);

        // Act, Assert
        verify(mockRepository, times(1)).create(mockManufacturer);
    }

    @Test
    public void Create_should_Throw_When_Customer() {
        Manufacturer mockManufacturer = Helpers.createMockManufacturer();

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.create(customerUser, mockManufacturer));
    }

    @Test
    public void update_Should_Call_Repository_When_Updating_Manufacturer() {
        // Arrange
        Manufacturer mockManufacturer = Helpers.createMockManufacturer();

        // Act
        service.update(employeeUser, mockManufacturer);

        // Assert
        verify(mockRepository, times(1)).update(mockManufacturer);
    }

    @Test
    public void Update_should_Throw_When_Customer() {
        Manufacturer mockManufacturer = Helpers.createMockManufacturer();

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.update(customerUser, mockManufacturer));
    }

    @Test
    public void delete_Should_Call_Repository_When_Deleting_Model() {
        // Arrange
        String mockId = CONSTANTS.MOCK_MANUFACTURER_ID;

        // Act
        service.delete(employeeUser, mockId);

        // Assert
        verify(mockRepository, times(1)).delete(mockId);
    }

    @Test
    public void Delete_should_Throw_When_Customer() {
        // Arrange
        String mockId = CONSTANTS.MOCK_MANUFACTURER_ID;

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.delete(customerUser, mockId));
    }
}
