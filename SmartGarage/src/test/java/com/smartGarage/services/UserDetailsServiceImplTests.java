package com.smartGarage.services;

import com.smartGarage.exceptions.DuplicateEntityException;
import com.smartGarage.exceptions.EntityNotFoundException;
import com.smartGarage.helpers.CONSTANTS;
import com.smartGarage.helpers.Helpers;
import com.smartGarage.models.User;
import com.smartGarage.models.UserDetails;
import com.smartGarage.repositories.contracts.UserDetailRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.smartGarage.helpers.CONSTANTS.MOCK_CUSTOMER_ID;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserDetailsServiceImplTests {

    @Mock
    UserDetailRepository repository;

    @InjectMocks
    UserDetailsServiceImpl service;

    static User employeeUser;
    static User customerUser;

    @BeforeAll
    static void init() {
        employeeUser = Helpers.createMockUserEmployee();
        customerUser = Helpers.createMockUserCustomer();
    }

    @Test
    public void getAll_Should_Return_UserDetails_When_MatchExist() {
        // Arrange:
        List<UserDetails> mockList = Helpers.createMockUserDetailsList();

        Mockito.when(repository.getAll()).thenReturn(mockList);

        // Act
        List<UserDetails> result = service.getAll();

        // Assert
        Assertions.assertEquals(mockList, result);
    }

    @Test
    public void getByID_should_Return_UserDetails() {

        UserDetails mockUserDetails = Helpers.createMockUserDetailsCustomer();
        when(repository.getByID(CONSTANTS.MOCK_CUSTOMER_DETAILS_ID)).thenReturn(mockUserDetails);

        // Act
        UserDetails result = service.getByID(CONSTANTS.MOCK_CUSTOMER_DETAILS_ID);

        // Assert
        Assertions.assertEquals(mockUserDetails, result);
    }

    @Test
    public void create_Should_Throw_When_CustomerWithSameIDExists() {
        // Arrange
        var mockCustomer = Helpers.createMockUserDetailsCustomer();

        Mockito.when(repository.getByID(mockCustomer.getUserDetailId()))
                .thenReturn(mockCustomer);

        // Act, Assert
        Assertions.assertThrows(
                DuplicateEntityException.class,
                () -> service.create(mockCustomer)
        );
    }

    @Test
    public void Create_Should_Create_New_Customer() {
        // Arrange:
        Mockito.when(repository.getByID(CONSTANTS.MOCK_CUSTOMER_DETAILS_ID))
                .thenReturn(new UserDetails(CONSTANTS.MOCK_CUSTOMER_DETAILS_ID,
                        CONSTANTS.MOCK_CUSTOMER_DETAILS_FIRST_NAME,
                        CONSTANTS.MOCK_CUSTOMER_DETAILS_LAST_NAME,
                        CONSTANTS.MOCK_CUSTOMER_DETAILS_PHONE,
                        Helpers.createMockUserCustomer()));

        // Act
        UserDetails result = service.getByID(CONSTANTS.MOCK_CUSTOMER_DETAILS_ID);

        // Assert
        Assertions.assertEquals(CONSTANTS.MOCK_CUSTOMER_DETAILS_ID, result.getUserDetailId());
        Assertions.assertEquals(CONSTANTS.MOCK_CUSTOMER_DETAILS_FIRST_NAME, result.getFirstName());
        Assertions.assertEquals(CONSTANTS.MOCK_CUSTOMER_DETAILS_LAST_NAME, result.getLastName());
        Assertions.assertEquals(CONSTANTS.MOCK_CUSTOMER_DETAILS_PHONE, result.getPhone());
        Assertions.assertEquals(CONSTANTS.MOCK_CUSTOMER_ID, result.getUser().getUserId());
    }

    @Test
    public void Create_should_Call_Repository() {
        UserDetails userDetails = Helpers.createMockUserDetailsCustomer();

        when(repository.getByID(userDetails.getUserDetailId())).thenThrow(EntityNotFoundException.class);

        service.create(userDetails);

        verify(repository, times(1)).create(userDetails);
    }

    @Test
    public void update_Should_Call_Repository_When_Updating_UserDetails() {
        // Arrange
        UserDetails userDetails = Helpers.createMockUserDetailsCustomer();

        // Act
        service.update(userDetails);

        // Assert
        verify(repository, times(1)).update(userDetails);
    }

    @Test
    public void delete_Should_Call_Repository_When_Delete_User() {
        // Act
        service.delete(MOCK_CUSTOMER_ID);

        // Assert
        verify(repository, times(1)).delete(MOCK_CUSTOMER_ID);
    }

    @Test
    public void filter_By_Customer_Should_Call_Repository() {
        service.filterByCustomer(customerUser.getUserId());

        verify(repository, times(1)).filterByCustomer(customerUser.getUserId());
    }

    @Test
    public void search_Should_Return_Filtered_List() {
        // Arrange:
        List<UserDetails> userDetails = new ArrayList<>();
        userDetails.add(Helpers.createMockUserDetailsCustomer());

        Mockito.when(repository.search(java.util.Optional.of(CONSTANTS.MOCK_CUSTOMER_DETAILS_FIRST_NAME),
                java.util.Optional.of(CONSTANTS.MOCK_CUSTOMER_DETAILS_LAST_NAME),
                java.util.Optional.of(CONSTANTS.MOCK_CUSTOMER_EMAIL),
                java.util.Optional.of(CONSTANTS.MOCK_CUSTOMER_DETAILS_PHONE),
                java.util.Optional.of(CONSTANTS.MOCK_VEHICLE_ID)))
                .thenReturn(userDetails);

        // Act:
        List<UserDetails> filteredList = service.search(Helpers.createMockUserEmployee(),
                java.util.Optional.of(CONSTANTS.MOCK_CUSTOMER_DETAILS_FIRST_NAME),
                java.util.Optional.of(CONSTANTS.MOCK_CUSTOMER_DETAILS_LAST_NAME),
                java.util.Optional.of(CONSTANTS.MOCK_CUSTOMER_EMAIL),
                java.util.Optional.of(CONSTANTS.MOCK_CUSTOMER_DETAILS_PHONE),
                java.util.Optional.of(CONSTANTS.MOCK_VEHICLE_ID));

        // Assert:
        Assertions.assertEquals(filteredList.toString(), userDetails.toString());
    }


    @Test
    public void sort_Should_Return_Filtered_List() {
        // Arrange:
        List<UserDetails> userDetails = new ArrayList<>();
        userDetails.add(Helpers.createMockUserDetailsCustomer());

        Mockito.when(repository.sort())
                .thenReturn(userDetails);

        // Act:
        List<UserDetails> sortedList = service.sort(Helpers.createMockUserEmployee());

        // Assert:
        Assertions.assertEquals(sortedList, userDetails);
    }

    @Test
    public void GetCount_should_Call_Repository() {
        service.getCount();

        verify(repository, times(1)).getCount();
    }
}
