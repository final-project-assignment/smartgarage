package com.smartGarage.services;

import com.smartGarage.exceptions.DuplicateEntityException;
import com.smartGarage.exceptions.EntityNotFoundException;
import com.smartGarage.helpers.CONSTANTS;
import com.smartGarage.helpers.Helpers;
import com.smartGarage.models.User;
import com.smartGarage.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static com.smartGarage.helpers.CONSTANTS.MOCK_CUSTOMER_ID;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository mockRepository;

    @InjectMocks
    UserServiceImpl service;

    static User employeeUser;
    static User customerUser;

    @BeforeAll
    static void init() {
        employeeUser = Helpers.createMockUserEmployee();
        customerUser = Helpers.createMockUserCustomer();
    }

    @Test
    public void getAll_Should_Return_User_When_MatchExist() {
        // Arrange:
        List<User> mockList = Helpers.createMockUserList();

        Mockito.when(mockRepository.getAll()).thenReturn(mockList);

        // Act
        List<User> result = service.getAll();

        // Assert
        Assertions.assertEquals(mockList, result);
    }

    @Test
    public void getByID_should_Return_User() {
        User mockUser = Helpers.createMockUserCustomer();
        when(mockRepository.getByID(MOCK_CUSTOMER_ID)).thenReturn(mockUser);

        // Act
        User result = service.getByID(MOCK_CUSTOMER_ID);

        // Assert
        Assertions.assertEquals(mockUser, result);
    }

    @Test
    public void Create_should_Call_Repository_When_Called() {
        var mockUser = Helpers.createMockUserCustomerNew();

        when(mockRepository.getByEmail(mockUser.getEmail())).thenThrow(EntityNotFoundException.class);

        service.create(mockUser);

        verify(mockRepository, times(1)).create(mockUser);
    }

    @Test
    public void create_Should_Throw_When_CustomerWithSameEmailExists() {
        // Arrange
        User mockCustomer = Helpers.createMockUserCustomer();

        // Act, Assert
        Assertions.assertThrows(
                DuplicateEntityException.class,
                () -> service.create(mockCustomer)
        );
    }

    @Test
    public void Update_should_Call_Repository_When_Called() {
        var mockUser = Helpers.createMockUserCustomerNew();

        service.update(mockUser);

        // Act, Assert
        verify(mockRepository, times(1)).update(mockUser);
    }

    @Test
    public void delete_Should_Call_Repository_When_Delete_User() {
        // Act
        service.delete(MOCK_CUSTOMER_ID);

        // Assert
        verify(mockRepository, times(1)).delete(MOCK_CUSTOMER_ID);
    }

    @Test
    public void Get_By_Email_Should_Return_Filtered_List() {
        // Arrange:
        User user = Helpers.createMockUserCustomer();

        Mockito.when(mockRepository.getByEmail(CONSTANTS.MOCK_CUSTOMER_EMAIL)).thenReturn(user);

        // Act:
        User answer = service.getByEmail(CONSTANTS.MOCK_CUSTOMER_EMAIL);

        // Assert:
        Assertions.assertEquals(answer, user);
    }

    @Test
    public void Get_Reset_Password_Token_should_Call_Repository_When_Called() {
        service.getByResetPasswordToken("test");

        verify(mockRepository, times(1)).findByResetPasswordToken("test");
    }

    @Test
    public void Update_Password_should_Call_Repository_When_Called() {
        service.updatePassword(customerUser,"test");

        // Act, Assert
        verify(mockRepository, times(1)).update(customerUser);
    }

    @Test
    public void When_No_Existing_Password_Throw_Exception() {
        Mockito.when(mockRepository.getByEmail(customerUser.getEmail())).thenReturn(null);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class, ()->service.updateResetPasswordToken("test",customerUser.getEmail()));
    }

    @Test
    public void Update_Reset_Password_Token_should_Call_Repository_When_Called() {
        Mockito.when(mockRepository.getByEmail(customerUser.getEmail())).thenReturn(customerUser);

        service.updateResetPasswordToken("test",customerUser.getEmail());

        verify(mockRepository, times(1)).update(customerUser);
    }
}
