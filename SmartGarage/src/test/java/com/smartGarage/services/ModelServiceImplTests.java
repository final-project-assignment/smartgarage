package com.smartGarage.services;

import com.smartGarage.exceptions.UnauthorizedOperationException;
import com.smartGarage.helpers.CONSTANTS;
import com.smartGarage.helpers.Helpers;
import com.smartGarage.models.Model;
import com.smartGarage.models.User;
import com.smartGarage.repositories.contracts.ModelRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ModelServiceImplTests {

    @Mock
    ModelRepository mockRepository;

    @InjectMocks
    ModelServiceImpl service;

    static User employeeUser;
    static User customerUser;

    @BeforeAll
    static void init() {
        employeeUser = Helpers.createMockUserEmployee();
        customerUser = Helpers.createMockUserCustomer();
    }

    @Test
    public void getAll_Should_Return_Models_When_MatchExist() {
        // Arrange:
        List<Model> mockList = Helpers.createMockModelList();

        Mockito.when(mockRepository.getAll()).thenReturn(mockList);

        // Act
        List<Model> result = service.getAll(employeeUser);

        // Assert
        Assertions.assertEquals(mockList, result);
    }

    @Test
    public void getAll_Should_Return_Models_When_No_Employee() {
        // Arrange:
        List<Model> mockList = Helpers.createMockModelList();

        Mockito.when(mockRepository.getAll()).thenReturn(mockList);

        // Act
        List<Model> result = service.getAll();

        // Assert
        Assertions.assertEquals(mockList, result);
    }

    @Test
    public void getByID_should_Return_Model() {
        String mockId = "23";
        Model mockModel = Helpers.createMockModel();
        when(mockRepository.getByID(mockId)).thenReturn(mockModel);

        // Act
        Model result = service.getByID(employeeUser, mockId);

        // Assert
        Assertions.assertEquals(mockModel, result);
    }

    @Test
    public void getAll_should_Throw_When_Customer() {
        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.getAll(customerUser));
    }

    @Test
    public void getByID_should_Throw_When_Customer() {
        String mockId = "23";

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.getByID(customerUser, mockId));
    }

    @Test
    public void Create_should_Call_Repository_When_Called() {
        Model mockModel = Helpers.createMockModel();

        service.create(employeeUser, mockModel);

        // Act, Assert
        verify(mockRepository, times(1)).create(mockModel);
    }

    @Test
    public void Create_should_Throw_When_Customer() {
        Model mockModel = Helpers.createMockModel();

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.create(customerUser, mockModel));
    }

    @Test
    public void update_Should_Call_Repository_When_Updating_Model() {
        // Arrange
        Model mockModel = Helpers.createMockModel();

        // Act
        service.update(employeeUser, mockModel);

        // Assert
        verify(mockRepository, times(1)).update(mockModel);
    }

    @Test
    public void Update_should_Throw_When_Customer() {
        Model mockModel = Helpers.createMockModel();

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.update(customerUser, mockModel));
    }

    @Test
    public void delete_Should_Call_Repository_When_Deleting_Model() {
        // Arrange
        String mockId = CONSTANTS.MOCK_MODEL_ID;

        // Act
        service.delete(employeeUser, mockId);

        // Assert
        verify(mockRepository, times(1)).delete(mockId);
    }

    @Test
    public void Delete_should_Throw_When_Customer() {
        // Arrange
        String mockId = CONSTANTS.MOCK_MODEL_ID;

        // Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> service.delete(customerUser, mockId));
    }
}
