package com.smartGarage.services;

import com.smartGarage.helpers.CONSTANTS;
import com.smartGarage.helpers.Helpers;
import com.smartGarage.models.FrontEndVisitModel;
import com.smartGarage.models.User;
import com.smartGarage.models.Vehicle;
import com.smartGarage.models.Visit;
import com.smartGarage.repositories.contracts.VehicleRepository;
import com.smartGarage.repositories.contracts.VisitRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class VisitServiceImplTests {

    @Mock
    VisitRepository mockRepository;
    @Mock
    VehicleRepository vehicleMockRepository;

    @InjectMocks
    VisitServiceImpl service;

    static User employeeUser;
    static User customerUser;

    @BeforeAll
    static void init() {
        employeeUser = Helpers.createMockUserEmployee();
        customerUser = Helpers.createMockUserCustomer();
    }

    @Test
    public void getAll_Should_Return_Visits_When_MatchExist() {
        // Arrange:
        List<Visit> mockList = Helpers.createMockVisitList();

        Mockito.when(mockRepository.getAll()).thenReturn(mockList);

        // Act
        List<Visit> result = service.getAll(employeeUser);

        // Assert
        Assertions.assertEquals(mockList, result);
    }

    @Test
    public void getByID_should_Return_Visit() {
        Visit mockVisit = Helpers.createMockVisit();
        when(mockRepository.getByID(CONSTANTS.MOCK_VISIT_ID)).thenReturn(mockVisit);

        // Act
        Visit result = service.getByID(employeeUser, CONSTANTS.MOCK_VISIT_ID);

        // Assert
        Assertions.assertEquals(mockVisit, result);
    }

    @Test
    public void Create_should_Call_Repository() {
        Visit mockVisit = Helpers.createMockVisit();

        service.create(employeeUser, mockVisit);

        // Act, Assert
        verify(mockRepository, times(1)).create(mockVisit);
    }

    @Test
    public void update_Should_Call_Repository_When_Updating_Visit() {
        // Arrange
        Visit mockVisit = Helpers.createMockVisit();

        // Act
        service.update(employeeUser, mockVisit);

        // Assert
        verify(mockRepository, times(1)).update(mockVisit);
    }

    @Test
    public void delete_Should_Call_Repository_When_Delete_Visit() {
        // Act
        service.delete(employeeUser, CONSTANTS.MOCK_VISIT_ID);

        // Assert
        verify(mockRepository, times(1)).delete(CONSTANTS.MOCK_VISIT_ID);
    }

    @Test
    public void getAllCustomer_Should_Return_Visits_When_Customer() {
        // Arrange:
        List<Visit> mockList = Helpers.createMockVisitList();

        Mockito.when(mockRepository.getAll()).thenReturn(mockList);

        // Act
        List<Visit> result = service.getAllCustomer();

        // Assert
        Assertions.assertEquals(mockList, result);
    }

    @Test
    public void filter_By_Customer_Should_Call_Repository() {
        service.filterByCustomer(customerUser,customerUser.getUserId());

        verify(mockRepository, times(1)).filterByCustomer(customerUser.getUserId());
    }

    @Test
    public void filter_By_CustomerId_should_Return_Filtered_List() {
        var customer = Helpers.createMockUserCustomer();
        var vehicle = Helpers.createMockVehicle();
        List<Visit> visits = Helpers.createMockVisitList();
        List<Vehicle> vehicles = new ArrayList<>();
        vehicles.add(vehicle);

        when(vehicleMockRepository.filterByCustomer(customer.getUserId())).thenReturn(vehicles);

        when(mockRepository.searchByVehicleId(vehicle.getVehicleId())).thenReturn(visits);

        List<FrontEndVisitModel> services = new ArrayList<>(Collections.emptyList());

        visits.forEach(visit -> visit.getAutoServices().forEach(it -> services.add(new FrontEndVisitModel(
                visit.getStartDate().toString(),
                visit.getEndDate().toString(),
                it.getName(),
                visit.getVehicle().getModel().getModelName(),
                visit.getVehicle().getModel().getManufacturer().getManufactureName(),
                it.getPrice(),
                "BGN"
        ))));

        Assertions.assertEquals(services, service.searchByCustomerID(customer.getUserId(), "BGN"));
    }

    @Test
    public void filter_Should_Return_Filtered_List() {
        var visit = Helpers.createMockVisit();

        service.filter(employeeUser, Optional.of(visit.getStartDate().toString()), Optional.of(visit.getEndDate().toString()), Optional.of(visit.getVehicle().getLicensePlate()));

        verify(mockRepository, times(1)).filter(Optional.of(visit.getStartDate().toString()), Optional.of(visit.getEndDate().toString()), Optional.of(visit.getVehicle().getLicensePlate()));
    }

    @Test
    public void GetCount_should_Call_Repository() {
        service.getCount();

        verify(mockRepository, times(1)).getCount();
    }
}
