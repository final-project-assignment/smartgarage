package com.smartGarage.helperClasses;

public class QueryHelpers {

    /**
    Creates a query like: "% parameter %"
    */
    public static String like(String parameter) {
        return '%' + parameter + '%';
    }
}