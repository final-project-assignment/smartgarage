package com.smartGarage.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * Used to encapsulate search method parameters.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AutoServiceFilterParams {

    String name;

    Double minPrice;

    Double maxPrice;

}
