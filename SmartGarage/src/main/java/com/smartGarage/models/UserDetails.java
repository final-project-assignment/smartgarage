package com.smartGarage.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "user_details")
@AllArgsConstructor
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class UserDetails {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "user_detail_id", updatable = true, nullable = false)
    @EqualsAndHashCode.Include
    private String userDetailId;

    @Column (name = "first_name")
    @EqualsAndHashCode.Include
    private String firstName;

    @Column (name = "last_name")
    @EqualsAndHashCode.Include
    private String lastName;

    @Column (name = "phone")
    @EqualsAndHashCode.Include
    private String phone;

    @OneToOne(cascade = CascadeType.ALL , orphanRemoval = true)
    @JoinColumn(name = "user_id")
    private User user;

}
