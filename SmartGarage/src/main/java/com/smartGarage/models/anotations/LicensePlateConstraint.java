package com.smartGarage.models.anotations;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = LicensePlateValidator.class)
@Target({FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
public @interface LicensePlateConstraint {

    String message() default "Invalid license plate";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
