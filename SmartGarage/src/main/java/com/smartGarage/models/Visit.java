package com.smartGarage.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "visits")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Visit {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "visit_id", updatable = false, nullable = false)
    private String visitId;

    @JsonFormat(pattern = "yyyy-MM-dd", locale = "bg")
    @Column(name = "start_date", nullable = false)
    private LocalDate startDate;

    @JsonFormat(pattern = "yyyy-MM-dd", locale = "bg")
    @Column(name = "end_date", nullable = false)
    private LocalDate endDate;

    @ManyToOne()
    @JoinColumn(name = "vehicle_id", updatable = false, nullable = false)
    private Vehicle vehicle;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "visits_services",
            joinColumns = @JoinColumn(name = "visit_id"),
            inverseJoinColumns = @JoinColumn(name = "service_id"))
    private List<AutoService> autoServices;
}
