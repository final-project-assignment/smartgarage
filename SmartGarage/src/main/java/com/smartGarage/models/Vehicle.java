package com.smartGarage.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "vehicles")
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Vehicle {

    @Id
    @EqualsAndHashCode.Include
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")

    @Column(name = "vehicle_id", updatable = false, nullable = false)
    private String vehicleId;

    @Column(name = "VIN", nullable = false)
    @EqualsAndHashCode.Include
    private String VIN;

    @Column(name = "license_plate", nullable = false)
    @EqualsAndHashCode.Include
    private String licensePlate;

    @Column(name = "year")
    @EqualsAndHashCode.Include
    private Integer year;

    @ManyToOne()
    @JoinColumn(name = "owner")
    @EqualsAndHashCode.Include
    private User owner;

    @ManyToOne()
    @JoinColumn(name = "model_id")
    @EqualsAndHashCode.Include
    private Model model;
}
