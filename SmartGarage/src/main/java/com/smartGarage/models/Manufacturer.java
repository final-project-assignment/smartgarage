package com.smartGarage.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "manufacturers")
@Data
@NoArgsConstructor
public class Manufacturer {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "manufacturer_id", updatable = false, nullable = false)
    private String manufacturerId;

    @Column(name = "manufacturer_name")
    private String manufactureName;

}
