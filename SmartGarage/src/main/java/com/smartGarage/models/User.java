package com.smartGarage.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class User {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator")
    @EqualsAndHashCode.Include
    @Column(name = "user_id", updatable = true, nullable = false)
    private String userId;

    @Column(name = "username", updatable = true, nullable = false)
    @EqualsAndHashCode.Include
    private String username;

    @Column(name = "email", updatable = true, nullable = false)
    @EqualsAndHashCode.Include
    private String email;

    @Column(name = "password")
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles = new HashSet<>();

    @Column(name = "reset_password_token")
    private String resetPasswordToken;



    /**
     * Checks if the given User is employee
     */
    public boolean isEmployee() {
        return getRoles().stream().anyMatch(r -> r.getType().equals("employee"));
    }

    /**
     * Checks if the given User is customer
     */
    public boolean isCustomer() {
        return getRoles().stream().anyMatch(r -> r.getType().equals("customer"));
    }
}
