package com.smartGarage.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FrontEndVisitModel {

    @NotNull
    private String startDate;

    @NotNull
    private String endDate;

    @NotNull
    private String serviceName;

    @NotNull
    private String model;

    @NotNull
    private String manufacturer;

    @Positive(message = "Price should be a positive number")
    @NotNull
    private Double price;

    @NotNull
    private String currency;

}
