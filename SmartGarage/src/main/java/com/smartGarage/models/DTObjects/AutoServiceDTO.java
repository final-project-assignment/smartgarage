package com.smartGarage.models.DTObjects;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@NoArgsConstructor
public class AutoServiceDTO {

    @NotNull
    private String name;

    @Positive(message = "Price should be a positive number")
    @NotNull
    private Double price;
}
