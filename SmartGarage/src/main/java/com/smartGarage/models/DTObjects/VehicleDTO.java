package com.smartGarage.models.DTObjects;

import com.smartGarage.models.anotations.LicensePlateConstraint;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class VehicleDTO {

    @Size(min = 17, max = 17,  message = "VIN must be 17 character-long")
    @NotNull
    private String vin;

    @LicensePlateConstraint
    @NotNull
    private String licensePlate;

    @NotNull
    private String ownerId;

    @NotNull
    private String modelId;

    @NotNull
    private Integer year;
}
