package com.smartGarage.models.DTObjects;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ManufacturerDTO {

    String manufacturerName;
}
