package com.smartGarage.models.DTObjects;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
public class LoginDTO {
    @NotEmpty
    private String email;

    @NotEmpty
    private String password;
}
