package com.smartGarage.models.DTObjects;

import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * Used to transport filter params
 * from a template to the application.
 */
@Data
@NoArgsConstructor
public class AutoServiceFilterDTO {

    String name;
    Double minPrice;
    Double maxPrice;

}
