package com.smartGarage.models.DTObjects.search;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Used to encapsulate search method parameters.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VehicleSearchParameters {

    /*
      Used to encapsulate search method parameters.
     */

    String customerId;

}
