package com.smartGarage.models.DTObjects;

import lombok.Data;

@Data
public class SelectedServiceDTO {

    private String selectedServiceId;

    private String selectedServiceName;

}