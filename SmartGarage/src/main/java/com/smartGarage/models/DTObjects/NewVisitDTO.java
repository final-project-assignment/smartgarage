package com.smartGarage.models.DTObjects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewVisitDTO {

    @NotNull
    private String startDate;

    @NotNull
    private String endDate;

    private String vehicleId;
}
