package com.smartGarage.models.DTObjects.search;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDetailsSearchDTO {

    /*
     * Used to transport search params
     * from a template to the application.
     */

    private String firstName;

    private String lastName;

}
