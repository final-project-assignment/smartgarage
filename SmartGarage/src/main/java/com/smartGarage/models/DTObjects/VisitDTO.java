package com.smartGarage.models.DTObjects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VisitDTO {

    @NotNull
    private String startDate;

    @NotNull
    private String endDate;

    @NotNull
    private String vehicleId;

    @NotNull
    private List<String> servicesId;
}