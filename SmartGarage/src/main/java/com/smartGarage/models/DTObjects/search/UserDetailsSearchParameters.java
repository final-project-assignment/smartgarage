package com.smartGarage.models.DTObjects.search;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDetailsSearchParameters {

    /*
      Used to encapsulate search method parameters.
     */

    private String firstName;

    private String lastName;
}
