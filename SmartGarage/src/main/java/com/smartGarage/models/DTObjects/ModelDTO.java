package com.smartGarage.models.DTObjects;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ModelDTO {

    @NotNull
    private String manufactureId;

    @NotNull
    private String modelName;
}
