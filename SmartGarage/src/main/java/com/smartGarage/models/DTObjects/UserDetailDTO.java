package com.smartGarage.models.DTObjects;

import com.smartGarage.models.anotations.ContactNumberConstraint;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

@Data
@NoArgsConstructor
public class UserDetailDTO {

    @Size(min = 1, max = 20, message = "First name is required")
    @NotNull
    private String firstName;

    @Size(min = 1, max = 20, message = "Last name is required")
    @NotNull
    private String lastName;

    @ContactNumberConstraint
    @Size(min = 10, max = 10, message = "Last name is required")
    @NotNull
    private String phone;

    @NotNull
    private String userId;
}
