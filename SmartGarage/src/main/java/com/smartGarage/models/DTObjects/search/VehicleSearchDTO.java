package com.smartGarage.models.DTObjects.search;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VehicleSearchDTO {

    /*
     * Used to transport search params
     * from a template to the application.
     */

    private String customerId;

}
