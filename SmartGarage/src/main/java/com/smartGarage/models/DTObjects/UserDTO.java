package com.smartGarage.models.DTObjects;

import com.smartGarage.models.anotations.ValidPassword;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@NoArgsConstructor
@Data
public class UserDTO {

    @Size(min = 1, message = "Username is required")
    private String username;

    @ValidPassword
    private String password;

    @Email
    private String email;

}
