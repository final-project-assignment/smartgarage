package com.smartGarage.models.DTObjects;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
public class CurrencyDTO {

    @NotEmpty
    private String userCurrency;
}