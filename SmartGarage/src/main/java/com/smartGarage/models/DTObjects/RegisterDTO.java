package com.smartGarage.models.DTObjects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegisterDTO {

    @NotEmpty(message = "First name can't be empty")
    private String username;

    @NotEmpty(message = "Email can't be empty")
    private String email;

}
