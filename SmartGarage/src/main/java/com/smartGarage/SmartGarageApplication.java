package com.smartGarage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.mail.MessagingException;
import java.io.IOException;
import java.security.GeneralSecurityException;

@EnableSwagger2
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class SmartGarageApplication {

    public static void main(String[] args)
            throws IOException, GeneralSecurityException, MessagingException {

        SpringApplication.run(SmartGarageApplication.class, args);

    }
}
