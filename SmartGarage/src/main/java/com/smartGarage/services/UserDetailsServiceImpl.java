package com.smartGarage.services;

import com.smartGarage.exceptions.DuplicateEntityException;
import com.smartGarage.exceptions.EntityNotFoundException;
import com.smartGarage.models.DTObjects.search.UserDetailsSearchParameters;
import com.smartGarage.models.User;
import com.smartGarage.models.UserDetails;
import com.smartGarage.repositories.contracts.UserDetailRepository;
import com.smartGarage.services.contracts.UserDetailsService;
import com.smartGarage.validations.ValidationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserDetailRepository userDetailRepository;

    @Autowired
    public UserDetailsServiceImpl(UserDetailRepository userDetailRepository) {
        this.userDetailRepository = userDetailRepository;
    }

    @Override
    public List<UserDetails> getAll() {
        return userDetailRepository.getAll();
    }

    @Override
    public UserDetails getByID(String id) {
        return userDetailRepository.getByID(id);
    }

    @Override
    public void create(UserDetails userDetails) {

        isDuplicateException(userDetails);

        userDetailRepository.create(userDetails);
    }

    @Override
    public void update(UserDetails userDetails) {
        userDetailRepository.update(userDetails);
    }

    @Override
    public void delete(String id) {

        userDetailRepository.delete(id);
    }

    private void isDuplicateException(UserDetails userDetails) {
        boolean duplicateExists = true;
        try {
            userDetailRepository.getByID(userDetails.getUserDetailId());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {

            throw new DuplicateEntityException("UserDetails", "id",
                    userDetails.getUserDetailId());
        }
    }

    @Override
    public List<UserDetails> search(User user,
                                    Optional<String> firstName,
                                    Optional<String> lastName,
                                    Optional<String> email,
                                    Optional<String> phone,
                                    Optional<String> vehicle) {

        ValidationHelper.validateEmployee(user);

        return userDetailRepository.search(firstName, lastName, email, phone, vehicle);
    }

    @Override
    public List<UserDetails> sort(User user) {

        ValidationHelper.validateEmployee(user);

        return userDetailRepository.sort();
    }

    @Override
    public Integer getCount() {

        return userDetailRepository.getCount();
    }

    @Override
    public List<UserDetails> filterByCustomer(String customerId) {

        return userDetailRepository.filterByCustomer(customerId);
    }

    @Override
    public List<UserDetails> filter(UserDetailsSearchParameters userDetailsSearchParameters) {

        return userDetailRepository.filter(userDetailsSearchParameters);
    }
}
