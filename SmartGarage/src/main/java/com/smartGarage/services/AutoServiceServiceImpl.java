package com.smartGarage.services;

import com.smartGarage.models.AutoService;
import com.smartGarage.models.AutoServiceFilterParams;
import com.smartGarage.models.User;
import com.smartGarage.repositories.contracts.AutoServiceRepository;
import com.smartGarage.services.contracts.AutoServiceService;
import com.smartGarage.validations.ValidationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AutoServiceServiceImpl implements AutoServiceService {

    private final AutoServiceRepository autoServiceRepository;

    @Autowired
    public AutoServiceServiceImpl(AutoServiceRepository autoServiceRepository) {
        this.autoServiceRepository = autoServiceRepository;
    }

    @Override
    public List<AutoService> getAll(User user) {

        return autoServiceRepository.getAll();
    }

    @Override
    public AutoService getByID(User user, String id) {

        return autoServiceRepository.getByID(id);
    }

    @Override
    public void create(User user, AutoService value) {

        ValidationHelper.validateEmployee(user);

        autoServiceRepository.create(value);
    }

    @Override
    public void update(User user, AutoService value) {

        ValidationHelper.validateEmployee(user);

        autoServiceRepository.update(value);
    }

    @Override
    public void delete(User user, String id) {

        ValidationHelper.validateEmployee(user);

        autoServiceRepository.delete(id);
    }

    @Override
    public List<AutoService> filter(User user,
                                    Optional<String> name,
                                    Optional<Double> minPrice,
                                    Optional<Double> maxPrice) {

        ValidationHelper.validateEmployee(user);

        return autoServiceRepository.filter(name, minPrice, maxPrice);
    }

    @Override
    public Integer getCount() {

        return autoServiceRepository.getCount();
    }

    @Override
    public List<AutoService> getAll() {

        return autoServiceRepository.getAll();
    }

    @Override
    public List<AutoService> filter(AutoServiceFilterParams filterParam) {

        return autoServiceRepository.filter(filterParam);
    }
}