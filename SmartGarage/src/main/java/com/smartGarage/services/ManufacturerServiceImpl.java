package com.smartGarage.services;

import com.smartGarage.models.Manufacturer;
import com.smartGarage.models.User;
import com.smartGarage.repositories.contracts.ManufacturerRepository;
import com.smartGarage.services.contracts.ManufacturerService;
import com.smartGarage.validations.ValidationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ManufacturerServiceImpl implements ManufacturerService {


    private final ManufacturerRepository manufacturerRepository;

    @Autowired
    public ManufacturerServiceImpl(ManufacturerRepository manufacturerRepository) {

        this.manufacturerRepository = manufacturerRepository;
    }

    @Override
    public List<Manufacturer> getAll(User user) {

        ValidationHelper.validateEmployee(user);

        return manufacturerRepository.getAll();
    }

    @Override
    public Manufacturer getByID(User user, String id) {

        ValidationHelper.validateEmployee(user);

        return manufacturerRepository.getByID(id);
    }

    @Override
    public void create(User user, Manufacturer value) {

        ValidationHelper.validateEmployee(user);

        manufacturerRepository.create(value);
    }

    @Override
    public void update(User user, Manufacturer value) {

        ValidationHelper.validateEmployee(user);

        manufacturerRepository.update(value);
    }

    @Override
    public void delete(User user, String id) {

        ValidationHelper.validateEmployee(user);

        manufacturerRepository.delete(id);
    }

    @Override
    public List<Manufacturer> getAll() {

        return manufacturerRepository.getAll();
    }
}
