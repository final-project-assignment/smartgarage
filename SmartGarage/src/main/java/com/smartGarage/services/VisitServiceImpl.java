package com.smartGarage.services;

import com.smartGarage.currency.CurrencyConverter;
import com.smartGarage.models.FrontEndVisitModel;
import com.smartGarage.models.User;
import com.smartGarage.models.Vehicle;
import com.smartGarage.models.Visit;
import com.smartGarage.repositories.contracts.VehicleRepository;
import com.smartGarage.repositories.contracts.VisitRepository;
import com.smartGarage.services.contracts.VisitService;
import com.smartGarage.validations.ValidationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class VisitServiceImpl implements VisitService {

    private final VisitRepository visitRepository;
    private final VehicleRepository vehicleRepository;

    @Autowired
    public VisitServiceImpl(VisitRepository visitRepository,
                            VehicleRepository vehicleRepository) {

        this.visitRepository = visitRepository;
        this.vehicleRepository = vehicleRepository;
    }

    @Override
    public List<Visit> getAll(User user) {

        return visitRepository.getAll();
    }

    @Override
    public Visit getByID(User user, String id) {

        return visitRepository.getByID(id);
    }

    @Override
    public void create(User user, Visit value) {
        visitRepository.create(value);
    }

    @Override
    public void update(User user, Visit value) {
        visitRepository.update(value);
    }

    @Override
    public void delete(User user, String id) {
        visitRepository.delete(id);
    }

    @Override
    public List<Visit> filter(User user,
                              Optional<String> startDate,
                              Optional<String> endDate,
                              Optional<String> licencePlate) {

        return visitRepository.filter(startDate, endDate, licencePlate);
    }

    @Override
    public List<Visit> filterByCustomer(User user,
                                        String customerId) {

        ValidationHelper.validateCustomer(user);
        return visitRepository.filterByCustomer(customerId);
    }

    @Override
    public List<Visit> getAllCustomer() {
        return visitRepository.getAll();
    }

    @Override
    public List<FrontEndVisitModel> searchByCustomerID(String customerId,
                                                       String currency) {

        List<Visit> ans = new ArrayList<>();
        List<Vehicle> vehicles = vehicleRepository.filterByCustomer(customerId);

        for (Vehicle v : vehicles) {
            List<Visit> vehicleVisits = visitRepository.searchByVehicleId(v.getVehicleId());
            ans.addAll(vehicleVisits);
        }

        List<FrontEndVisitModel> services = new ArrayList<>(Collections.emptyList());

        ans.forEach(visit -> visit.getAutoServices()
                .forEach(it -> services.add(new FrontEndVisitModel(
                        visit.getStartDate().toString(),
                        visit.getEndDate().toString(),
                        it.getName(),
                        visit.getVehicle().getModel().getModelName(),
                        visit.getVehicle().getModel().getManufacturer().getManufactureName(),
                        it.getPrice() * CurrencyConverter.exchangeCurrency("BGN", currency),
                        currency
                ))));

        return services;
    }

    @Override
    public Integer getCount() {
        return visitRepository.getCount();
    }
}
