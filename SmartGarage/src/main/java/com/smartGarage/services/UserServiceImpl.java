package com.smartGarage.services;

import com.smartGarage.exceptions.DuplicateEntityException;
import com.smartGarage.exceptions.EntityNotFoundException;
import com.smartGarage.models.User;
import com.smartGarage.repositories.contracts.UserRepository;
import com.smartGarage.services.contracts.UserService;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {

        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getByID(String id) {
        return userRepository.getByID(id);
    }

    @Override
    public void create(User user) {
        isDuplicateException(user);
        userRepository.create(user);
    }

    @Override
    public void update(User user) {
        userRepository.update(user);
    }

    @Override
    public void delete(String id) {
        userRepository.delete(id);
    }

    private void isDuplicateException(User user) {
        boolean duplicateExists = true;

        try {
            userRepository.getByEmail(user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email",
                    user.getEmail());
        }
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }

    /**
     * Sets a new reset token when called
     */
    @Override
    public void updateResetPasswordToken(String token, String email) {

        User user = userRepository.getByEmail(email);

        if (user != null) {
            user.setResetPasswordToken(token);
            userRepository.update(user);
        } else {

            throw new EntityNotFoundException("Could not find any customer with the email " + email);
        }
    }

    /**
     * Gets the reset token
     */
    @Override
    public User getByResetPasswordToken(String token) {

        return userRepository.findByResetPasswordToken(token);
    }


    /**
     * Gets the new password, encodes it and sets it
     * Once the reset token has been used it will go back to null
     */
    @Override
    public void updatePassword(User user, String newPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(newPassword);

        user.setPassword(encodedPassword);
        user.setResetPasswordToken(null);

        userRepository.update(user);
    }
}
