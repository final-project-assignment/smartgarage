package com.smartGarage.services;

import com.smartGarage.googleApi.GmailOperations;
import com.smartGarage.models.DTObjects.RegisterDTO;
import com.smartGarage.models.User;
import com.smartGarage.repositories.contracts.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.IOException;
import java.security.GeneralSecurityException;

import static com.smartGarage.models.RandomPasswordGenerator.generatePassayPassword;

@Service
public class RegisterServiceImpl implements com.smartGarage.services.contracts.RegisterService {

    public static final String CUSTOMER_ROLE_UUID = "85d322cb-7a0f-11eb-9eaf-05e779a6d53b"; //todo fix
    private final RoleRepository roleRepository;
    private final String generatedPassword = generatePassayPassword();

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    public RegisterServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }


    @Override
    public User registerCustomer(RegisterDTO registerDTO)
            throws GeneralSecurityException, IOException, MessagingException {

        User user = new User();

        GmailOperations.sendEmail(registerDTO.getEmail(),
                "Smart Garage Password",
                "Use this email and password to login into SmartGarage:"
                        + System.lineSeparator() + System.lineSeparator() +
                        "Email: " + registerDTO.getEmail() + System.lineSeparator() +
                        "Password: " + generatedPassword);

        user.setPassword(passwordEncoder.encode(generatedPassword));
        user.setUsername(registerDTO.getUsername());
        user.setEmail(registerDTO.getEmail());
        user.getRoles().add(roleRepository.getByID(CUSTOMER_ROLE_UUID));

        return user;
    }

}
