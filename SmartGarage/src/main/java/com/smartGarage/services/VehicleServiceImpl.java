package com.smartGarage.services;

import com.smartGarage.models.User;
import com.smartGarage.models.Vehicle;
import com.smartGarage.models.DTObjects.search.VehicleSearchParameters;
import com.smartGarage.repositories.contracts.VehicleRepository;
import com.smartGarage.services.contracts.VehicleService;
import com.smartGarage.validations.ValidationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository vehicleRepository;

    @Autowired
    public VehicleServiceImpl(VehicleRepository vehicleRepository) {

        this.vehicleRepository = vehicleRepository;
    }

    @Override
    public List<Vehicle> getAll(User user) {

        return vehicleRepository.getAll();
    }

    @Override
    public Vehicle getByID(User user, String id) {

        return vehicleRepository.getByID(id);
    }

    @Override
    public void create(User user, Vehicle vehicle) {

        vehicleRepository.create(vehicle);
    }

    @Override
    public void update(User user, Vehicle vehicle) {
        ValidationHelper.validateEmployee(user);

        vehicleRepository.update(vehicle);
    }

    @Override
    public void delete(User user, String id) {
        vehicleRepository.delete(id);
    }

    @Override
    public List<Vehicle> filterByOwner(User user, String ownerId) {

        return vehicleRepository.filterByOwner(ownerId);
    }

    @Override
    public List<Vehicle> filterByCustomer(User user, String customerId) {

        return vehicleRepository.filterByCustomer(customerId);
    }

    @Override
    public Integer getCount() {

        return vehicleRepository.getCount();
    }

    @Override
    public List<Vehicle> filter(VehicleSearchParameters vehicleSearchParameters) {

        return vehicleRepository.filter(vehicleSearchParameters);
    }
}