package com.smartGarage.services;

import com.smartGarage.models.Model;
import com.smartGarage.models.User;
import com.smartGarage.repositories.contracts.ModelRepository;
import com.smartGarage.services.contracts.ModelService;
import com.smartGarage.validations.ValidationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModelServiceImpl implements ModelService {

    private final ModelRepository modelRepository;

    @Autowired
    public ModelServiceImpl(ModelRepository modelRepository) {
        this.modelRepository = modelRepository;
    }

    @Override
    public List<Model> getAll(User user) {

        ValidationHelper.validateEmployee(user);

        return modelRepository.getAll();
    }

    @Override
    public Model getByID(User user, String id) {

        ValidationHelper.validateEmployee(user);

        return modelRepository.getByID(id);
    }

    @Override
    public void create(User user, Model model) {

        ValidationHelper.validateEmployee(user);

        modelRepository.create(model);
    }

    @Override
    public void update(User user, Model model) {

        ValidationHelper.validateEmployee(user);

        modelRepository.update(model);
    }

    @Override
    public void delete(User user, String id) {

        ValidationHelper.validateEmployee(user);

        modelRepository.delete(id);
    }

    @Override
    public List<Model> getAll() {
        return modelRepository.getAll();
    }
}