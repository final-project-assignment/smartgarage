package com.smartGarage.services.contracts;

import com.smartGarage.models.Manufacturer;

import java.util.List;

public interface ManufacturerService extends CRUDService<Manufacturer>{

    List<Manufacturer> getAll();

}
