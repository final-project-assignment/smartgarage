package com.smartGarage.services.contracts;

import com.smartGarage.models.DTObjects.search.UserDetailsSearchParameters;
import com.smartGarage.models.User;
import com.smartGarage.models.UserDetails;

import java.util.List;
import java.util.Optional;

public interface UserDetailsService {

    void create(UserDetails userDetails);

    List<UserDetails> getAll();

    UserDetails getByID(String id);

    void update(UserDetails value);

    void delete(String id);

    List<UserDetails> search(User user,
                             Optional<String> firstName,
                             Optional<String> lastName,
                             Optional<String> email,
                             Optional<String> phone,
                             Optional<String> vehicle);

    List<UserDetails> sort(User user);

    Integer getCount();

    List<UserDetails> filterByCustomer(String customerId);

    List<UserDetails> filter(UserDetailsSearchParameters userDetailsSearchParameters);
}
