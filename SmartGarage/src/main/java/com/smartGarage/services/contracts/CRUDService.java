package com.smartGarage.services.contracts;

import com.smartGarage.models.User;

import java.util.List;

public interface CRUDService<T> {
    List<T> getAll(User user);

    T getByID(User user, String id);

    void create(User user, T value);

    void update(User user, T value);

    void delete(User user, String id);
}