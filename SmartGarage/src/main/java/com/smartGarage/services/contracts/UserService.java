package com.smartGarage.services.contracts;

import com.smartGarage.models.User;

import java.util.List;

public interface UserService {
    List<User> getAll();

    User getByID(String id);

    void create(User user);

    void update(User user);

    void delete(String id);

    User getByEmail(String email);

    User getByResetPasswordToken(String token);

    void updateResetPasswordToken(String token, String email);

    void updatePassword(User user, String newPassword);

}
