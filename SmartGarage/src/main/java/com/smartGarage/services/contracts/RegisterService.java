package com.smartGarage.services.contracts;

import com.smartGarage.models.DTObjects.RegisterDTO;
import com.smartGarage.models.User;

import javax.mail.MessagingException;
import java.io.IOException;
import java.security.GeneralSecurityException;

public interface RegisterService {

    User registerCustomer(RegisterDTO userDTO)
            throws GeneralSecurityException, IOException, MessagingException;

}
