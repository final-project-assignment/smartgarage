package com.smartGarage.services.contracts;

import com.smartGarage.models.User;
import com.smartGarage.models.Vehicle;
import com.smartGarage.models.DTObjects.search.VehicleSearchParameters;

import java.util.List;

public interface VehicleService extends CRUDService<Vehicle> {

    List<Vehicle> filterByOwner(User user, String ownerId);

    List<Vehicle> filterByCustomer(User user, String customerId);

    Integer getCount();

    List<Vehicle> filter(VehicleSearchParameters vehicleSearchParameters);
}