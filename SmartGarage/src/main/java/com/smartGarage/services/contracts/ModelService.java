package com.smartGarage.services.contracts;

import com.smartGarage.models.Model;

import java.util.List;

public interface ModelService extends CRUDService<Model> {

    List<Model> getAll();

}
