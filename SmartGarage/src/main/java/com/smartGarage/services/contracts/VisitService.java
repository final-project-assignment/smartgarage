package com.smartGarage.services.contracts;

import com.smartGarage.models.FrontEndVisitModel;
import com.smartGarage.models.User;
import com.smartGarage.models.Visit;

import java.util.List;
import java.util.Optional;

public interface VisitService extends CRUDService<Visit> {
    List<Visit> filter(User user,
                       Optional<String> startDate,
                       Optional<String> endDate,
                       Optional<String> licencePlate);

    List<FrontEndVisitModel> searchByCustomerID(String customerId, String currency);

    Integer getCount();

    List<Visit> filterByCustomer(User user, String customerId);

    List<Visit>getAllCustomer();
}