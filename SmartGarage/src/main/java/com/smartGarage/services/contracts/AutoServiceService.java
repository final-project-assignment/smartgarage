package com.smartGarage.services.contracts;

import com.smartGarage.models.AutoService;
import com.smartGarage.models.AutoServiceFilterParams;
import com.smartGarage.models.User;

import java.util.List;
import java.util.Optional;

public interface AutoServiceService extends CRUDService<AutoService> {
    List<AutoService> filter(User user,
                             Optional<String> name,
                             Optional<Double> minPrice,
                             Optional<Double> maxPrice);
    Integer getCount();

    List<AutoService> getAll();

    List<AutoService> filter(AutoServiceFilterParams filterParam);
}
