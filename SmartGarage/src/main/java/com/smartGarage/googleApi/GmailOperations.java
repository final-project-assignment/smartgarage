package com.smartGarage.googleApi;

import com.google.api.services.gmail.Gmail;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.google.api.client.util.Base64;
import com.google.api.services.gmail.model.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GmailOperations {

    private static final Logger LOGGER = LoggerFactory.getLogger(GmailOperations.class);

    public static final String USER = "me";

    public static void sendMessage(Gmail service, String userId, MimeMessage email)
            throws MessagingException, IOException {

        Message message = createMessageWithEmail(email);
        message = service.users().messages().send(userId, message).execute();

        LOGGER.info("Message id: " + message.getId());
        LOGGER.info(message.toPrettyString());
    }

    private static Message createMessageWithEmail(MimeMessage email)
            throws MessagingException, IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        email.writeTo(baos);
        String encodedEmail = Base64.encodeBase64URLSafeString(baos.toByteArray());
        Message message = new Message();
        message.setRaw(encodedEmail);
        return message;
    }

    private static MimeMessage createEmail(String to, String from, String subject,
            String bodyText)
            throws MessagingException, IOException {

        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        MimeMessage email = new MimeMessage(session);

        email.setFrom(new InternetAddress(from)); //me - awesomesmartgarage@gmail.com
        email.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(to)); //
        email.setSubject(subject);

        email.setText(bodyText);

        return email;
    }

    /**
     * Method to send a plain text email using Gmail api
     *
     * @param receiver the email you want to send the attachment to
     * @param subject the subject of the email (title)
     * @param bodyText the text that will be shown in the body
     */
    public static void sendEmail(String receiver,
                                 String subject,
                                 String bodyText) throws IOException,
                                    GeneralSecurityException, MessagingException {

        Gmail service = GmailUtils.getGmailService();
        MimeMessage mimeMessage =
                createEmail(receiver,
                        "me",
                        subject,
                        bodyText);

        Message message = createMessageWithEmail(mimeMessage);

        message = service.users().messages().send("me", message).execute();

        LOGGER.info("Message id: " + message.getId());
        LOGGER.info("Receiver: " + receiver);
        LOGGER.info(message.toPrettyString());
    }

    /**
     * Method to create the email structure of email with body attachment
     *
     * @param to             email you want to send the Email to
     * @param subject        email subject (title)
     * @param html           type of attachment to send
     * @param htmlReportPath the full path to the attachment
     */

    private static MimeMessage createHTMLEmailBodyWithAttachment(
            String to, String subject, String html, String htmlReportPath)
            throws AddressException, MessagingException {

        Properties props = new Properties();
        Session session = Session.getDefaultInstance(props, null);

        MimeMessage email = new MimeMessage(session);

        email.setFrom(new InternetAddress("me"));

        //For Multiple Email with comma separated ...

        String[] split = to.split(",");
        for (int i = 0; i < split.length; i++) {

            email.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(split[i]));
        }

        email.setSubject(subject);

        Multipart multiPart = new MimeMultipart("mixed");

        MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(html, "text/html; charset=utf-8");
        multiPart.addBodyPart(htmlPart, 0);

        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        DataSource source = new FileDataSource(new File(htmlReportPath));

        mimeBodyPart.setDataHandler(new DataHandler(source));
        mimeBodyPart.setFileName("autoServices.html");
        multiPart.addBodyPart(mimeBodyPart, 1);

        email.setContent(multiPart);

        return email;
    }

    /**
     * Method to send email with attachment using Gmail api
     *
     * @param receiver the email you want to send the attachment to
     */

    public static void sendEmailWithHTMLBodyAndAttachment(String receiver)
            throws IOException, AddressException,
            MessagingException,
            GeneralSecurityException {

        //HTML parse
        Document doc = Jsoup.parse(new File(System.getProperty("user.dir") +
                        "/SmartGarage/src/main/java/com/smartGarage/reports/autoServices.html"),
                "utf-8");

        Elements tags = doc.getElementsByTag("html"); //todo fix

        String body = tags.first().html();

        String htmlText = "<html>" + body + "</html>";


        Gmail service = GmailUtils.getGmailService(); //todo fix mimeMessage
        MimeMessage mimeMessage = createHTMLEmailBodyWithAttachment(receiver+"," +
                        "awesomesmartgarage@gmail.com",
                "This is a subject test",
                htmlText, System.getProperty("user.dir")
                        + "/SmartGarage/src/main/java/com/smartGarage/reports/autoServices.html");

        Message message = createMessageWithEmail(mimeMessage);

        message = service.users().messages().send("me", message).execute();

        LOGGER.info("Message id: " + message.getId()); //todo fix
        LOGGER.info(message.toPrettyString()); //todo fix

    }

}
