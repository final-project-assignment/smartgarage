package com.smartGarage.googleApi;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.util.Base64;
import com.google.api.client.util.StringUtils;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import org.json.JSONObject;

import java.io.*;

import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
@PropertySource("classpath:application.properties")
public class GmailUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(GmailUtils.class);

    private static String refreshToken, clientId, clientSecret, tokenUrl;

    private static final String APPLICATION_NAME = "Smart Garage";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final File FILE_PATH =
            new File(System.getProperty("user.dir") +
                    "/src/main/java/com/smartGarage/googleApi/credentials.json");
    public static final String USER = "me";
    public static Gmail service = null;

    @Autowired
    public GmailUtils(Environment env) {
        clientSecret = env.getProperty("google.client.client-secret");
        clientId = env.getProperty("google.client.client-id");
        refreshToken = env.getProperty("google.client.refresh-token");
        tokenUrl = env.getProperty("google.client.token.url");
    }

    /**
     * Method to read the email body
     */
    public static void getMailBody(String searchString) throws IOException {

        Gmail.Users.Messages.List request = service.users()
                .messages()
                .list(USER)
                .setQ(searchString);

        ListMessagesResponse messagesResponse = request.execute();
        request.setPageToken(messagesResponse.getNextPageToken());

        String messageId = messagesResponse.getMessages().get(0).getId();

        Message message = service.users().messages().get(USER, messageId).execute();


        String emailBody = StringUtils
                .newStringUtf8(Base64.decodeBase64(
                        message.getPayload().getParts().get(0).getBody().getData()));

        LOGGER.info("Email body : " + emailBody);
    }

    /**
     * Sets Google Client Secret, Access Token and Refresh Token
     */
    public static Gmail getGmailService() throws IOException, GeneralSecurityException {

        InputStream in = new FileInputStream(FILE_PATH); // Read credentials.json
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Credential builder

        Credential authorize = new GoogleCredential
                .Builder()
                .setTransport(GoogleNetHttpTransport.newTrustedTransport())
                .setJsonFactory(JSON_FACTORY)
                .setClientSecrets(clientSecrets.getDetails().getClientId().toString(),
                        clientSecrets.getDetails().getClientSecret().toString())
                .build()
                .setAccessToken(getAccessToken())
                .setRefreshToken(refreshToken);

        // Create Gmail service
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        service = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, authorize)
                .setApplicationName(GmailUtils.APPLICATION_NAME).build();

        return service;
    }

    /**
     * Access tokens expire every hour, the method below will generate
     * a new access token every time it is called using the refresh token,
     * client secret and clientId
     */
    private static String getAccessToken() {
        try {
            Map<String, Object> params = new LinkedHashMap<>();

            params.put("grant_type", "refresh_token");
            params.put("client_id", clientId);
            params.put("client_secret", clientSecret);
            params.put("refresh_token", refreshToken);

            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String, Object> param : params.entrySet()) {
                if (postData.length() != 0) {
                    postData.append('&');
                }
                postData.append(URLEncoder.encode(param.getKey(), StandardCharsets.UTF_8));
                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), StandardCharsets.UTF_8));
            }
            byte[] postDataBytes = postData.toString().getBytes(StandardCharsets.UTF_8);

            URL url = new URL(tokenUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setUseCaches(false);
            con.setRequestMethod("POST");
            con.getOutputStream().write(postDataBytes);

            BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuffer buffer = new StringBuffer();
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                buffer.append(line);
            }

            JSONObject json = new JSONObject(buffer.toString());
            String accessToken = json.getString("access_token");

            return accessToken;
        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return null;
    }

}
