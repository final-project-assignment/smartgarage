package com.smartGarage.modelMappers;

import com.smartGarage.models.DTObjects.ManufacturerDTO;
import com.smartGarage.models.DTObjects.ModelDTO;
import com.smartGarage.models.Manufacturer;
import com.smartGarage.models.Model;
import com.smartGarage.repositories.contracts.ManufacturerRepository;
import com.smartGarage.repositories.contracts.ModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CarModelModelMapper {

    private final ManufacturerRepository manufacturerRepository;

    @Autowired
    public CarModelModelMapper(ManufacturerRepository manufacturerRepository) {

        this.manufacturerRepository = manufacturerRepository;
    }

    /**
     * Creates a service without having to assign an id
     *
     * @param modelDTO data transfer object
     */
    public Model fromDto(ModelDTO modelDTO) {
        Model model = new Model();
        dtoToObject(modelDTO, model);

        return model;
    }

    /**
     * Creates an object from the dto
     *
     * @param modelDTO data transfer object
     * @param model    the new object
     */
    private void dtoToObject(ModelDTO modelDTO, Model model) {
        Manufacturer manufacturer = manufacturerRepository.getByID(modelDTO.getManufactureId());

        model.setModelName(modelDTO.getModelName());

        model.setManufacturer(manufacturer);
    }

    /**
     * Creates a service without having to assign an id
     *
     * @param manufacturerDTO data transfer object
     */
    public Manufacturer fromDto(ManufacturerDTO manufacturerDTO) {
        Manufacturer manufacturer = new Manufacturer();
        dtoToObject(manufacturerDTO, manufacturer);

        return manufacturer;
    }

    /**
     * Creates an object from the dto
     *
     * @param manufacturerDTO data transfer object
     * @param manufacturer    the new object
     */
    private void dtoToObject(ManufacturerDTO manufacturerDTO, Manufacturer manufacturer) {

        manufacturer.setManufactureName(manufacturerDTO.getManufacturerName());
    }
}
