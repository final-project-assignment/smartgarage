package com.smartGarage.modelMappers;

import com.smartGarage.models.DTObjects.UserDTO;
import com.smartGarage.models.RandomPasswordGenerator;
import com.smartGarage.models.User;
import com.smartGarage.repositories.contracts.RoleRepository;
import com.smartGarage.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserModelMapper {

    private final UserRepository userRepository;
    public static final String CUSTOMER_ROLE_UUID = "85d322cb-7a0f-11eb-9eaf-05e779a6d53b"; //todo fix
    public final RoleRepository roleRepository; //todo fix

    @Autowired
    public UserModelMapper(UserRepository userRepository,
                           RoleRepository roleRepository) {

        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    /**
     * Creates an user without having to assign an id
     *
     * @param userDTO data transfer object
     */
    public User fromDto(UserDTO userDTO) {
        User user = new User();
        dtoToObject(userDTO, user);
        return user;
    }

    /**
     * Gets an user with the given id
     *
     * @param userDTO data transfer object
     * @param id      owner of the vehicle
     */
    public User fromDto(UserDTO userDTO, String id) {
        User user = userRepository.getByID(id);
        dtoToObject(userDTO, user);
        return user;
    }

    /**
     * Creates a DTO from an Object
     *
     * @param user the new object
     */
    public UserDTO toDto(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername(user.getUsername());
        userDTO.setEmail(user.getEmail());
        userDTO.setPassword(user.getPassword());

        return userDTO;
    }

    /**
     * Creates an object from the dto
     *
     * @param userDTO data transfer object
     * @param user    the new object
     */
    private void dtoToObject(UserDTO userDTO, User user) {

        user.setUsername(userDTO.getUsername());
        user.setPassword(RandomPasswordGenerator.generatePassayPassword());
        user.setEmail(userDTO.getEmail());
        user.getRoles().add(roleRepository.getByID(CUSTOMER_ROLE_UUID));

    }
}
