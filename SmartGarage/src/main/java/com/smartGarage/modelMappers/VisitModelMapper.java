package com.smartGarage.modelMappers;

import com.smartGarage.models.AutoService;
import com.smartGarage.models.DTObjects.AutoServiceDTO;
import com.smartGarage.models.DTObjects.VisitDTO;
import com.smartGarage.models.Vehicle;
import com.smartGarage.models.Visit;
import com.smartGarage.repositories.contracts.AutoServiceRepository;
import com.smartGarage.repositories.contracts.VehicleRepository;
import com.smartGarage.repositories.contracts.VisitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class VisitModelMapper {

    private final VisitRepository visitRepository;
    private final AutoServiceRepository serviceRepository;
    private final VehicleRepository vehicleRepository;

    @Autowired
    public VisitModelMapper(VisitRepository visitRepository,
                            AutoServiceRepository serviceRepository,
                            VehicleRepository vehicleRepository) {

        this.visitRepository = visitRepository;
        this.serviceRepository = serviceRepository;
        this.vehicleRepository = vehicleRepository;
    }

    /**
     * Creates a visit without having to assign an id
     *
     * @param visitDTO data transfer object
     */
    public Visit fromDto(VisitDTO visitDTO) {
        Visit visit = new Visit();
        dtoToObject(visitDTO, visit);
        return visit;
    }

    /**
     * Creates a visit with having to assign an
     * id
     *
     * @param visitDTO data transfer object
     * @param id       the id which we want to assign to the new object
     */
    public Visit fromDto(VisitDTO visitDTO, String id) {
        Visit visit = visitRepository.getByID(id);
        dtoToObject(visitDTO, visit);
        return visit;
    }

    /**
     * Creates an object from the dto
     *
     * @param visitDTO data transfer object
     * @param visit    the new object
     */
    private void dtoToObject(VisitDTO visitDTO, Visit visit) {
        Vehicle vehicle = vehicleRepository.getByID(visitDTO.getVehicleId());
        List<AutoService> services = new ArrayList<>();
        for (String s : visitDTO.getServicesId()) {
            AutoService autoService = serviceRepository.getByID(s);
            services.add(autoService);
        }
        visit.setAutoServices(services);
        visit.setStartDate(LocalDate.parse(visitDTO.getStartDate()));
        visit.setEndDate(LocalDate.parse(visitDTO.getEndDate()));
        visit.setVehicle(vehicle);
    }

    public VisitDTO toDto(Visit visit) {
        VisitDTO visitDTO = new VisitDTO();
        visitDTO.setStartDate(visit.getStartDate().toString());
        visitDTO.setEndDate(visit.getEndDate().toString());
        visitDTO.setVehicleId(visit.getVehicle().getVehicleId());
        List<String> servicesId = new ArrayList<>();
        for (AutoService s: visit.getAutoServices()) {
            String id = s.getServiceId();
            servicesId.add(id);
        }
        visitDTO.setServicesId(servicesId);
        return visitDTO;
    }
}