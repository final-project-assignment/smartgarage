package com.smartGarage.modelMappers;

import com.smartGarage.models.AutoService;
import com.smartGarage.models.AutoServiceFilterParams;
import com.smartGarage.models.DTObjects.AutoServiceDTO;
import com.smartGarage.models.DTObjects.AutoServiceFilterDTO;
import com.smartGarage.repositories.contracts.AutoServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AutoServiceModelMapper {

    private final AutoServiceRepository autoServiceRepository;

    @Autowired
    public AutoServiceModelMapper(AutoServiceRepository autoServiceRepository) {
        this.autoServiceRepository = autoServiceRepository;
    }

    /**
     * Creates a service without having to assign an id
     *
     * @param serviceDto data transfer object
     */
    public AutoService fromDto(AutoServiceDTO serviceDto) {
        AutoService autoService = new AutoService();
        dtoToObject(serviceDto, autoService);

        return autoService;
    }

    /**
     * Creates a vehicle with having to assign an
     * id
     *
     * @param serviceDTO data transfer object
     * @param id         the id which we want to assign to the new object
     */
    public AutoService fromDto(AutoServiceDTO serviceDTO, String id) {
        AutoService autoService = autoServiceRepository.getByID(id);
        dtoToObject(serviceDTO, autoService);

        return autoService;
    }

    /**
     * Creates an object from the dto
     *
     * @param autoServiceDTO data transfer object
     * @param autoService    the new object
     */
    private void dtoToObject(AutoServiceDTO autoServiceDTO, AutoService autoService) {
        autoService.setName(autoServiceDTO.getName());
        autoService.setPrice(autoServiceDTO.getPrice());
    }

    /**
     * Creates an dto from object
     *
     * @param autoService    the object we want to make a dto
     */
    public AutoServiceDTO toDto(AutoService autoService) {
        AutoServiceDTO autoServiceDTO = new AutoServiceDTO();
        autoServiceDTO.setName(autoService.getName());
        autoServiceDTO.setPrice(autoService.getPrice());

        return autoServiceDTO;
    }

    public AutoServiceFilterParams fromDto(AutoServiceFilterDTO dto) {

        return new AutoServiceFilterParams(dto.getName(), dto.getMinPrice(), dto.getMaxPrice());
    }
}
