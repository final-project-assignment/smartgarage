package com.smartGarage.modelMappers;

import com.smartGarage.models.DTObjects.UserDetailDTO;
import com.smartGarage.models.DTObjects.search.UserDetailsSearchDTO;
import com.smartGarage.models.DTObjects.search.UserDetailsSearchParameters;
import com.smartGarage.models.DTObjects.search.VehicleSearchDTO;
import com.smartGarage.models.DTObjects.search.VehicleSearchParameters;
import com.smartGarage.models.Model;
import com.smartGarage.models.User;
import com.smartGarage.models.UserDetails;
import com.smartGarage.repositories.contracts.UserDetailRepository;
import com.smartGarage.repositories.contracts.UserRepository;
import org.hibernate.id.UUIDGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UserDetailModelMapper {

    private final UserDetailRepository detailRepository;
    private final UserRepository userRepository;

    @Autowired
    public UserDetailModelMapper(UserDetailRepository detailRepository,
                                 UserRepository userRepository) {

        this.detailRepository = detailRepository;
        this.userRepository = userRepository;
    }

    /**
     * Creates an user without having to assign an id
     *
     * @param userDetailDTO data transfer object
     */
    public UserDetails fromDto(UserDetailDTO userDetailDTO) {
        UserDetails userDetails = new UserDetails();
        UUID uuid = UUID. randomUUID();
        userDetails.setUserDetailId(uuid.toString());

        dtoToObject(userDetailDTO, userDetails);

        return userDetails;
    }

    public UserDetailsSearchParameters fromDto(UserDetailsSearchDTO dto){

        return new UserDetailsSearchParameters(dto.getFirstName(), dto.getLastName());
    }

    /**
     * Gets an user with the given id
     *
     * @param userDetailDTO data transfer object
     * @param id            owner of the vehicle
     */
    public UserDetails fromDto(UserDetailDTO userDetailDTO, String id) {
        UserDetails userDetails = detailRepository.getByID(id);
        dtoToObject(userDetailDTO, userDetails);

        return userDetails;
    }

    /**
     * Creates a DTO from an Object
     *
     * @param userDetails   the new object
     */
    public UserDetailDTO toDto(UserDetails userDetails) {
        UserDetailDTO userDetailDTO = new UserDetailDTO();

        userDetailDTO.setUserId(userDetails.getUser().getUserId());
        userDetailDTO.setFirstName(userDetails.getFirstName());
        userDetailDTO.setLastName(userDetails.getLastName());
        userDetailDTO.setPhone(userDetails.getPhone());

        return userDetailDTO;
    }

    /**
     * Creates an object from the dto
     *
     * @param userDetailDTO data transfer object
     * @param userDetails   the new object
     */
    private void dtoToObject(UserDetailDTO userDetailDTO, UserDetails userDetails) {

        User user = userRepository.getByID(userDetailDTO.getUserId());

        userDetails.setUser(user);
        userDetails.setPhone(userDetailDTO.getPhone());
        userDetails.setFirstName(userDetailDTO.getFirstName());
        userDetails.setLastName(userDetailDTO.getLastName());

    }

}
