package com.smartGarage.modelMappers;

import com.smartGarage.models.*;
import com.smartGarage.models.DTObjects.VehicleDTO;
import com.smartGarage.models.DTObjects.search.VehicleSearchDTO;
import com.smartGarage.models.DTObjects.search.VehicleSearchParameters;
import com.smartGarage.repositories.contracts.ModelRepository;
import com.smartGarage.repositories.contracts.UserRepository;
import com.smartGarage.repositories.contracts.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VehicleModelMapper {

    private final UserRepository userRepository;
    private final VehicleRepository vehicleRepository;
    private final ModelRepository modelRepository;

    @Autowired
    public VehicleModelMapper(UserRepository userRepository,
                              VehicleRepository vehicleRepository,
                              ModelRepository modelRepository) {

        this.userRepository = userRepository;
        this.vehicleRepository = vehicleRepository;
        this.modelRepository = modelRepository;
    }

    /**
     * Creates a vehicle without having to assign an id
     *
     * @param vehicleDTO data transfer object
     */
    public Vehicle fromDto(VehicleDTO vehicleDTO) {
        Vehicle vehicle = new Vehicle();
        dtoToObject(vehicleDTO, vehicle);

        return vehicle;
    }

    /**
     * Creates a vehicle with having to assign an
     * id
     *
     * @param vehicleDTO data transfer object
     * @param id         the id which we want to assign to the new object
     */
    public Vehicle fromDto(VehicleDTO vehicleDTO, String id) {
        Vehicle vehicle = vehicleRepository.getByID(id);
        dtoToObject(vehicleDTO, vehicle);

        return vehicle;
    }

    public VehicleSearchParameters fromDto(VehicleSearchDTO dto){

        return new VehicleSearchParameters(dto.getCustomerId());
    }

    /**
     * Creates a vehicle with having to assign an User
     *
     * @param vehicleDTO data transfer object
     * @param user       owner of the vehicle
     */
    public Vehicle fromDto(VehicleDTO vehicleDTO, User user) {
        Vehicle vehicle = new Vehicle();

        dtoToObject(vehicleDTO, vehicle);
        vehicle.setOwner(user);

        return vehicle;
    }
    /**
     * Creates an object from the dto
     *
     * @param vehicleDTO data transfer object
     * @param vehicle    the new object
     */
    private void dtoToObject(VehicleDTO vehicleDTO, Vehicle vehicle) {

        User user = userRepository.getByID(vehicleDTO.getOwnerId());
        Model model = modelRepository.getByID(vehicleDTO.getModelId());

        vehicle.setLicensePlate(vehicleDTO.getLicensePlate());
        vehicle.setModel(model);
        vehicle.setOwner(user);
        vehicle.setYear(vehicleDTO.getYear());
        vehicle.setVIN(vehicleDTO.getVin());
    }

    public VehicleDTO toDto(Vehicle vehicle) { //todo fix

        VehicleDTO vehicleDTO = new VehicleDTO();
        vehicleDTO.setVin(vehicle.getVIN());
        vehicleDTO.setLicensePlate(vehicle.getLicensePlate());
        vehicleDTO.setYear(vehicle.getYear());
        vehicleDTO.setOwnerId(vehicle.getOwner().getUserId());
        vehicleDTO.setModelId(vehicle.getModel().getModelId());

        return vehicleDTO;
    }

    public VehicleDTO toDto(Vehicle vehicle,Integer vehicleYear,String vehicleModelId) {
        VehicleDTO vehicleDTO = new VehicleDTO();
        vehicleDTO.setVin(vehicle.getVIN());
        vehicleDTO.setLicensePlate(vehicle.getLicensePlate());
        vehicleDTO.setYear(vehicleYear);
        vehicleDTO.setOwnerId(vehicle.getOwner().getUserId());
        vehicleDTO.setModelId(vehicleModelId);

        return vehicleDTO;
    }
}
