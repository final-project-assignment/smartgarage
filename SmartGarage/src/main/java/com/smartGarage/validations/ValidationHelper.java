package com.smartGarage.validations;

import com.smartGarage.exceptions.UnauthorizedOperationException;
import com.smartGarage.models.User;

public class ValidationHelper {


    /**
     * Checks if the given user has the Role of Employee
     */
    public static void validateEmployee(User user) {

        if (!user.isEmployee()) {
            throw new UnauthorizedOperationException("User is not an employee");
        }
    }

    /**
     * Checks if the given user has the Role of Customer
     */
    public static void validateCustomer(User user) {

        if (!user.isCustomer()) {
            throw new UnauthorizedOperationException("User is not an customer");
        }
    }
}
