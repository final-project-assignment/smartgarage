package com.smartGarage.repositories;

import com.smartGarage.exceptions.EntityNotFoundException;
import com.smartGarage.helperClasses.QueryHelpers;
import com.smartGarage.models.AutoService;
import com.smartGarage.models.AutoServiceFilterParams;
import com.smartGarage.models.Vehicle;
import com.smartGarage.repositories.contracts.AutoServiceRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.smartGarage.helperClasses.QueryHelpers.like;

@Repository
public class AutoServiceRepositoryImpl implements AutoServiceRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public AutoServiceRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AutoService> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<AutoService> query = session.createQuery
                    ("from AutoService", AutoService.class);

            return query.getResultList();
        }
    }

    @Override
    public AutoService getByID(String id) {
        try (Session session = sessionFactory.openSession()) {
            AutoService autoService = session.get(AutoService.class, id);

            if (autoService == null) {
                throw new EntityNotFoundException("Service", id);
            }

            return autoService;
        }
    }

    @Override
    public void create(AutoService value) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(value);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(AutoService value) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(value);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(String id) {
        AutoService autoService = getByID(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(autoService);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<AutoService> filter(Optional<String> name,
            Optional<Double> minPrice,
            Optional<Double> maxPrice) {

        try (Session session = sessionFactory.openSession()) {
            Query<AutoService> query = session.createQuery(
                    "from AutoService a where a.name = :name" +
                            " and a.price> :minPrice" +
                            " and a.price < :maxPrice",
                    AutoService.class);

            query.setParameter("name", name.orElse(""));
            query.setParameter("minPrice", minPrice.orElse(null));
            query.setParameter("maxPrice", maxPrice.orElse(null));
            List<AutoService> result = query.getResultList();
            if (result.size() == 0) {
                throw new EntityNotFoundException("AutoService", "criteria");
            }

            return query.getResultList();
        }
    }

    @Override
    public List<AutoService> filter(AutoServiceFilterParams filterParams) {

        try (Session session = sessionFactory.openSession()) {

            var baseQuery = " from AutoService a";
            var filters = new ArrayList<String>();

            if (!filterParams.getName().isBlank()) {
                filters.add(" a.name like lower(:name) ");
            }

            if (filterParams.getMinPrice() != null) {
                filters.add(" a.price > (:minPrice) ");
            }

            if (filterParams.getMaxPrice() != null) {
                filters.add(" a.price < (:maxPrice) ");
            }

            if (filters.size() > 0) {
                var finalQuery = String.join(" and ", filters);
                baseQuery = baseQuery + " where " + finalQuery;
            }

            Query<AutoService> query = session.createQuery(baseQuery, AutoService.class);

            if (!filterParams.getName().isBlank()) {
                query.setParameter("name", like(filterParams.getName()));
            }

            if (filterParams.getMinPrice() != null) {
                query.setParameter("minPrice", filterParams.getMinPrice());
            }

            if (filterParams.getMaxPrice() != null) {
                query.setParameter("maxPrice", filterParams.getMaxPrice());
            }

            return query.list();
        }
    }

    @Override
    public Integer getCount() {
        return getAll().size();
    }
}
