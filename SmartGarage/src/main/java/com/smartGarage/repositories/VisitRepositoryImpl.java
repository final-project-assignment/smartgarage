package com.smartGarage.repositories;

import com.smartGarage.exceptions.EntityNotFoundException;
import com.smartGarage.models.Visit;
import com.smartGarage.repositories.contracts.VisitRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public class VisitRepositoryImpl implements VisitRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public VisitRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Visit> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery("from Visit", Visit.class);

            return query.list();
        }
    }

    @Override
    public Visit getByID(String id) {
        try (Session session = sessionFactory.openSession()) {
            Visit visit = session.get(Visit.class, id);
            if (visit == null) {
                throw new EntityNotFoundException("Visit", id);
            }

            return visit;
        }
    }

    @Override
    public void create(Visit value) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(value);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Visit value) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(value);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(String id) {
        Visit visit = getByID(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(visit);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Visit> filter(
            Optional<String> licensePlate,
            Optional<String> startDate,
            Optional<String> endDate) {

        LocalDate startDateFormatted =
                LocalDate.parse(startDate.orElse(LocalDate.of(1970, 01, 01).toString()));

        LocalDate endDateFormatted =
                LocalDate.parse(endDate.orElse(LocalDate.now().toString()));

        try (Session session = sessionFactory.openSession()) {
            Query<Visit> query = session.createQuery(
                    "from Visit v where v.vehicle.licensePlate = :licensePlate " +
                            "and v.startDate >= :startDateFormatted " +
                            "and v.endDate <= :endDateFormatted ",
                    Visit.class);

            query.setParameter("licensePlate", licensePlate.orElse(null));
            query.setParameter("startDateFormatted", startDateFormatted);
            query.setParameter("endDateFormatted", endDateFormatted);

            List<Visit> result = query.getResultList();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Visit", "criteria");
            }

            return query.getResultList();
        }
    }

    @Override
    public List<Visit> searchByVehicleId(String vehicleId) {
        try (Session session = sessionFactory.openSession()) {

            Query<Visit> query = session.createQuery(
                    "from Visit v where v.vehicle.vehicleId = :vehicleId", Visit.class);
            query.setParameter("vehicleId", vehicleId);

            List<Visit> result = query.getResultList();

            if (result.size() == 0) {
                throw new EntityNotFoundException("Visit", "criteria");
            }

            return query.getResultList();
        }
    }

    @Override
    public Integer getCount() {
        return getAll().size();
    }

    @Override
    public List<Visit> filterByCustomer(String customerId) {
        try (Session session = sessionFactory.openSession()) {

            Query<Visit> query = session.createQuery(
                    "from Visit v where v.vehicle.owner.id = :customerId ",
                    Visit.class);
            query.setParameter("customerId", customerId);

            List<Visit> result = query.getResultList();

            if (result.size() == 0) {
                throw new EntityNotFoundException("Customer", "criteria");
            }

            return query.getResultList();
        }
    }

}
