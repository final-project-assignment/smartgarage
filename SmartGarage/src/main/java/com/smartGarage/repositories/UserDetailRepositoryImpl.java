package com.smartGarage.repositories;

import com.smartGarage.exceptions.EntityNotFoundException;
import com.smartGarage.models.DTObjects.search.UserDetailsSearchParameters;
import com.smartGarage.models.DTObjects.search.VehicleSearchParameters;
import com.smartGarage.models.UserDetails;
import com.smartGarage.models.Vehicle;
import com.smartGarage.repositories.contracts.UserDetailRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.smartGarage.helperClasses.QueryHelpers.like;

@Repository
public class UserDetailRepositoryImpl implements UserDetailRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserDetailRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<UserDetails> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery
                    ("from UserDetails", UserDetails.class);

            return query.list();
        }
    }

    @Override
    public UserDetails getByID(String id) {
        try (Session session = sessionFactory.openSession()) {
            UserDetails userDetails = session.get(UserDetails.class, id);
            if (userDetails == null) {
                throw new EntityNotFoundException("UserDetails", id);
            }

            return userDetails;
        }
    }

    @Override
    public void create(UserDetails userDetails) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(userDetails);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(UserDetails userDetails) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(userDetails);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(String id) {
        UserDetails userDetails = getByID(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(userDetails);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<UserDetails> search(Optional<String> firstName,
                                    Optional<String> lastName,
                                    Optional<String> email,
                                    Optional<String> phone,
                                    Optional<String> vehicle) {

        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery(
                    "from UserDetails u where u.firstName like concat('%', :firstName , '%') " +
                            "and u.lastName like concat('%', :lastName , '%') " +
                            "and u.user.email like concat('%', :email , '%') " +
                            "and u.phone like concat('%', :phone , '%') ", UserDetails.class);

            query.setParameter("firstName", firstName.orElse(""));
            query.setParameter("lastName", lastName.orElse(""));
            query.setParameter("email", email.orElse(""));
            query.setParameter("phone", phone.orElse(""));

            List<UserDetails> result = query.getResultList();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "search");
            }

            return query.getResultList();
        }
    }

    @Override
    public List<UserDetails> sort() {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery(
                    "from UserDetails order by firstName, lastName", UserDetails.class);


            List<UserDetails> result = query.getResultList();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "sort");
            }

            return query.list();
        }
    }

    @Override
    public Integer getCount() {
        return getAll().size();
    }

    @Override
    public List<UserDetails> filterByCustomer(String customerId) {

        try (Session session = sessionFactory.openSession()) {

            Query<UserDetails> query = session.createQuery(
                    "from UserDetails ud where ud.user.id = :customerId ",
                    UserDetails.class);
            query.setParameter("customerId", customerId);

            List<UserDetails> result = query.getResultList();

            if (result.size() == 0) {
                throw new EntityNotFoundException("Customer", "criteria");
            }

            return query.getResultList();
        }
    }


    @Override
    public List<UserDetails> filter(UserDetailsSearchParameters userDetailsSearchParameters) {
        try (Session session = sessionFactory.openSession()) {

            var baseQuery = "from UserDetails ";
            var filters = new ArrayList<String>();

            if (!userDetailsSearchParameters.getFirstName().isBlank()) {
                filters.add(" first_name like : firstName ");
            }

            if (!userDetailsSearchParameters.getLastName().isBlank()) {
                filters.add(" last_name like : lastName ");
            }

            if (filters.size() > 0) {
                var finalQuery = String.join("and", filters);
                baseQuery = baseQuery + " where " + finalQuery;
            }

            Query<UserDetails> query = session.createQuery(baseQuery, UserDetails.class);

            if (!userDetailsSearchParameters.getFirstName().isBlank()) {
                query.setParameter("firstName",
                        like(userDetailsSearchParameters.getFirstName()));
            }

            if (!userDetailsSearchParameters.getLastName().isBlank()) {
                query.setParameter("lastName",
                        like(userDetailsSearchParameters.getLastName()));
            }

            return query.list();
        }
    }


    @Override
    public List<UserDetails> filter(String searchParameter) {

        try (Session session = sessionFactory.openSession()) {

            return session.createQuery
                    (" from UserDetails where firstName like lower(:searchParameter) " +
                                    " or lastName like lower(:searchParameter) ",
                            UserDetails.class)
                    .setParameter("searchParameter", like(searchParameter))
                    .list();
        }
    }

}
