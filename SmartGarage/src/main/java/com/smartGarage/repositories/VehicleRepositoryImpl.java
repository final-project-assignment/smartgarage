package com.smartGarage.repositories;

import com.smartGarage.exceptions.EntityNotFoundException;
import com.smartGarage.models.Vehicle;
import com.smartGarage.models.DTObjects.search.VehicleSearchParameters;
import com.smartGarage.repositories.contracts.VehicleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import static com.smartGarage.helperClasses.QueryHelpers.like;

@Repository
public class VehicleRepositoryImpl implements VehicleRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public VehicleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Vehicle> getAll() {
        try (Session session = sessionFactory.openSession()) {

            Query<Vehicle> query =
                    session.createQuery("from Vehicle", Vehicle.class);

            return query.list();
        }
    }

    @Override
    public Vehicle getByID(String id) {
        try (Session session = sessionFactory.openSession()) {

            Vehicle vehicle = session.get(Vehicle.class, id);

            if (vehicle == null) {
                throw new EntityNotFoundException("Vehicle", id);
            }

            return vehicle;
        }
    }

    @Override
    public void create(Vehicle value) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(value);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Vehicle value) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(value);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(String id) {
        Vehicle vehicle = getByID(id);

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(vehicle);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Vehicle> filterByOwner(String ownerId) {
        try (Session session = sessionFactory.openSession()) {

            Query<Vehicle> query = session.createQuery(
                    "from Vehicle v where v.owner.id =:ownerId",
                    Vehicle.class);

            query.setParameter("ownerId", ownerId);

            List<Vehicle> result = query.getResultList();

            if (result.size() == 0) {
                throw new EntityNotFoundException("Vehicle", "criteria");
            }

            return query.getResultList();
        }
    }

    @Override
    public List<Vehicle> filterByCustomer(String customerId) {
        try (Session session = sessionFactory.openSession()) {

            Query<Vehicle> query = session.createQuery(
                    "from Vehicle v where v.owner.id = :customerId ",
                    Vehicle.class);

            query.setParameter("customerId", customerId);

            List<Vehicle> result = query.getResultList();

            if (result.size() == 0) {
                throw new EntityNotFoundException("Customer", "criteria");
            }

            return query.getResultList();
        }
    }

    @Override
    public Integer getCount() {
        return getAll().size();
    }

    @Override
    public List<Vehicle> filter(VehicleSearchParameters vehicleSearchParameters) {
        try (Session session = sessionFactory.openSession()) {

            var baseQuery = "from Vehicle v";
            var filters = new ArrayList<String>();

            if (!vehicleSearchParameters.getCustomerId().isBlank()) {
                filters.add(" v.owner.userId like : customerId ");
            }

            if (filters.size() > 0) {
                var finalQuery = String.join("and", filters);
                baseQuery = baseQuery + " where " + finalQuery;
            }

            Query<Vehicle> query = session.createQuery(baseQuery, Vehicle.class);

            if (!vehicleSearchParameters.getCustomerId().isBlank()) {
                query.setParameter("customerId", like(vehicleSearchParameters.getCustomerId()));
            }

            return query.list();
        }
    }

    @Override
    public List<Vehicle> filter(String searchParameter) {

        try (Session session = sessionFactory.openSession()) {

            return session.createQuery(
                    " from Vehicle where owner.id like lower(:searchParameter) ",
                    Vehicle.class)
                    .setParameter("searchParameter", like(searchParameter))
                    .list();
        }
    }

}