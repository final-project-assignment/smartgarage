package com.smartGarage.repositories;

import com.smartGarage.exceptions.EntityNotFoundException;
import com.smartGarage.models.Manufacturer;
import com.smartGarage.repositories.contracts.ManufacturerRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ManufacturerRepositoryImpl implements ManufacturerRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ManufacturerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Manufacturer> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Manufacturer> query = session.createQuery
                    ("from Manufacturer", Manufacturer.class);

            return query.list();
        }
    }

    @Override
    public Manufacturer getByID(String id) {
        try (Session session = sessionFactory.openSession()) {
            Manufacturer manufacturer = session.get(Manufacturer.class, id);
            if (manufacturer == null) {
                throw new EntityNotFoundException("Manufturer", id);
            }

            return manufacturer;
        }
    }

    @Override
    public void create(Manufacturer value) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(value);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Manufacturer value) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(value);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(String id) {
        Manufacturer manufacturer = getByID(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(manufacturer);
            session.getTransaction().commit();
        }
    }
}
