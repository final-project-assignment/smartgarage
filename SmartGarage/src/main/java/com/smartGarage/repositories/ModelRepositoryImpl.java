package com.smartGarage.repositories;

import com.smartGarage.exceptions.EntityNotFoundException;
import com.smartGarage.models.Model;
import com.smartGarage.repositories.contracts.ModelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class
ModelRepositoryImpl implements ModelRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ModelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Model> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Model> query = session.createQuery
                    ("from Model", Model.class);

            return query.list();
        }
    }

    @Override
    public Model getByID(String id) {
        try (Session session = sessionFactory.openSession()) {
            Model model = session.get(Model.class, id);
            if (model == null) {
                throw new EntityNotFoundException("Model", id);
            }

            return model;
        }
    }

    @Override
    public void create(Model value) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(value);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Model value) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(value);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(String id) {
        Model model = getByID(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(model);
            session.getTransaction().commit();
        }
    }
}
