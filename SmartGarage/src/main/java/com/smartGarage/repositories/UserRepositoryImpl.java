package com.smartGarage.repositories;

import com.smartGarage.exceptions.EntityNotFoundException;
import com.smartGarage.models.User;
import com.smartGarage.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {

            Query<User> query = session.createQuery("from User", User.class);

            return query.list();
        }
    }

    @Override
    public User getByID(String id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }

            return user;
        }
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(String id) {
        User user = getByID(id);

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public User getByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {

            Query<User> query = session.createQuery(
                    "from User where email = :email", User.class);
            query.setParameter("email", email);

            List<User> userList = query.list();

            if (userList.size() == 0) {
                throw new EntityNotFoundException("Wrong username or password");
            }

            return userList.get(0);
        }
    }

    @Override
    public User findByResetPasswordToken(String token) {
        try (Session session = sessionFactory.openSession()) {

            Query<User> query = session.createQuery(
                    "from User where resetPasswordToken = :token", User.class);
            query.setParameter("token", token);

            List<User> userList = query.list();

            if (userList.size() == 0) {
                throw new EntityNotFoundException("User", "token", token);
            }

            return userList.get(0);
        }
    }

}
