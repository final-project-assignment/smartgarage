package com.smartGarage.repositories.contracts;


import com.smartGarage.models.Model;

public interface ModelRepository extends CRUDRepository<Model>{
}
