package com.smartGarage.repositories.contracts;

import com.smartGarage.models.Manufacturer;

public interface ManufacturerRepository extends CRUDRepository<Manufacturer>{
}
