package com.smartGarage.repositories.contracts;

import com.smartGarage.models.Vehicle;
import com.smartGarage.models.Visit;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface VisitRepository extends CRUDRepository<Visit> {
    List<Visit> filter(Optional<String> startDate,
                       Optional<String> endDate,
                       Optional<String> licencePlate);

    List<Visit> searchByVehicleId(String vehicleId);

    Integer getCount();

    List<Visit> filterByCustomer(String customerId);
}
