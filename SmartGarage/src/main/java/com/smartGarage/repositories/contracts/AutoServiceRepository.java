package com.smartGarage.repositories.contracts;

import com.smartGarage.models.AutoService;
import com.smartGarage.models.AutoServiceFilterParams;

import java.util.List;
import java.util.Optional;

public interface AutoServiceRepository extends CRUDRepository<AutoService> {
    List<AutoService> filter(Optional<String> name,
                             Optional<Double> minPrice,
                             Optional<Double> maxPrice);

    List<AutoService> filter(AutoServiceFilterParams filterParam);

    Integer getCount();
}
