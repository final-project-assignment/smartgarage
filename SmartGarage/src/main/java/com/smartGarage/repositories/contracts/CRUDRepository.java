package com.smartGarage.repositories.contracts;

import java.util.List;

public interface CRUDRepository<T> {

    List<T> getAll();

    T getByID(String id);

    void create(T value);

    void update(T value);

    void delete(String id);
}
