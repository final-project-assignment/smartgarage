package com.smartGarage.repositories.contracts;

import com.smartGarage.models.DTObjects.search.UserDetailsSearchParameters;
import com.smartGarage.models.UserDetails;
import com.smartGarage.models.Visit;

import java.util.List;
import java.util.Optional;

public interface UserDetailRepository extends CRUDRepository<UserDetails> {

    List<UserDetails> search(Optional<String> firstName,
                             Optional<String> lastName,
                             Optional<String> email,
                             Optional<String> phone,
                             Optional<String> vehicle);

    List<UserDetails> sort();

    Integer getCount();

    List<UserDetails> filterByCustomer(String customerId);

    List<UserDetails> filter(UserDetailsSearchParameters userDetailsSearchParameters);

    List<UserDetails> filter(String searchParameter);
}
