package com.smartGarage.repositories.contracts;

import com.smartGarage.models.Vehicle;
import com.smartGarage.models.DTObjects.search.VehicleSearchParameters;

import java.util.List;

public interface VehicleRepository extends CRUDRepository<Vehicle> {

    List<Vehicle> filterByOwner(String ownerId);

    List<Vehicle> filterByCustomer(String customerId);

    Integer getCount();

    List<Vehicle> filter(VehicleSearchParameters vehicleSearchParameters);

    List<Vehicle> filter(String searchParameter);

}