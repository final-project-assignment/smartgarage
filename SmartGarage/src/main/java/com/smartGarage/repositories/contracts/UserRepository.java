package com.smartGarage.repositories.contracts;

import com.smartGarage.models.User;
import com.smartGarage.models.UserDetails;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CRUDRepository<User> {

    User getByEmail(String email);

    User findByResetPasswordToken(String token);

}
