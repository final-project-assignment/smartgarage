package com.smartGarage.repositories.contracts;

import com.smartGarage.models.Role;

import java.util.List;


public interface RoleRepository {
    List<Role> getAll();

    Role getByID(String id);
}
