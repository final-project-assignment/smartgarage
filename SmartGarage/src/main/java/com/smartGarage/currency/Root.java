package com.smartGarage.currency;

import java.util.Map;

public class Root {
    public String result;
    public String documentation;
    public String terms_of_use;
    public int time_last_update_unix;
    public String time_last_update_utc;
    public int time_next_update_unix;
    public String time_next_update_utc;
    public String base_code;
    public Map<String, String> conversion_rates;
}
