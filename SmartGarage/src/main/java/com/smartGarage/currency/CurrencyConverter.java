package com.smartGarage.currency;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class CurrencyConverter {

    public static Root getExchangeRate(String base) throws IOException {
        // Setting URL
        String url_str = "https://v6.exchangerate-api.com/v6/5531d0de7ac3a8cc47f58bcd/latest/" + base;

        // Making Request
        URL url = new URL(url_str);
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.connect();

        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
        JsonObject jsonObj = root.getAsJsonObject();

        ObjectMapper om = new ObjectMapper();

        return om.readValue(jsonObj.toString(), Root.class);
    }

    public static Double exchangeCurrency(String lhs, String rhs) {
        Root rate;
        try {
            rate = CurrencyConverter.getExchangeRate(lhs);
        } catch (IOException ignored) {
            return 1.0;
        }

        return Double.valueOf(rate.conversion_rates.get(rhs));
    }

    public static ArrayList<String> getAllCurrencies() {
        ArrayList<String> arr = new ArrayList<>();
        try {
            arr.addAll(getExchangeRate("BGN").conversion_rates.keySet());
        } catch (IOException e) {
            arr.add("BGN");
        }

        return arr;
    }
}
