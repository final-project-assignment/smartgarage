package com.smartGarage.exceptions;

public class EntityNotFoundException  extends RuntimeException {

    //todo fix

    /**
     * Unchecked exception that shows a message
     * "String  with id not found."
     *
     * @param type String, the tpe of entity;
     * @param id   int, the id of the entity
     */
    public EntityNotFoundException(String type, String id) {
        this(type, "type", String.valueOf(id));
    }

    /**
     * Unchecked exception that shows a message
     * "String  with id not found."
     *
     * @param type      String, the type of entity
     * @param attribute String, the missing attribute
     * @param value     String, the value of the entity
     */
    public EntityNotFoundException(String type, String attribute, String value) {
        super(String.format("%s with %s %s not found.", type, attribute, value));
    }

    /**
     * Unchecked exception that shows a message
     * "String  with id not found."
     *
     * @param name      Attribute that isn't fount
     */
    public EntityNotFoundException(String name) {
        super(name);

    }
}
