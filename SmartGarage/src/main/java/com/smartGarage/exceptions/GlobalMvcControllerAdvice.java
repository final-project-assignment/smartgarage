package com.smartGarage.exceptions;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice("com.smartGarage.controllers.mvc")
public class GlobalMvcControllerAdvice {

    /*
    Class to handle all the exceptions in MVC Controllers
*/

    // Unauthorised
    @ExceptionHandler(UnauthorizedOperationException.class)
    public String handleUnauthorized(UnauthorizedOperationException e, Model model) {
        model.addAttribute("message", e.getMessage());

        return "errorPages/error-401";
    }

    // Not Found
    @ExceptionHandler(EntityNotFoundException.class)
    public String handleNotFound(EntityNotFoundException e, Model model) {
        model.addAttribute("message", e.getMessage());

        return "errorPages/error-404";
    }

    // Duplicate
    @ExceptionHandler(DuplicateEntityException.class)
    public String handleDuplicate(DuplicateEntityException e, Model model) {
        model.addAttribute("message", e.getMessage());

        return "errorPages/error-409";
    }

    // Authentication
    @ExceptionHandler(AuthenticationFailureException.class)
    public String handleAuthenticationFailure(AuthenticationFailureException e, Model model) {
        model.addAttribute("message", e.getMessage());

        return "errorPages/error-401";
    }
}
