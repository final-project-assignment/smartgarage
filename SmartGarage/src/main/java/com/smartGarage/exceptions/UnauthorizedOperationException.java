package com.smartGarage.exceptions;

public class UnauthorizedOperationException extends RuntimeException {

    /**
     * Throws an error when User isn't authorized to do
     * a specific task
     */
    public UnauthorizedOperationException(String message) {
        super(message);
    }

}
