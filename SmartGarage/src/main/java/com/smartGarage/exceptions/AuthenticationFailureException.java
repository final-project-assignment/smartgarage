package com.smartGarage.exceptions;

public class AuthenticationFailureException extends RuntimeException {

    /**
     * Unchecked exception that shows a message
     * "Authentication Failed."
     *
     * @param message String, the message of entity
     */
    public AuthenticationFailureException(String message) {

        super(message);
    }
}