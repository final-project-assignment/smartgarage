package com.smartGarage.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice("com.smartGarage.controllers.rest")
public class GlobalRestExceptionHandler extends ResponseEntityExceptionHandler {

/*
    Class to handle all the exceptions in REST Controllers
*/

    @ExceptionHandler(value = EntityNotFoundException.class)
    protected ResponseEntity<Object> handleConflict(RuntimeException ex,
                                                    WebRequest request) {

        return handleExceptionInternal(ex, ex.getMessage(),
                new HttpHeaders(),
                HttpStatus.NOT_FOUND,
                request);
    }

    @ExceptionHandler(value = DuplicateEntityException.class)
    protected ResponseEntity<Object> handleConflict(DuplicateEntityException ex,
                                                    WebRequest request) {

        return handleExceptionInternal(ex, ex.getMessage(),
                new HttpHeaders(),
                HttpStatus.CONFLICT,
                request);
    }


    @ExceptionHandler(value = UnauthorizedOperationException.class)
    protected ResponseEntity<Object> handleConflict(UnauthorizedOperationException ex,
                                                    WebRequest request) {

        return handleExceptionInternal(ex, ex.getMessage(),
                new HttpHeaders(),
                HttpStatus.UNAUTHORIZED,
                request);
    }

}
