package com.smartGarage.controllers;

import com.smartGarage.exceptions.UnauthorizedOperationException;
import com.smartGarage.models.User;
import com.smartGarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpSession;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class AuthenticationHelper {

	public static final String AUTHORIZATION_HEADER_NAME = "Authorization"; // todo fix
	public static final String AUTHENTICATION_FAILURE_MESSAGE = "Wrong username or password."; // todo fix

	private final UserService userService;
	private final PasswordEncoder bCryptPasswordEncoder;

	@Autowired
	public AuthenticationHelper(UserService userService, PasswordEncoder bCryptPasswordEncoder) {

		this.userService = userService;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	public User tryGetUser(HttpHeaders headers) {

		if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
					"The requested resource requires authentication.");
		}

		String email = headers.getFirst(AUTHORIZATION_HEADER_NAME);

		return userService.getByEmail(email);
	}

	public User tryGetUser(HttpSession session) {

		String currentUser = (String) session.getAttribute("currentUser");

		if (currentUser == null) {
			throw new UnauthorizedOperationException("No user logged in.");
		}

		return userService.getByEmail(currentUser);
	}

	public User verifyAuthorization(HttpSession session, String role) {

		User user = tryGetUser(session);

		Set<String> userRoles =
				user.getRoles().stream().map(r -> r.getType().toLowerCase()).collect(Collectors.toSet());

		if (!userRoles.contains(role.toLowerCase())) {

			throw new UnauthorizedOperationException("Users does not have the required authorization.");
		}

		return user;
	}

	public User verifyAuthentication(String email, String password) {

		try {
			User user = userService.getByEmail(email);

			if (!bCryptPasswordEncoder.matches(password, user.getPassword())) {

				throw new UnauthorizedOperationException(AUTHENTICATION_FAILURE_MESSAGE);
			}
			return user;
		} catch (EntityNotFoundException e) {

			throw new UnauthorizedOperationException(AUTHENTICATION_FAILURE_MESSAGE);
		}
	}
}
