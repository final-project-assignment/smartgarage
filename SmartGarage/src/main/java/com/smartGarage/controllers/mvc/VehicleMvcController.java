package com.smartGarage.controllers.mvc;

import com.smartGarage.controllers.AuthenticationHelper;
import com.smartGarage.modelMappers.VehicleModelMapper;
import com.smartGarage.models.DTObjects.VehicleDTO;
import com.smartGarage.models.DTObjects.search.VehicleSearchDTO;
import com.smartGarage.models.User;
import com.smartGarage.models.Vehicle;
import com.smartGarage.services.contracts.ModelService;
import com.smartGarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/vehicles")
public class VehicleMvcController {

    public static final String ROLE_EMPLOYEE = "Employee"; //todo fix
    private final VehicleService vehicleService;
    public final AuthenticationHelper authenticationHelper;
    public final VehicleModelMapper modelMapper;
    private final ModelService modelService;
    private VehicleDTO vehicleDtoSaved;

    @Autowired
    public VehicleMvcController(VehicleService vehicleService,
                                AuthenticationHelper authenticationHelper,
                                VehicleModelMapper modelMapper,
                                ModelService modelService) {

        this.vehicleService = vehicleService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
        this.modelService = modelService;
    }

    @ModelAttribute("models")
    public List<com.smartGarage.models.Model> allModels() {
        return modelService.getAll();
    }

    @GetMapping
    public String showAllVehicles(Model model, HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);
        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

        model.addAttribute("vehicles", vehicleService.getAll(user));

        return "vehicle/vehicles";
    }

    @GetMapping("/new")
    public String showNewVehiclePage(Model model, HttpSession session) {
        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

        model.addAttribute("vehicle", new VehicleDTO());

        return "vehicle/vehicle-new";
    }

    @PostMapping("/new")
    public String createVehicle(Model model,
                                @Valid @ModelAttribute("vehicle") VehicleDTO vehicleDTO,
                                BindingResult errors,
                                HttpSession session) {

        User user = authenticationHelper.tryGetUser(session);
        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

        if (errors.hasErrors()) {

            return "vehicle/vehicle-new";
        }

        Vehicle vehicle = modelMapper.fromDto(vehicleDTO);
        vehicleService.create(user, vehicle);

        model.addAttribute("message",
                "Vehicle with id %s was created:  " + vehicle.getVehicleId());

        return "vehicle/vehicle-new";
    }

    @GetMapping("/update")
    public String showAllVehiclesForUpdate(Model model, HttpSession session) {

        User user = authenticationHelper.tryGetUser(session);
        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

        model.addAttribute("vehicles", vehicleService.getAll(user));

        return "vehicle/vehicles-to-update";
    }

    @GetMapping("/update/{id}")
    public String showEditVehiclePage(
            @PathVariable String id,
            Model model,
            HttpSession session) {

        User user = authenticationHelper.tryGetUser(session);
        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

        Vehicle vehicle = vehicleService.getByID(user, id);
        Integer vehicleYear = vehicle.getYear();
        String vehicleModelId = vehicle.getModel().getModelId();
        VehicleDTO vehicleDTO = modelMapper.toDto(vehicle, vehicleYear, vehicleModelId);

        vehicleDtoSaved = vehicleDTO;
        model.addAttribute("vehicle", vehicleDTO);

        return "vehicle/vehicle-update";
    }

    @PostMapping("/update/{id}")
    public String updateVehicle(@PathVariable String id,
            @Valid @ModelAttribute("vehicle") VehicleDTO vehicleDTO,
            BindingResult errors,
            HttpSession session) {

        User user = authenticationHelper.tryGetUser(session);
        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

        if (errors.hasErrors()) {

            return "errorPages/error-409";
        }

        vehicleDTO.setModelId(vehicleDtoSaved.getModelId());
        vehicleDTO.setYear(vehicleDtoSaved.getYear());
        Vehicle vehicle = modelMapper.fromDto(vehicleDTO, id);
        vehicleService.update(user, vehicle);

        return "redirect:/vehicles";
    }

    @GetMapping("/delete")
    public String deleteVehiclePage(Model model, HttpSession session) {

        User user = authenticationHelper.tryGetUser(session);
        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

        model.addAttribute("vehicles", vehicleService.getAll(user));

        return "vehicle/vehicle-delete";
    }

    @GetMapping("/delete/{id}")
    public String deleteVehicle(@PathVariable String id, HttpSession session) {

        User user = authenticationHelper.tryGetUser(session);
        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

        vehicleService.delete(user, id);

        return "redirect:/vehicles";
    }

    @GetMapping("/filter")
    public String handleGetVehicleSearch() {

        return "redirect:/vehicles";
    }

    @PostMapping("/filter")
    public String handleVehicleSearch(
            Model model,
            @ModelAttribute("vehicleSearchDTO") VehicleSearchDTO vehicleSearchDTO) {

        System.out.println(vehicleSearchDTO);

        vehicleService.filter(modelMapper.fromDto(vehicleSearchDTO));

        model.addAttribute("vehicles",
                vehicleService.filter(modelMapper.fromDto(vehicleSearchDTO)));

        return "vehicle/vehicles";
    }
}