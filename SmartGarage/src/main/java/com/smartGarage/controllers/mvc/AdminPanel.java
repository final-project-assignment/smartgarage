package com.smartGarage.controllers.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class AdminPanel {

	@GetMapping
	public String showAllWarehouses() {

		return "/admin-panel";
	}
}
