package com.smartGarage.controllers.mvc;

import com.smartGarage.exceptions.EntityNotFoundException;
import com.smartGarage.googleApi.GmailOperations;
import com.smartGarage.models.User;
import com.smartGarage.services.contracts.UserService;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

@Controller
public class ForgotPasswordController {

	@Autowired
	private UserService userService;

	@GetMapping("/forgot_password")
	public String showForgotPasswordForm() {

		return "forgot_password_form";
	}

	@PostMapping("/forgot_password")
	public String processForgotPassword(HttpServletRequest request, Model model)
			throws GeneralSecurityException, IOException, MessagingException {

		String token = RandomString.make(30);
		String email = request.getParameter("email");

		try {
			userService.updateResetPasswordToken(token, email);
			String resetPasswordLink = "http://localhost:8080" + "/reset_password?token=" + token;

			GmailOperations.sendEmail(email, "Reset Password - SmartGarage",
					"You can now reset your password on: " +
							System.lineSeparator() +
							System.lineSeparator() +
							resetPasswordLink);

			model.addAttribute("message",
					"We have sent a reset password link to your email. Please check.");

		} catch (EntityNotFoundException ex) {
			model.addAttribute("error", ex.getMessage());
		} catch (UnsupportedEncodingException | MessagingException e) {
			model.addAttribute("error", "Error while sending email");
		}

		return "forgot_password_form";
	}

	@GetMapping("/reset_password")
	public String showResetPasswordForm(@Param(value = "token") String token, Model model) {

		User user = userService.getByResetPasswordToken(token);
		model.addAttribute("token", token);

		if (user == null) {
			model.addAttribute("message", "Invalid Token");
			return "/index";
		}

		return "reset_password_form";
	}

	@PostMapping("/reset_password")
	public String processResetPassword(HttpServletRequest request, Model model) {

		String token = request.getParameter("token");
		String password = request.getParameter("password");

		User user = userService.getByResetPasswordToken(token);
		model.addAttribute("title", "Reset your password");

		if (user == null) {
			model.addAttribute("message", "Invalid Token");
			return "/index";
		} else {
			userService.updatePassword(user, password);

			model.addAttribute("message",
					"You have successfully changed your password.");
		}

		return "/index";
	}
}


