package com.smartGarage.controllers.mvc;

import com.smartGarage.controllers.AuthenticationHelper;
import com.smartGarage.modelMappers.AutoServiceModelMapper;
import com.smartGarage.models.AutoService;
import com.smartGarage.models.DTObjects.AutoServiceDTO;
import com.smartGarage.models.DTObjects.AutoServiceFilterDTO;
import com.smartGarage.models.User;
import com.smartGarage.services.contracts.AutoServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/autoServices")
public class AutoServiceMvcController {

	public static final String ROLE_EMPLOYEE = "Employee"; // todo move to authenticate class
	private final AutoServiceService autoServiceService;
	private final AuthenticationHelper authenticationHelper;
	private final AutoServiceModelMapper modelMapper;

	@Autowired
	public AutoServiceMvcController(
			AutoServiceService autoServiceService, AuthenticationHelper authenticationHelper,
			AutoServiceModelMapper modelMapper) {

		this.autoServiceService = autoServiceService;
		this.authenticationHelper = authenticationHelper;
		this.modelMapper = modelMapper;
	}

	@GetMapping
	public String showAllAutoServices(
			Model model, HttpSession session) {

		User user = authenticationHelper.tryGetUser(session);
		authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

		model.addAttribute("autoServiceFilterDTO", new AutoServiceFilterDTO());
		model.addAttribute("autoServices", autoServiceService.getAll(user));

		return "autoService/autoServices";
	}

	@GetMapping("/{id}")
	public String showSingleAutoService(
			@PathVariable String id, Model model, HttpSession session) {

		User user = authenticationHelper.tryGetUser(session);
		authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

		AutoService autoService = autoServiceService.getByID(user, id);
		model.addAttribute("autoService", autoService);

		return "autoService/autoService";
	}

	@GetMapping("/new")
	public String showNewAutoServicePage(
			Model model, HttpSession session) {

		authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

		model.addAttribute("autoService", new AutoServiceDTO());

		return "autoService/autoService-new";
	}

	@PostMapping("/new")
	public String createAutoService(
			@Valid @ModelAttribute("autoService") AutoServiceDTO autoServiceDTO, BindingResult errors,
			HttpSession session) {

		User user = authenticationHelper.tryGetUser(session);
		authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

		if (errors.hasErrors()) {
			return "autoService/autoService-new";
		}

		AutoService autoService = modelMapper.fromDto(autoServiceDTO);
		autoServiceService.create(user, autoService);

		return "redirect:/autoServices";
	}

	@GetMapping("/update/{id}")
	public String showEditAutoServicePage(
			@PathVariable String id, Model model, HttpSession session) {

		User user = authenticationHelper.tryGetUser(session);
		authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

		AutoService autoService = autoServiceService.getByID(user, id);
		AutoServiceDTO autoServiceDTO = modelMapper.toDto(autoService);

		model.addAttribute("autoServiceId", id);
		model.addAttribute("autoService", autoServiceDTO);

		return "autoService/autoService-update";
	}

	@PostMapping("/update/{id}")
	public String updateAutoService(@PathVariable String id, @Valid @ModelAttribute("autoService") AutoServiceDTO autoServiceDTO,
			BindingResult errors, HttpSession session) {

		User user = authenticationHelper.tryGetUser(session);
		authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

		if (errors.hasErrors()) {
			return "/autoService/autoServices";
		}

		AutoService autoService = modelMapper.fromDto(autoServiceDTO, id);
		autoServiceService.update(user, autoService);

		return "redirect:/autoServices";
	}

	@GetMapping("/update")
	public String showAllServicesForUpdate(
			Model model, HttpSession session) {

		User user = authenticationHelper.tryGetUser(session);
		authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

		model.addAttribute("autoServices", autoServiceService.getAll(user));

		return "autoService/autoServices-to-update";
	}

	@GetMapping("/delete")
	public String deleteAutoServicePage(
			Model model, HttpSession session) {

		User user = authenticationHelper.tryGetUser(session);
		authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

		model.addAttribute("autoServices", autoServiceService.getAll(user));

		return "autoService/autoService-delete";
	}

	@GetMapping("/delete/{id}")
	public String deleteAutoService(
			@PathVariable String id, HttpSession session) {

		User user = authenticationHelper.tryGetUser(session);
		authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

		autoServiceService.delete(user, id);

		return "redirect:/autoServices";
	}

	@GetMapping("/filter")
	public String handleGetAutoServiceFilter() {

		return "redirect:autoServices";
	}

	@PostMapping("/filter")
	public String handleAutoServiceFilter(
			Model model, @ModelAttribute("autoServiceFilterDTO") AutoServiceFilterDTO autoServiceFilterDTO) {

		List<AutoService> result;
		if (autoServiceFilterDTO != null) {
			result = autoServiceService.filter(modelMapper.fromDto(autoServiceFilterDTO));
		} else {
			result = autoServiceService.getAll();
		}

		model.addAttribute("autoServices", result);

		return "autoService/autoServices";
	}
}
