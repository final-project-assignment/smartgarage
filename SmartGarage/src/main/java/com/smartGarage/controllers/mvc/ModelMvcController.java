package com.smartGarage.controllers.mvc;

import com.smartGarage.controllers.AuthenticationHelper;
import com.smartGarage.modelMappers.CarModelModelMapper;
import com.smartGarage.models.DTObjects.ManufacturerDTO;
import com.smartGarage.models.DTObjects.ModelDTO;
import com.smartGarage.models.Manufacturer;
import com.smartGarage.models.User;
import com.smartGarage.services.contracts.ManufacturerService;
import com.smartGarage.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/models")
public class ModelMvcController {

	public static final String ROLE_EMPLOYEE = "Employee"; // todo fix

	private final ModelService modelService;
	private final AuthenticationHelper authenticationHelper;
	private final CarModelModelMapper modelMapper;
	private final ManufacturerService manufacturerService;
	private List<Manufacturer> manufactures;

	@Autowired
	public ModelMvcController(ModelService modelService,
			AuthenticationHelper authenticationHelper,
			CarModelModelMapper modelMapper,
			ManufacturerService manufacturerService) {

		this.modelService = modelService;
		this.authenticationHelper = authenticationHelper;
		this.modelMapper = modelMapper;
		this.manufacturerService = manufacturerService;
	}

	@ModelAttribute("manufacturers")
	public List<Manufacturer> allManufacturers() {

		return manufactures;
	}

	@ModelAttribute("manufacturer")
	public ManufacturerDTO manufacturer() {

		return new ManufacturerDTO();
	}

	@ModelAttribute("model")
	public ModelDTO model() {

		return new ModelDTO();
	}

	@PostMapping("/new/manufacturer")
	public String createManufacturer(@Valid @ModelAttribute("manufacturer") ManufacturerDTO manufacturerDTO,
			BindingResult errors,
			HttpSession session) {

		User user = authenticationHelper.tryGetUser(session);
		authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

		if (errors.hasErrors()) {

			return "model/model-new";
		}

		Manufacturer manufacturer = modelMapper.fromDto(manufacturerDTO);
		manufacturerService.create(user, manufacturer);
		manufactures = manufacturerService.getAll();

		return "redirect:./";
	}

	@GetMapping("/new")
	public String showNewModels(HttpSession session) {

		authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);
		manufactures = manufacturerService.getAll();

		return "model/model-new";
	}

	@PostMapping("/new")
	public String createModel(@Valid @ModelAttribute("model") ModelDTO modelDTO,
			BindingResult errors, HttpSession session) {

		User user = authenticationHelper.tryGetUser(session);
		authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

		if (errors.hasErrors()) {

			return "model/model-new";
		}

		com.smartGarage.models.Model model = modelMapper.fromDto(modelDTO);
		modelService.create(user, model);

		return "redirect:../vehicles/new";
	}
}