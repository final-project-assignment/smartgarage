package com.smartGarage.controllers.mvc;

import com.smartGarage.controllers.AuthenticationHelper;
import com.smartGarage.modelMappers.UserModelMapper;
import com.smartGarage.models.DTObjects.RegisterDTO;
import com.smartGarage.models.DTObjects.UserDTO;
import com.smartGarage.models.User;
import com.smartGarage.services.contracts.RegisterService;
import com.smartGarage.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

import static com.smartGarage.controllers.mvc.AutoServiceMvcController.ROLE_EMPLOYEE;

@Controller
@RequestMapping("/users")
public class UserMvcController {

	private final AuthenticationHelper authenticationHelper;
	private final UserService userService;
	private final RegisterService registerService;
	private final UserModelMapper userModelMapper;

	public UserMvcController(AuthenticationHelper authenticationHelper,
			UserService userService,
			RegisterService registerService,
			UserModelMapper userModelMapper) {

		this.authenticationHelper = authenticationHelper;
		this.userService = userService;
		this.registerService = registerService;
		this.userModelMapper = userModelMapper;
	}

	@ModelAttribute("users")
	public List<User> populateUsers() {

		return userService.getAll();
	}

	@GetMapping
	public String showAllUsers(Model model, HttpSession session) {

		User user;
		user = authenticationHelper.tryGetUser(session);

		if (user.isEmployee()) {
			model.addAttribute("users", userService.getAll());
		}
		return "users/users";
	}

	@GetMapping("/register")
	public String showRegisterPage(Model model, HttpSession session) {

		authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

		model.addAttribute("register", new RegisterDTO());

		return "users/users-new";
	}

	@PostMapping("/register")
	public String handleRegister(
			Model model, @Valid @ModelAttribute("register") RegisterDTO register,
			BindingResult bindingResult,
			HttpSession session)
			throws GeneralSecurityException, IOException, MessagingException {

		authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

		if (bindingResult.hasErrors()) {

			return "users/users-new";
		}

		if (!register.getEmail().contains("@") || !register.getEmail().contains(".")) {
			bindingResult.rejectValue("email", "email_error", "Invalid email.");

			return "users/users-new";
		}

		User user = registerService.registerCustomer(register);
		userService.create(user);

		model.addAttribute("message", "User with id was created:  " + user.getUserId());

		return "users/users-new";
	}

	@GetMapping("/update/{id}")
	public String showEditUserPage(@PathVariable String id, Model model) {

		User user = userService.getByID(id);
		UserDTO userDTO = userModelMapper.toDto(user);

		model.addAttribute("userId", id);
		model.addAttribute("user", userDTO);

		return "users/users-update";

	}

	@PostMapping("/update/{id}")
	public String updateUser(@PathVariable String id, @Valid @ModelAttribute("user") UserDTO userDTO,
			BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {

			return "users/users";
		}

		if (!userDTO.getEmail().contains("@") || !userDTO.getEmail().contains(".")) {
			bindingResult.rejectValue("email", "email_error", "Invalid email.");

			return "users/users";
		}

		User user = userModelMapper.fromDto(userDTO, id);
		userService.update(user);

		return "redirect:/users";

	}

	@GetMapping("/update")
	public String showAllAdmin(Model model, HttpSession session) {

		authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

		model.addAttribute("users", userService.getAll());

		return "users/user-update-list";
	}

	@GetMapping("/delete")
	public String deleteUSerPage(Model model, HttpSession session) {

		authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

		model.addAttribute("users", userService.getAll());

		return "users/users-delete";
	}

	@GetMapping("/delete/{id}")
	public String deleteUser(@PathVariable String id, HttpSession session) {

		authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);
		userService.delete(id);

		return "redirect:/users";
	}
}
