package com.smartGarage.controllers.mvc;

import com.smartGarage.controllers.AuthenticationHelper;
import com.smartGarage.currency.CurrencyConverter;
import com.smartGarage.modelMappers.VisitModelMapper;
import com.smartGarage.models.AutoService;
import com.smartGarage.models.DTObjects.*;
import com.smartGarage.models.FrontEndVisitModel;
import com.smartGarage.models.User;
import com.smartGarage.models.Visit;
import com.smartGarage.services.contracts.AutoServiceService;
import com.smartGarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/visits")
public class VisitMvcController {

    public static final String ROLE_EMPLOYEE = "Employee"; // todo fix

    private final VisitService visitService;
    private final AutoServiceService autoServiceService;
    private final AuthenticationHelper authenticationHelper;
    private final VisitModelMapper modelMapper;
    private final List<String> selectedServices = new ArrayList<>();
    private final List<String> selectedServicesNames = new ArrayList<>();
    private String vehicleID;
    private String visitID = "";

    @Autowired
    public VisitMvcController(VisitService visitService,
                              AutoServiceService autoServiceService,
                              AuthenticationHelper authenticationHelper,
                              VisitModelMapper modelMapper) {

        this.visitService = visitService;
        this.autoServiceService = autoServiceService;
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
    }

    @ModelAttribute("visitID")
    public String id() {

        return visitID;
    }

    @ModelAttribute("userCurrency")
    public CurrencyDTO currency() {

        return new CurrencyDTO();
    }

    @ModelAttribute("selectedService")
    public SelectedServiceDTO selectedService() {

        return new SelectedServiceDTO();
    }

    @ModelAttribute("visit")
    public NewVisitDTO visit() {

        return new NewVisitDTO();
    }

    @ModelAttribute("currencies")
    public List<String> populateCurrencies() {

        return CurrencyConverter.getAllCurrencies();
    }

    @ModelAttribute("services")
    public List<AutoService> listServices(HttpSession session) {
        User user = authenticationHelper.tryGetUser(session);

        return autoServiceService.getAll(user);
    }

    @ModelAttribute("selectedServices")
    public List<String> selectedServices() {

        return selectedServices;
    }

    @ModelAttribute("selectedServicesNames")
    public List<String> selectedServicesNames() {

        return selectedServicesNames;
    }

    @GetMapping
    public String showAllVisits(Model model,
                                HttpSession session) {

        User user = authenticationHelper.tryGetUser(session);

        model.addAttribute("visits", visitService.getAll(user));

        return "visit/visits";
    }

    @GetMapping("/{id}")
    public String showSingleVisit(@PathVariable String id,
                                  Model model,
                                  HttpSession session) {

        User user = authenticationHelper.tryGetUser(session);
        Visit visit = visitService.getByID(user, id);

        model.addAttribute("visit", visit);

        return "visit/visit";
    }

    @GetMapping("/new/delete")
    public String resetSelectedServices(HttpSession session) {

        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);
        selectedServices.clear();
        selectedServicesNames.clear();

        return "visit/visit-new";
    }

    @PostMapping("/new")
    public String addSelectedService(@ModelAttribute("selectedService") SelectedServiceDTO selectedService,
                                     HttpSession session) {

        User user = authenticationHelper.tryGetUser(session);

        selectedServices
                .add(selectedService
                        .getSelectedServiceId());

        selectedServicesNames
                .add(autoServiceService
                        .getByID(user, selectedService.getSelectedServiceId())
                        .getName());

        return "visit/visit-new";
    }

    @GetMapping("/new")
    public String showNewVisitPage() {

        return "visit/visit-new";
    }

    @PostMapping("/new-visit")
    public String createVisit(
            @Valid @ModelAttribute("visit") NewVisitDTO visitDTONew,
            BindingResult errors,
            HttpSession session) {

        User user = authenticationHelper.tryGetUser(session);

        if (errors.hasErrors()) {

            return "visit/visit-new";
        }

        VisitDTO visitDTO = new VisitDTO(visitDTONew.getStartDate(),
                visitDTONew.getEndDate(),
                visitDTONew.getVehicleId(),
                selectedServices);

        Visit visit = modelMapper.fromDto(visitDTO);
        visitService.create(user, visit);

        return "redirect:/visits";
    }

    @GetMapping("/update")
    public String showAllVisitsForUpdate(Model model, HttpSession session) {

        User user = authenticationHelper.tryGetUser(session);
        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

        model.addAttribute("visits", visitService.getAll(user));

        return "visit/visit-to-update";
    }

    @GetMapping("/update/{id}")
    public String showEditVisitPage(@PathVariable String id,
                                    Model model,
                                    HttpSession session) {

        User user = authenticationHelper.tryGetUser(session);
        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

        Visit visit = visitService.getByID(user, id);
        VisitDTO visitDTO = modelMapper.toDto(visit);

        visitID = id;
        vehicleID = visit.getVehicle().getVehicleId();

        model.addAttribute("visit", visitDTO);

        return "visit/visit-update";
    }

    @PostMapping("/update/{id}")
    public String updateVisit(@PathVariable String id,
                              @Valid @ModelAttribute("visit") NewVisitDTO visitDTO,
                              BindingResult errors,
                              HttpSession session) {

        User user = authenticationHelper.tryGetUser(session);
        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

        if (errors.hasErrors()) {

            return "/visit/visits";
        }

        VisitDTO visitObj =
                new VisitDTO(visitDTO.getStartDate(), visitDTO.getEndDate(),
                        vehicleID, selectedServices);

        Visit visit = modelMapper.fromDto(visitObj, id);
        visitService.update(user, visit);

        return "redirect:/visits";
    }

    @GetMapping("/update/delete")
    public String resetSelectedServicesForUpdate(HttpSession session) {

        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);
        selectedServices.clear();
        selectedServicesNames.clear();

        return "visit/visit-update";
    }

    @PostMapping("/update")
    public String addSelectedServiceForUpdate(@ModelAttribute("selectedService") SelectedServiceDTO selectedService,
                                              HttpSession session) {

        User user = authenticationHelper.tryGetUser(session);

        selectedServices.add(selectedService.getSelectedServiceId());
        selectedServicesNames.add(autoServiceService
                .getByID(user, selectedService.getSelectedServiceId()).getName());

        return "visit/visit-update";
    }

    @GetMapping("/delete")
    public String deleteVisitPage(Model model, HttpSession session) {

        User user = authenticationHelper.tryGetUser(session);
        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

        model.addAttribute("visits", visitService.getAll(user));

        return "visit/visit-delete";
    }

    @GetMapping("/delete/{id}")
    public String deleteVisit(@PathVariable String id,
                              HttpSession session) {

        User user = authenticationHelper.tryGetUser(session);

        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);
        visitService.delete(user, id);

        return "redirect:/visits";
    }

    @PostMapping("user/{id}")
    public String showVisitsByUserIdWithChangedCurrency(@PathVariable String id,
                                                        @ModelAttribute("userCurrency") CurrencyDTO currencyDTO,
                                                        Model model) {

        List<FrontEndVisitModel> visits =
                visitService.searchByCustomerID(id, currencyDTO.getUserCurrency());

        model.addAttribute("userVisits", visits);
        model.addAttribute("id", id);

        return "visit/visits-of-user";
    }

    @GetMapping("user/{id}")
    public String showVisitsByUserId(@PathVariable String id,
                                     Model model) {

        List<FrontEndVisitModel> visits = visitService.searchByCustomerID(id, "BGN");

        model.addAttribute("userVisits", visits);
        model.addAttribute("id", id);

        return "visit/visits-of-user";
    }
}