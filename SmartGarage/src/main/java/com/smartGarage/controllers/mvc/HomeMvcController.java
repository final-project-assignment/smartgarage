package com.smartGarage.controllers.mvc;

import com.smartGarage.controllers.AuthenticationHelper;
import com.smartGarage.exceptions.UnauthorizedOperationException;
import com.smartGarage.models.User;
import com.smartGarage.services.contracts.AutoServiceService;
import com.smartGarage.services.contracts.UserDetailsService;
import com.smartGarage.services.contracts.VehicleService;
import com.smartGarage.services.contracts.VisitService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeMvcController {

	private final VisitService visitService;
	private final VehicleService vehicleService;
	private final UserDetailsService userService;
	private final AutoServiceService autoServiceService;
	private final AuthenticationHelper authenticationHelper;

	public HomeMvcController(VisitService visitService,
			VehicleService vehicleService,
			UserDetailsService userService,
			AutoServiceService autoServiceService,
			AuthenticationHelper authenticationHelper) {

		this.visitService = visitService;
		this.vehicleService = vehicleService;
		this.userService = userService;
		this.autoServiceService = autoServiceService;
		this.authenticationHelper = authenticationHelper;
	}

	@ModelAttribute("isAuthenticated")
	public boolean populateIsAuthenticated(HttpSession session) {

		return session.getAttribute("currentUser") != null;
	}

	@GetMapping("/")
	public String showHomePage(Model model, HttpSession session) {

		String countVisits = visitService.getCount().toString();
		String countVehicles = vehicleService.getCount().toString();
		String countUsers = userService.getCount().toString();
		String countServices = autoServiceService.getCount().toString();

		try {
			User currentUser = authenticationHelper.tryGetUser(session);
			model.addAttribute("currentUser", currentUser);
		} catch (UnauthorizedOperationException e) {
			model.addAttribute("currentUser", null);
		}

		model.addAttribute("countVisits", countVisits);
		model.addAttribute("countVehicles", countVehicles);
		model.addAttribute("countUsers", countUsers);
		model.addAttribute("countServices", countServices);

		return "index";
	}
}
