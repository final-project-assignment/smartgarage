package com.smartGarage.controllers.mvc;

import com.smartGarage.controllers.AuthenticationHelper;
import com.smartGarage.currency.CurrencyConverter;
import com.smartGarage.modelMappers.VisitModelMapper;
import com.smartGarage.models.*;
import com.smartGarage.models.DTObjects.CurrencyDTO;
import com.smartGarage.models.DTObjects.SelectedServiceDTO;
import com.smartGarage.models.DTObjects.VisitDTO;
import com.smartGarage.services.contracts.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/profile")
public class ProfilePageMvcController {

	private final VehicleService vehicleService;
	private final VisitService visitService;
	private final AuthenticationHelper authenticationHelper;
	private final UserService userService;
	private final UserDetailsService userDetailsService;
	private final VisitModelMapper visitModelMapper;
	private final AutoServiceService autoServiceService;
	private final List<String> selectedServices = new ArrayList<>();
	private final List<String> selectedServicesNames = new ArrayList<>();

	public ProfilePageMvcController(VehicleService vehicleService,
			VisitService visitService,
			AuthenticationHelper authenticationHelper,
			UserService userService,
			UserDetailsService userDetailsService,
			VisitModelMapper visitModelMapper,
			AutoServiceService autoServiceService) {

		this.vehicleService = vehicleService;
		this.visitService = visitService;
		this.authenticationHelper = authenticationHelper;
		this.userService = userService;
		this.userDetailsService = userDetailsService;
		this.visitModelMapper = visitModelMapper;
		this.autoServiceService = autoServiceService;
	}

	@ModelAttribute("selectedService")
	public SelectedServiceDTO selectedService() {

		return new SelectedServiceDTO();
	}

	@ModelAttribute("services")
	public List<AutoService> populateAutoService() {

		return autoServiceService.getAll();
	}

	@ModelAttribute("userCurrency")
	public CurrencyDTO currency() {

		return new CurrencyDTO();
	}

	@ModelAttribute("currencies")
	public ArrayList<String> populateCurrencies() {

		return CurrencyConverter.getAllCurrencies();
	}

	@ModelAttribute("user")
	public User populateUser(HttpSession session) {

		User user;
		user = authenticationHelper.tryGetUser(session);

		return userService.getByID(user.getUserId());
	}

	@ModelAttribute("vehicles")
	public List<Vehicle> populateVehicles(HttpSession session) {

		User user;
		user = authenticationHelper.tryGetUser(session);

		return vehicleService.filterByOwner(user, user.getUserId());
	}

	@ModelAttribute("visits")
	public List<Visit> populateVisits(HttpSession session) {

		User user;
		user = authenticationHelper.tryGetUser(session);

		return visitService.filterByCustomer(user, user.getUserId());
	}

	@ModelAttribute("userDetails")
	public List<UserDetails> populateUserDetails(HttpSession session) {

		User user;
		user = authenticationHelper.tryGetUser(session);

		return userDetailsService.filterByCustomer(user.getUserId());
	}

	@GetMapping
	public String userProfileBGN(
			Model model, HttpSession session, @ModelAttribute("userCurrency") CurrencyDTO currencyDTO) {

		User user;
		user = authenticationHelper.tryGetUser(session);

		model.addAttribute("users", userService.getByID(user.getUserId()));
		model.addAttribute("email", userService.getByEmail(user.getEmail()));

		List<FrontEndVisitModel> visits = visitService.searchByCustomerID(user.getUserId(), "BGN");
		model.addAttribute("userVisits", visits);
		model.addAttribute("id", user.getUserId());

		return "users/profilePage";
	}

	// Visits:

	@GetMapping("/visits")
	public String userVisits(
			Model model, HttpSession session) {

		User user;
		user = authenticationHelper.tryGetUser(session);

		model.addAttribute("users", visitService.filterByCustomer(user, user.getUserId()));

		return "customer/customerVisits";
	}

	@GetMapping("/visit")
	public String userVisitPage() {

		return "customer/customerVisitPage";
	}

	@PostMapping
	public String userProfileWithCurrency(
			@ModelAttribute("userCurrency") CurrencyDTO currencyDTO, Model model, HttpSession session) {

		User user;
		user = authenticationHelper.tryGetUser(session);

		model.addAttribute("users", userService.getByID(user.getUserId()));
		model.addAttribute("email", userService.getByEmail(user.getEmail()));

		List<FrontEndVisitModel> visits =
				visitService.searchByCustomerID(user.getUserId(), currencyDTO.getUserCurrency());
		model.addAttribute("userVisits", visits);
		model.addAttribute("id", user.getUserId());

		return "users/profilePage";
	}

	@PostMapping("visit/new/service")
	public String addSelectedService(
			@ModelAttribute("selectedService") SelectedServiceDTO selectedService, HttpSession session) {

		User user = authenticationHelper.tryGetUser(session);

		selectedServices.add(selectedService.getSelectedServiceId());
		selectedServicesNames.add(autoServiceService.getByID(user, selectedService.getSelectedServiceId()).getName());

		return "customer/customerVisits-new";
	}

	@GetMapping("visit/new")
	public String showNewVisitPage(Model model) {

		model.addAttribute("visits", new VisitDTO());

		return "customer/customerVisits-new";
	}

	@PostMapping("visit/new")
	public String createAutoService(
			@ModelAttribute("visits") VisitDTO visitDTO, BindingResult bindingResult, HttpSession session) {

		User user = authenticationHelper.tryGetUser(session);

		if (bindingResult.hasErrors()) {

			return "redirect:/profile";
		}

		Visit visit = visitModelMapper.fromDto(visitDTO);
		visitService.create(user, visit);

		return "redirect:/profile";
	}

	@GetMapping("visit/delete")
	public String deleteVisitPage(
			Model model, HttpSession session) {

		User user = authenticationHelper.tryGetUser(session);

		model.addAttribute("visits", visitService.filterByCustomer(user, user.getUserId()));

		return "customer/customerVisits-delete";
	}

	@GetMapping("/visit/delete/{id}")
	public String deleteVisit(
			@PathVariable String id, HttpSession session) {

		User user = authenticationHelper.tryGetUser(session);

		visitService.delete(user, id);

		return "redirect:/";
	}

	@GetMapping("/vehicle")
	public String userVehiclePage() {

		return "customer/customerVehiclePage";
	}

	@GetMapping("/vehicles")
	public String userVehicles(
			Model model, HttpSession session) {

		User user;
		user = authenticationHelper.tryGetUser(session);

		model.addAttribute("users", vehicleService.filterByCustomer(user, user.getUserId()));

		return "customer/customerVehicles";
	}

	@GetMapping("/vehicle/delete")
	public String deleteVehiclePage(
			Model model, HttpSession session) {

		User user = authenticationHelper.tryGetUser(session);

		model.addAttribute("vehicle", vehicleService.filterByCustomer(user, user.getUserId()));

		return "customer/customerVehicle-delete";
	}

	@GetMapping("/vehicle/delete/{id}")
	public String deleteVehicle(
			@PathVariable String id, HttpSession session) {

		User user = authenticationHelper.tryGetUser(session);

		vehicleService.delete(user, id);
		return "redirect:/";
	}

	@GetMapping("/details")
	public String userDetails(
			Model model, HttpSession session) {

		User user;
		user = authenticationHelper.tryGetUser(session);

		model.addAttribute("userDetails", userDetailsService.filterByCustomer(user.getUserId()));

		return "customer/customerUserDetails";
	}
}