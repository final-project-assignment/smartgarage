package com.smartGarage.controllers.mvc;

import com.smartGarage.controllers.AuthenticationHelper;
import com.smartGarage.exceptions.EntityNotFoundException;
import com.smartGarage.exceptions.UnauthorizedOperationException;
import com.smartGarage.models.DTObjects.LoginDTO;
import com.smartGarage.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/auth")
public class AuthenticationMvcController {

	private final AuthenticationHelper authenticationHelper;

	@ModelAttribute("isAuthenticated")
	public boolean populateIsAuthenticated(HttpSession session) {

		return session.getAttribute("currentUser") != null;
	}

	@Autowired
	public AuthenticationMvcController(AuthenticationHelper authenticationHelper) {

		this.authenticationHelper = authenticationHelper;
	}

	@GetMapping("/register")
	public String showHowToRegisterPage() {

		return "how-to-register";
	}

	@GetMapping("/login")
	public String showLoginPage(Model model) {

		model.addAttribute("login", new LoginDTO());

		return "login";
	}

	@PostMapping("/login")
	public String handleLogin(
			@Valid @ModelAttribute("login") LoginDTO login,
			BindingResult bindingResult,
			HttpSession session,
			Model model) {

		try {
			if (bindingResult.hasErrors()) {
				return "login";
			}

			authenticationHelper.verifyAuthentication(login.getEmail(), login.getPassword());
			session.setAttribute("currentUser", login.getEmail());

			User user = authenticationHelper.tryGetUser(session);

			if (user.isEmployee()) {
				return "redirect:/admin";
			}

			if (user.isCustomer()) {
				return "redirect:/profile";
			}

		} catch (UnauthorizedOperationException ex) {

			model.addAttribute("error", ex.getMessage());
		} catch (EntityNotFoundException ex2) {

			model.addAttribute("error", ex2.getMessage());
		}

		return "/login";
	}

	@GetMapping("/logout")
	public String handleLogout(HttpSession session) {

		session.removeAttribute("currentUser");

		return "redirect:/";
	}
}
