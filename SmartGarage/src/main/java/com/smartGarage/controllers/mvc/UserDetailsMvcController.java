package com.smartGarage.controllers.mvc;

import com.smartGarage.controllers.AuthenticationHelper;
import com.smartGarage.modelMappers.UserDetailModelMapper;
import com.smartGarage.models.DTObjects.UserDetailDTO;
import com.smartGarage.models.DTObjects.search.UserDetailsSearchDTO;
import com.smartGarage.models.User;
import com.smartGarage.models.UserDetails;
import com.smartGarage.services.contracts.UserDetailsService;
import com.smartGarage.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

import static com.smartGarage.controllers.mvc.AutoServiceMvcController.ROLE_EMPLOYEE;

@Controller
@RequestMapping("/user/details")
public class UserDetailsMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final UserDetailsService userDetailsService;
    private final UserService userService;
    private final UserDetailModelMapper modelMapper;

    public UserDetailsMvcController(AuthenticationHelper authenticationHelper,
                                    UserDetailsService userDetailsService,
                                    UserService userService,
                                    UserDetailModelMapper modelMapper) {

        this.authenticationHelper = authenticationHelper;
        this.userDetailsService = userDetailsService;
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @ModelAttribute("users")
    public List<User> populateUsers() {
        return userService.getAll();
    }

    @ModelAttribute("fillUserDetails")
    public List<UserDetails> populateUserDetails() {
        return userDetailsService.getAll();
    }

    @GetMapping
    public String showAllUsers(Model model, HttpSession session) {
        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

        model.addAttribute("userDetails", userDetailsService.getAll());

        return "userDetails/userDetails";
    }

    @GetMapping("/new")
    public String createNewUserDetails(Model model, HttpSession session) {
        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

        model.addAttribute("userDetails", new UserDetailDTO());

        return "userDetails/userDetails-new";
    }

    @PostMapping("/new")
    public String handleCreateNewUserDetails(Model model,
                                             @Valid @ModelAttribute("userDetails") UserDetailDTO userDetailDTO,
                                             BindingResult bindingResult,
                                             HttpSession session) {


        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);
        UserDetails userDetails = modelMapper.fromDto(userDetailDTO);
        if (bindingResult.hasErrors()) {
            model.addAttribute("errorMessage",
                    "Phone number should be unique, 10 numbers and start with 0");
            return "redirect:/user/detail/new";
        }
        userDetailsService.create(userDetails);

        return "redirect:/";
    }

    @GetMapping("/update/{id}")
    public String showEditPage(@PathVariable String id,
                               Model model,
                               HttpSession session) {

        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

        UserDetails userDetails = userDetailsService.getByID(id);
        UserDetailDTO userDetailDTO = modelMapper.toDto(userDetails);

        model.addAttribute("userDetailId", id);
        model.addAttribute("userDetail", userDetailDTO);

        return "userDetails/userDetails-update";
    }

    @PostMapping("/update/{id}")
    public String updateAutoService(@PathVariable String id,
                                    @Valid @ModelAttribute("userDetails") UserDetailDTO userDetailDTO,
                                    BindingResult errors,
                                    HttpSession session) {

        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

        if (errors.hasErrors()) {

            return "/userDetails/userDetails";
        }

        UserDetails userDetails = modelMapper.fromDto(userDetailDTO);
        userDetailsService.update(userDetails);

        return "redirect:/user/details";
    }

    @GetMapping("/update")
    public String showAllServicesForUpdate(Model model, HttpSession session) {

        authenticationHelper.verifyAuthorization(session, ROLE_EMPLOYEE);

        model.addAttribute("userDetails", userDetailsService.getAll());

        return "userDetails/userDetails-update-list";
    }


    @GetMapping("/delete")
    public String deleteUSerPage(Model model, HttpSession session) {

        model.addAttribute("usersDetails", userDetailsService.getAll());

        return "userDetails/userDetails-delete";
    }

    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable String id) {

        userDetailsService.delete(id);

        return "redirect:/user/details";
    }

    @GetMapping("/filter")
    public String handleGetUserDetailsSearch() {

        return "redirect:/user/details";
    }

    @PostMapping("/filter")
    public String handleUserDetailsSearch(Model model,
                      @ModelAttribute("userDetailsSearchDTO") UserDetailsSearchDTO userDetailsSearchDTO) {

        userDetailsService.filter(modelMapper.fromDto(userDetailsSearchDTO));

        model.addAttribute("userDetails",
                userDetailsService.filter(modelMapper.fromDto(userDetailsSearchDTO)));

        return "userDetails/userDetails";
    }
}
