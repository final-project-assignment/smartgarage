package com.smartGarage.controllers.rest;

import com.smartGarage.controllers.AuthenticationHelper;
import com.smartGarage.modelMappers.UserDetailModelMapper;
import com.smartGarage.models.DTObjects.UserDetailDTO;
import com.smartGarage.models.User;
import com.smartGarage.models.UserDetails;
import com.smartGarage.services.contracts.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/user/details")
public class UserDetailRestController {

    private final UserDetailsService userDetailsService;
    private final UserDetailModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;


    @Autowired
    public UserDetailRestController(UserDetailsService userDetailsService,
                                    UserDetailModelMapper modelMapper,
                                    AuthenticationHelper authenticationHelper) {

        this.userDetailsService = userDetailsService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<UserDetails> getAll() {
        return userDetailsService.getAll();
    }

    @GetMapping("/{id}")
    public UserDetails getByID(@PathVariable String id) {

        return userDetailsService.getByID(id);
    }

    @PostMapping
    public UserDetails create(@Valid @RequestBody UserDetailDTO userDetailsDTO,
                              BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            String a = bindingResult.getFieldError().toString();

            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, a);
        } else {
        UserDetails userDetails = modelMapper.fromDto(userDetailsDTO);

        userDetailsService.create(userDetails);

        return userDetails;
        }
    }

    @PutMapping("/{id}")
    public UserDetails update(@PathVariable String id,
                              @Valid @RequestBody UserDetailDTO userDetailDTO,
                              BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            String a = bindingResult.getFieldError().toString();

            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, a);
        }
        else {
            UserDetails userDetail = modelMapper.fromDto(userDetailDTO, id);
            userDetailsService.update(userDetail);

            return userDetail;
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id) {
        userDetailsService.delete(id);
    }

    @GetMapping("/search")
    public List<UserDetails> search(
            @RequestHeader HttpHeaders headers,
            @RequestParam(required = false, name = "firstName") Optional<String> firstName,
            @RequestParam(required = false, name = "lastName") Optional<String> lastName,
            @RequestParam(required = false, name = "email") Optional<String> email,
            @RequestParam(required = false, name = "phone") Optional<String> phone,
            @RequestParam(required = false, name = "vehicle") Optional<String> vehicle) {

        User user = authenticationHelper.tryGetUser(headers);

        return userDetailsService.search(user, firstName, lastName, email, phone, vehicle);
    }

    @GetMapping("/sort")
    public List<UserDetails> sort(@RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);

        return userDetailsService.sort(user);
    }
}
