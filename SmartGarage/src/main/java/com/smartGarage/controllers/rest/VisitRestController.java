package com.smartGarage.controllers.rest;

import com.smartGarage.controllers.AuthenticationHelper;
import com.smartGarage.modelMappers.VisitModelMapper;
import com.smartGarage.models.DTObjects.VisitDTO;
import com.smartGarage.models.FrontEndVisitModel;
import com.smartGarage.models.User;
import com.smartGarage.models.Visit;
import com.smartGarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/visits")
public class VisitRestController {

    private final VisitService visitService;
    private final VisitModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public VisitRestController(VisitService visitService,
                               VisitModelMapper modelMapper,
                               AuthenticationHelper authenticationHelper) {

        this.visitService = visitService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Visit> getAll(@RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);

        return visitService.getAll(user);
    }

    @GetMapping("/{id}")
    public Visit getByID(@PathVariable String id, @RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);

        return visitService.getByID(user, id);
    }

    @PostMapping
    public Visit create(@Valid @RequestBody VisitDTO visitDTO,
                        @RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);
        Visit visit = modelMapper.fromDto(visitDTO);

        visitService.create(user, visit);

        return visit;
    }

    @PutMapping("/{id}")
    public Visit update(@PathVariable String id,
                        @Valid @RequestBody VisitDTO visitDTO,
                        @RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);
        Visit visit = modelMapper.fromDto(visitDTO, id);

        visitService.update(user, visit);

        return visit;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id,
                       @RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);

        visitService.delete(user, id);
    }

    @GetMapping("/filter")
    public List<Visit> filter(
            @RequestHeader HttpHeaders headers,
            @RequestParam(required = false, name = "licensePlate") Optional<String> licensePlate,
            @RequestParam(required = false, name = "startDate") Optional<String> startDate,
            @RequestParam(required = false, name = "endDate") Optional<String> endDate) {

        User user = authenticationHelper.tryGetUser(headers);

        return visitService.filter(user, licensePlate, startDate, endDate);
    }

    @GetMapping("/search")
    public List<FrontEndVisitModel> searchByCustomerID(@RequestParam String customerId,
                                                       @RequestParam String currency) {

        return visitService.searchByCustomerID(customerId, currency);
    }

    @GetMapping("/count")
    public int getCount() {
        return visitService.getCount();
    }
}
