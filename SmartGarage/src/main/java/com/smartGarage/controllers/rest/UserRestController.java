package com.smartGarage.controllers.rest;

import com.smartGarage.modelMappers.UserModelMapper;
import com.smartGarage.models.DTObjects.UserDTO;
import com.smartGarage.models.User;
import com.smartGarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private final UserService userService;
    private final UserModelMapper modelMapper;

    @Autowired
    public UserRestController(UserService userService,
                              UserModelMapper modelMapper) {

        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public List<User> getAll() {

        return userService.getAll();
    }

    @GetMapping("/{id}")
    public User getById(@PathVariable String id) {

        return userService.getByID(id);
    }

    @PostMapping
    public User create(@Valid @RequestBody UserDTO userDTO,
                       BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            String a = Objects.requireNonNull(bindingResult.getFieldError()).toString();
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, a);
        } else {
            User user = modelMapper.fromDto(userDTO);
            userService.create(user);

            return user;
        }
    }

    @PutMapping("/{id}")
    public User update(@PathVariable String id,
                       @Valid @RequestBody UserDTO userDTO,
                       BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            String a = Objects.requireNonNull(bindingResult.getFieldError()).toString();
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, a);
        } else {
            User user = modelMapper.fromDto(userDTO, id);
            userService.update(user);

            return user;
        }
    }

    @DeleteMapping("/{username}")
    public void delete(@PathVariable String username) {
        userService.delete(username);
    }

}