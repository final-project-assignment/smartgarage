package com.smartGarage.controllers.rest;

import com.smartGarage.controllers.AuthenticationHelper;
import com.smartGarage.modelMappers.VehicleModelMapper;
import com.smartGarage.models.DTObjects.VehicleDTO;
import com.smartGarage.models.User;
import com.smartGarage.models.Vehicle;
import com.smartGarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/vehicles")
public class VehicleRestController {

    private final VehicleService vehicleService;
    private final VehicleModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public VehicleRestController(VehicleService vehicleService,
                                 VehicleModelMapper modelMapper,
                                 AuthenticationHelper authenticationHelper) {

        this.vehicleService = vehicleService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Vehicle> getAll(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);

        return vehicleService.getAll(user);
    }

    @GetMapping("/{id}")
    public Vehicle getByID(@PathVariable String id,
                           @RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);

        return vehicleService.getByID(user, id);
    }

    @PostMapping
    public Vehicle create(@Valid @RequestBody VehicleDTO vehicleDTO,
                          BindingResult errors,
                          @RequestHeader HttpHeaders headers) {

        if (errors.hasErrors())
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, "Errors");
        else {
            User user = authenticationHelper.tryGetUser(headers);
            Vehicle vehicle = modelMapper.fromDto(vehicleDTO);
            vehicleService.create(user, vehicle);

            return vehicle;
        }
    }

    @PutMapping("/{id}")
    public Vehicle update(@PathVariable String id,
                          @Valid @RequestBody VehicleDTO vehicleDTO,
                          BindingResult errors,
                          @RequestHeader HttpHeaders headers) {

        if (errors.hasErrors()) {
            String a = errors.getFieldError().toString();

            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, a);
        } else {
            User user = authenticationHelper.tryGetUser(headers);
            Vehicle vehicle = modelMapper.fromDto(vehicleDTO, id);
            vehicleService.update(user, vehicle);

            return vehicle;
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id,
                       @RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);
        vehicleService.delete(user, id);
    }

    @GetMapping("/filter/customer")
    public List<Vehicle> filterByCustomer(@RequestParam String customerId,
                                          @RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);

        return vehicleService.filterByCustomer(user, customerId);
    }

    @GetMapping("/filter/owner")
    public List<Vehicle> filterByOwner(@RequestParam(required = false, name = "owner") String ownerId,
                                       @RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);

        return vehicleService.filterByOwner(user, ownerId);
    }
}
