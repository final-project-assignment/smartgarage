package com.smartGarage.controllers.rest;

import com.smartGarage.controllers.AuthenticationHelper;
import com.smartGarage.modelMappers.AutoServiceModelMapper;
import com.smartGarage.models.AutoService;
import com.smartGarage.models.DTObjects.AutoServiceDTO;
import com.smartGarage.models.User;
/*
import com.smartGarage.reports.Report;
*/
import com.smartGarage.services.contracts.AutoServiceService;
/*
import net.sf.jasperreports.engine.JRException;
*/
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/services")
public class AutoServiceRestController {

    private final AutoServiceService autoService;
    private final AutoServiceModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

/*    @Autowired
    Report report;*/

    @Autowired
    public AutoServiceRestController(AutoServiceService autoService,
                                     AutoServiceModelMapper modelMapper,
                                     AuthenticationHelper authenticationHelper) {

        this.autoService = autoService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<AutoService> getAll(@RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);

        return autoService.getAll(user);
    }

    @GetMapping("/{id}")
    public AutoService getByID(@PathVariable String id,
                               @RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);

        return autoService.getByID(user, id);
    }

    @PostMapping
    public AutoService create(@Valid @RequestBody AutoServiceDTO autoServiceDTO,
                              @RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);
        AutoService autoService = modelMapper.fromDto(autoServiceDTO);

        this.autoService.create(user, autoService);

        return autoService;
    }

    @PutMapping("/{id}")
    public AutoService update(@PathVariable String id,
                              @Valid @RequestBody AutoServiceDTO autoServiceDTO,
                              @RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);
        AutoService autoService = modelMapper.fromDto(autoServiceDTO, id);

        this.autoService.update(user, autoService);

        return autoService;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id,
                       @RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);
        autoService.delete(user, id);
    }

    @GetMapping("/filter")
    public List<AutoService> filter(@RequestParam(required = false, name = "name") Optional<String> name,
                                    @RequestParam(required = false, name = "price") Optional<Double> minPrice,
                                    @RequestParam(required = false, name = "price") Optional<Double> maxPrice,
                                    @RequestHeader HttpHeaders headers) {

        User user = authenticationHelper.tryGetUser(headers);

        return autoService.filter(user, name, minPrice, maxPrice);
    }

/*    @GetMapping("/report/{format}")
    public String generateReport(@PathVariable String format)
            throws FileNotFoundException, JRException {

        return report.exportReport(format);
    }*/
}
