insert into smartgarage.users (user_id, username, password, email, reset_password_token) values ('2d15daf4-9399-11eb-ad6a-9829a64392c7', 'alexandra', '$2y$12$sTP.ijoBgf80yMAZ.p3fX.cbsbq2maJAT4I2qqP7JiQq4KlQeD5LW', 'alexandrakoluncheva@gmail.com', null);
insert into smartgarage.users (user_id, username, password, email, reset_password_token) values ('4bb2fa86-9399-11eb-ad6a-9829a64392c7', 'rosica', '$2y$12$lDfO4GZOfrgmFP4/lSvqQuou.RqOK8RQ.byPgVfB.XF6mjQsmeXZW', 'rosica@gmail.com', null);
insert into smartgarage.users (user_id, username, password, email, reset_password_token) values ('731ce78c-8f9b-11eb-8a56-cae72813e33c', 'irina', '$2y$12$gZyPtN0x0cOIP7tVJoqgH.eAJ3HqsuOxa1vzKe8FpaHhoBOhPE7vS', 'irina@gmail.com', null);

insert into smartgarage.user_details (user_detail_id, first_name, last_name, phone, user_id) values ('d2b0dee7-8f9b-11eb-8a56-cae72813e33c', 'Irina', 'Stoeva', '0896211121', '731ce78c-8f9b-11eb-8a56-cae72813e33c');
insert into smartgarage.user_details (user_detail_id, first_name, last_name, phone, user_id) values ('5e85284b-9399-11eb-ad6a-9829a64392c7', 'Alexandra', 'Koluncheva', '0894610711', '2d15daf4-9399-11eb-ad6a-9829a64392c7');
insert into smartgarage.user_details (user_detail_id, first_name, last_name, phone, user_id) values ('f1f36eba-9399-11eb-ad6a-9829a64392c7', 'Rosica', 'Staikova', '0894512456', '4bb2fa86-9399-11eb-ad6a-9829a64392c7');

insert into smartgarage.roles (role_id, type) values ('85d322cb-7a0f-11eb-9eaf-05e779a6d53b', 'customer');
insert into smartgarage.roles (role_id, type) values ('85d88d99-7a0f-11eb-9eaf-05e779a6d53b', 'employee');

insert into smartgarage.users_roles (user_id, role_id) values ('731ce78c-8f9b-11eb-8a56-cae72813e33c', '85d88d99-7a0f-11eb-9eaf-05e779a6d53b');
insert into smartgarage.users_roles (user_id, role_id) values ('2d15daf4-9399-11eb-ad6a-9829a64392c7', '85d88d99-7a0f-11eb-9eaf-05e779a6d53b');
insert into smartgarage.users_roles (user_id, role_id) values ('4bb2fa86-9399-11eb-ad6a-9829a64392c7', '85d322cb-7a0f-11eb-9eaf-05e779a6d53b');

insert into smartgarage.manufacturers (manufacturer_id, manufacturer_name) values ('6f106fa1-8f8a-11eb-8a56-cae72813e33c', 'Opel');
insert into smartgarage.manufacturers (manufacturer_id, manufacturer_name) values ('19142ae7-8f9e-11eb-8a56-cae72813e33c', 'BMW');
insert into smartgarage.manufacturers (manufacturer_id, manufacturer_name) values ('19157338-8f9e-11eb-8a56-cae72813e33c', 'Audi');
insert into smartgarage.manufacturers (manufacturer_id, manufacturer_name) values ('19162631-8f9e-11eb-8a56-cae72813e33c', 'Mercedes');
insert into smartgarage.manufacturers (manufacturer_id, manufacturer_name) values ('1916e450-8f9e-11eb-8a56-cae72813e33c', 'Volvo');

insert into smartgarage.models (model_id, manufacturer_id,model_name) values ('91e97e86-8f8a-11eb-8a56-cae72813e33c', '6f106fa1-8f8a-11eb-8a56-cae72813e33c','Corsa');
insert into smartgarage.models (model_id, manufacturer_id,model_name) values ('205bbf23-8fa0-11eb-8a56-cae72813e33c', '19142ae7-8f9e-11eb-8a56-cae72813e33c','X3');
insert into smartgarage.models (model_id, manufacturer_id,model_name) values ('205af353-8fa0-11eb-8a56-cae72813e33c', '19157338-8f9e-11eb-8a56-cae72813e33c','A3');
insert into smartgarage.models (model_id, manufacturer_id,model_name) values ('205a3c9b-8fa0-11eb-8a56-cae72813e33c', '19162631-8f9e-11eb-8a56-cae72813e33c','GLS');
insert into smartgarage.models (model_id, manufacturer_id,model_name) values ('2057adba-8fa0-11eb-8a56-cae72813e33c', '1916e450-8f9e-11eb-8a56-cae72813e33c','XC90');

insert into smartgarage.vehicles (vehicle_id, VIN, license_plate, owner, model_id, year) values ('81c55aaf-8f8c-11eb-8a56-cae72813e33c', 'abcdefgh123456789','CT1234AV','4bb2fa86-9399-11eb-ad6a-9829a64392c7','91e97e86-8f8a-11eb-8a56-cae72813e33c','1999');
insert into smartgarage.vehicles (vehicle_id, VIN, license_plate, owner, model_id, year) values ('3aad2b8c-9b50-11eb-8a7e-cbe9313781e1', 'abcfsgdjwgaf12345','E4590MK','4bb2fa86-9399-11eb-ad6a-9829a64392c7','205af353-8fa0-11eb-8a56-cae72813e33c','2009');
insert into smartgarage.vehicles (vehicle_id, VIN, license_plate, owner, model_id, year) values ('3aaa662c-9b50-11eb-8a7e-cbe9313781e1', '123456789abvgdshj','E1545MA','4bb2fa86-9399-11eb-ad6a-9829a64392c7','205bbf23-8fa0-11eb-8a56-cae72813e33c','2007');

insert into smartgarage.services (service_id, name, price) values ('23fac32f-9b51-11eb-8a7e-cbe9313781e1', 'Change Filters',900);
insert into smartgarage.services (service_id, name, price) values ('23fd3c53-9b51-11eb-8a7e-cbe9313781e1', 'Repaint',1149.99);
insert into smartgarage.services (service_id, name, price) values ('23fe2a27-9b51-11eb-8a7e-cbe9313781e1', 'Tyre Replacement',300);
insert into smartgarage.services (service_id, name, price) values ('23ffa5d6-9b51-11eb-8a7e-cbe9313781e1', 'Fix Brakes',890);
insert into smartgarage.services (service_id, name, price) values ('6f276571-9ac6-11eb-af27-9829a64392c7', 'Winter Tyres', 150);
insert into smartgarage.services (service_id, name, price) values ('6f2b24bc-9ac6-11eb-af27-9829a64392c7', 'Summer Tyres', 95);
insert into smartgarage.services (service_id, name, price) values ('6f2d3f09-9ac6-11eb-af27-9829a64392c7', 'Oil filter change', 80);
insert into smartgarage.services (service_id, name, price) values ('6f2ec5b3-9ac6-11eb-af27-9829a64392c7', 'Brake Replacement 1', 350);
insert into smartgarage.services (service_id, name, price) values ('6f30090a-9ac6-11eb-af27-9829a64392c7', 'Brake Fluid replacement', 100);
insert into smartgarage.services (service_id, name, price) values ('6f3147c2-9ac6-11eb-af27-9829a64392c7', 'Air Filter Change', 20);
insert into smartgarage.services (service_id, name, price) values ('e54f9616-8f98-11eb-8a56-cae72813e33c', 'Change oil', 30);
insert into smartgarage.services (service_id, name, price) values ('fe36db6e-a6d2-46d7-a01a-19201040a170', 'Change Transmision', 1000);

insert into smartgarage.visits (visit_id, start_date, end_date, vehicle_id) values ('e0187641-8f9c-11eb-8a56-cae72813e33c', '2018-04-03 02:07:00', '2018-05-03 02:07:00', '81c55aaf-8f8c-11eb-8a56-cae72813e33c');
insert into smartgarage.visits (visit_id, start_date, end_date, vehicle_id) values ('6f4b8178-9d74-11eb-bad7-9829a64392c7', '2020-05-11 01:53:23', '2020-10-03 01:53:29', '81c55aaf-8f8c-11eb-8a56-cae72813e33c');
insert into smartgarage.visits (visit_id, start_date, end_date, vehicle_id) values ('6f4d2e46-9d74-11eb-bad7-9829a64392c7', '2020-04-15 01:53:39', '2020-05-15 01:53:44', '81c55aaf-8f8c-11eb-8a56-cae72813e33c');
insert into smartgarage.visits (visit_id, start_date, end_date, vehicle_id) values ('6f4e258a-9d74-11eb-bad7-9829a64392c7', '2020-03-09 01:54:01', '2020-03-11 01:54:06', '81c55aaf-8f8c-11eb-8a56-cae72813e33c');
insert into smartgarage.visits (visit_id, start_date, end_date, vehicle_id) values ('6f4fa8de-9d74-11eb-bad7-9829a64392c7', '2021-04-10 01:54:23', '2021-04-15 01:54:26', '81c55aaf-8f8c-11eb-8a56-cae72813e33c');
insert into smartgarage.visits (visit_id, start_date, end_date, vehicle_id) values ('6f50afe9-9d74-11eb-bad7-9829a64392c7', '2021-03-11 01:54:42', '2021-04-15 01:54:48', '81c55aaf-8f8c-11eb-8a56-cae72813e33c');
insert into smartgarage.visits (visit_id, start_date, end_date, vehicle_id) values ('e0187341-8f9c-11eb-8a56-cae72813e33c', '2018-04-03 02:07:00', '2018-05-03 02:07:00', '81c55aaf-8f8c-11eb-8a56-cae72813e33c');
insert into smartgarage.visits (visit_id, start_date, end_date, vehicle_id) values ('e0187641-8f9c-11eb-8356-cae72813e331', '2018-05-03 02:07:00', '2018-05-02 02:07:00', '3aaa662c-9b50-11eb-8a7e-cbe9313781e1');
insert into smartgarage.visits (visit_id, start_date, end_date, vehicle_id) values ('20187241-8f9c-11eb-8356-cae72813e331', '2018-05-03 02:07:00', '2018-05-02 02:07:00', '3aad2b8c-9b50-11eb-8a7e-cbe9313781e1');

insert into smartgarage.visits_services (visit_id, service_id) values ('e0187641-8f9c-11eb-8a56-cae72813e33c', 'e54f9616-8f98-11eb-8a56-cae72813e33c');
insert into smartgarage.visits_services (visit_id, service_id) values ('6f4d2e46-9d74-11eb-bad7-9829a64392c7', 'fe36db6e-a6d2-46d7-a01a-19201040a170');
insert into smartgarage.visits_services (visit_id, service_id) values ('e0187641-8f9c-11eb-8a56-cae72813e33c', '6f276571-9ac6-11eb-af27-9829a64392c7');
insert into smartgarage.visits_services (visit_id, service_id) values ('6f4fa8de-9d74-11eb-bad7-9829a64392c7', '6f2d3f09-9ac6-11eb-af27-9829a64392c7');
insert into smartgarage.visits_services (visit_id, service_id) values ('6f4e258a-9d74-11eb-bad7-9829a64392c7', '6f2b24bc-9ac6-11eb-af27-9829a64392c7');
insert into smartgarage.visits_services (visit_id, service_id) values ('6f4d2e46-9d74-11eb-bad7-9829a64392c7', 'e54f9616-8f98-11eb-8a56-cae72813e33c');
insert into smartgarage.visits_services (visit_id, service_id) values ('6f4b8178-9d74-11eb-bad7-9829a64392c7', 'e54f9616-8f98-11eb-8a56-cae72813e33c');
