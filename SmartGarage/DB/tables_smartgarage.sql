create table manufacturers
(
    manufacturer_id varchar(40) default uuid() not null
        primary key,
    manufacturer_name varchar(20) not null
);

create table models
(
    model_id varchar(40) default uuid() not null
        primary key,
    manufacturer_id varchar(40) default uuid() not null,
    model_name varchar(20) not null,
    constraint models_manufacturers_fk
        foreign key (manufacturer_id) references manufacturers (manufacturer_id)
);

create table roles
(
    role_id varchar(40) default uuid() not null
        primary key,
    type varchar(40) not null
);

create table services
(
    service_id varchar(40) default uuid() not null
        primary key,
    name varchar(40) not null,
    price double not null
);

create table users
(
    user_id varchar(40) default uuid() not null
        primary key,
    username varchar(40) not null,
    password varchar(60) not null,
    email varchar(40) not null,
    reset_password_token varchar(30) null,
    constraint Users_email_uindex
        unique (email),
    constraint Users_username_uindex
        unique (username)
);

create table user_details
(
    user_detail_id varchar(40) default uuid() not null
        primary key,
    first_name varchar(20) not null,
    last_name varchar(20) not null,
    phone varchar(10) not null,
    user_id varchar(40) not null,
    constraint user_info_phone_uindex
        unique (phone),
    constraint user_info_users_fk
        foreign key (user_id) references users (user_id)
);

create table users_roles
(
    user_id varchar(40) default uuid() not null,
    role_id varchar(40) not null,
    constraint users_roles_role_fk
        foreign key (role_id) references roles (role_id),
    constraint users_users_role_fk
        foreign key (user_id) references users (user_id)
);

create table vehicles
(
    vehicle_id varchar(40) default uuid() not null
        primary key,
    VIN varchar(17) not null,
    license_plate varchar(8) not null,
    owner varchar(40) default uuid() not null,
    model_id varchar(40) default uuid() not null,
    year int not null,
    constraint vehicles_VIN_uindex
        unique (VIN),
    constraint vehicles_license_plate_uindex
        unique (VIN),
    constraint vehicles_models_fk
        foreign key (model_id) references models (model_id),
    constraint vehicles_owner_fk
        foreign key (owner) references users (user_id)
);

create table visits
(
    visit_id varchar(40) default uuid() not null
        primary key,
    start_date datetime null,
    end_date datetime null,
    vehicle_id varchar(40) default uuid() not null,
    constraint visits_vehicles_fk
        foreign key (vehicle_id) references vehicles (vehicle_id)
);

create table visits_services
(
    visit_id varchar(40) default uuid() not null,
    service_id varchar(40) default uuid() not null,
    constraint visits_services_services_fk
        foreign key (service_id) references services (service_id),
    constraint visits_services_visits_fk
        foreign key (visit_id) references visits (visit_id)
);

